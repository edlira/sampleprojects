-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 21, 2015 at 05:11 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;



-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_actions`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_actions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `block` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_blocks`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_blocks` (
  `id` int(11) NOT NULL,
  `block_label` varchar(255) NOT NULL,
  `block_sequence` varchar(50) NOT NULL,
  `block_module` varchar(255) NOT NULL,
  `block_tab` int(11) NOT NULL,
  `block_roles` text NOT NULL,
  `block_users` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_entities`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_entities` (
  `dsid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `entity` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dsid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_extensions`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `parenttab` varchar(255) NOT NULL,
  `current` tinyint(3) NOT NULL,
  `generated` tinyint(3) NOT NULL,
  `type` varchar(250) NOT NULL,
  `template` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_fieldparams`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_fieldparams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `input_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `parameter` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `modulefield` varchar(255) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_fields`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_fields` (
  `id` int(11) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `fieldlabel` varchar(255) NOT NULL,
  `fieldtype` varchar(255) NOT NULL,
  `mandatory` tinyint(1) DEFAULT NULL,
  `block` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `modulefield` varchar(255) NOT NULL,
  `operator` varchar(255) NOT NULL,
  `field_sequence` int(11) NOT NULL,
  `listview` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_parameters`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_parameters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `isgeneral` tinyint(1) NOT NULL,
  `action` int(11) DEFAULT NULL,
  `parent_parameter` int(11) DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_results`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_results` (
  `userid` int(11) NOT NULL,
  `crmid` int(11) NOT NULL,
  `entity` char(15) NOT NULL,
  `selected` tinyint(4) NOT NULL,
  `blockid` int(11) NOT NULL,
  KEY `userid` (`userid`,`selected`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_results_config`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_results_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `select_query` text,
  `cond_query` text,
  `where_query` text,
  `userid` int(11) NOT NULL,
  `blockid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboardbuilder_tabs`
--

CREATE TABLE IF NOT EXISTS `dashboardbuilder_tabs` (
  `id` int(11) NOT NULL,
  `tab_label` varchar(255) NOT NULL,
  `tab_sequence` varchar(255) NOT NULL,
  `tab_roles` text NOT NULL,
  `tab_users` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;