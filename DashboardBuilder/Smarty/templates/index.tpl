<script type="text/javascript" src="include/kendo/js/jquery.min.js"></script>
<script type="text/javascript">jQuery.noConflict();</script>
<link href="include/kendo/styles/kendo.common.css" rel="stylesheet" />
<link href="include/kendo/styles/kendo.default.css" rel="stylesheet" />
<script type="text/javascript" src="include/kendo/js/kendo.web.js"></script>
<script type="text/javascript" src="modules/DashboardBuilder/js/eCombobox.js"></script>
<link rel="stylesheet" type="text/css" href="modules/{$MODULE}/styles/jquery-ui.css" />
<script type="text/javascript" src="modules/{$MODULE}/js/jquery-ui.js"></script>
{*<!--<script src="include/kendoui/js/kendo.web.js"></script>*}
{include file='Buttons_List1.tpl'}

<script>
    {literal}
		jQuery(document).ready(function() {
			jQuery("#accordionBuilder").accordion({clearStyle: true}, {collapsible: true}, {active: false});
			jQuery("#accordionExtension").accordion({clearStyle: true});
		});
    {/literal}
</script>

<body>
    <div class="k-content" style="width:80%; margin-left: 5%;">
		<div id="accordionExtension" style="height:350px;">
			<h3>Dashboard Configuration </h3>
			<div id="extensionGrid" style="height:300px;"> </div>
			<input type="hidden" id="dsName" name="dsName">
		</div>

		<div id="accordionBuilder" style="display: none">
			<h3> Tab Configuration</h3>
			<div id="tabGrid" style="display:table-cell;height:200px;"></div>
			<h3> Block Configuration</h3>
			<div id="blockGrid" style="display:table-cell;height:200px;"></div>
			<h3> Fields Configuration</h3>
			<div id="fieldGrid" style="display:table-cell;height:200px;"></div>
			<h3> Actions Configuration</h3>
			<div id="actionGrid" style="display:table-cell;height:200px;"></div>
			<h3> Parameters Configuration </h3>
			<div id="parameterGrid" style="display:table-cell;height:200px;"></div>
			<h3> Fields Parameters Configuration </h3>
			<div id="fieldparamGrid" style="display:table-cell;height:200px;"></div>
		</div>
    </div>
	<script>
		{literal}
			actionURL = "index.php?module=DashboardBuilder&action=DashboardBuilderAjax&file=parameters";
			var parenttablist = new kendo.data.DataSource({
				transport: {
					read: {
						url: actionURL + '&kaction=parenttablist',
						dataType: "json"
					}
				},
				serverFiltering: true
			});
			var roleslist = new kendo.data.DataSource({
				transport: {
					read: {
						url: actionURL + '&kaction=roleslist',
						dataType: "json"
					}
				},
				schema: {
					model: {
						id: "id",
						fields: {
							rolename: {type: "string"}
						}
					}
				}
			});

			var userslist = new kendo.data.DataSource({
				transport: {
					read: {
						url: actionURL + '&kaction=userslist',
						dataType: "json"
					}
				},
				schema: {
					model: {
						id: "id",
						fields: {
							username: {type: "string"}
						}
					}
				}
			});

			var extensiontype = [
				{text: "Dashboard", value: "dashboard"},
				{text: "MassiveLauncher", value: "massivelauncher"},
			];
			var templatetype = [
				{text: "Default", value: "default"},
				{text: "AngularJS", value: "angular"},
			];
			var fieldtype = [
				{text: "Textbox", value: "textbox"},
				{text: "Checkbox", value: "checkbox"},
				{text: "Date", value: "date"},
				{text: "Picklist", value: "picklist"},
				{text: "Filter", value: "filter"},
				{text: "UItype10", value: "uitype10"}
			];
			var fieldoperator = [
				{text: "=", value: "eq"},
				{text: "<>", value: "neq"},
				{text: ">=", value: "ge"},
				{text: "<=", value: "le"}
			];
// Show Tab GRID 
// Show Extensions GRID
			jQuery("#extensionGrid").kendoGrid({
				dataSource: {
					transport: {
						read: {
							url: actionURL + '&kaction=retrieveextension',
							dataType: "json"
						},
						update: {
							url: actionURL + '&kaction=updateextension',
							dataType: "json",
							type: "POST",
							complete: function(e) {
								jQuery("#extensionGrid").data("kendoGrid").dataSource.read();
							}
						},
						destroy: {
							url: actionURL + '&kaction=deleteextension',
							dataType: "json",
							type: "POST",
							complete: function(e) {
								//jQuery("blockGrid").data("kendoGrid").dataSource.read();
							}
						},
						create: {
							url: actionURL + '&kaction=createextension',
							dataType: "json",
							type: "POST",
							complete: function(e) {
								jQuery("#extensionGrid").data("kendoGrid").dataSource.read();
							}
						},
						parameterMap: function(options, operation) {
							if (operation !== "read" && options.models) {
								return {models: kendo.stringify(options.models)};
							}
						}
					},
					batch: true,
					pageSize: 10,
					schema: {
						model: {
							id: "id",
							fields: {
								id: {editable: false},
								name: {type: "string",
									validation: {required: {message: "Extension Name is required!"}
									}
								},
								label: {type: "string",
									validation: {required: {message: "Extension label is required!"}
									}
								},
								type: {type: "string"},
								template: {type: "string"},
								parenttab: {type: "string"},
								generated: {editable: false}
							}
						}
					}
				},
				pageable: true,
				selectable: true,
				change: function() {
					var gview = jQuery("#extensionGrid").data("kendoGrid");
					var selectedItem = gview.dataItem(gview.select());
					var selectedDashboard = selectedItem.name;
					jQuery("#dsName").val("" + selectedDashboard);
					actionURL += "&dsName=" + selectedDashboard;
					jQuery("#accordionBuilder").hide();
					jQuery("#accordionBuilder").show();
					loadAllData();


					//loadAllData();                                  
				},
				toolbar: ["create"],
				columns: [
					{field: "name", title: "Extension Name"},
					{field: "label", title: "Extension Label"},
					{field: "type", title: "Extension Type", editor:
								function(container, options) {
									jQuery('<input name="' + options.field + '">').appendTo(container).kendoComboBox({
										dataTextField: "text",
										dataValueField: "text",
										dataSource: extensiontype

									});
								}},
					{field: "template", title: "Template Type", editor:
								function(container, options) {
									jQuery('<input name="' + options.field + '">').appendTo(container).kendoComboBox({
										dataTextField: "text",
										dataValueField: "text",
										dataSource: templatetype

									});
								}},
					{field: "parenttab", title: "ParentTab", editor:
								function(container, options) {
									jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
										dataSource: {transport: {
												read: {
													url: actionURL + '&kaction=parenttablist',
													dataType: "json"
												}
											}
										},
										dataTextField: "name",
										dataValueField: "name",
										optionLabel: "Choose ParentTab",
										autoBind: false

									});
								}},
					{field: "generated", title: "Generated"},
					{command: ["edit", "destroy", {text: "Load", click: generateExtension, name: "run-script"}], title: "&nbsp;", width: "300px"}],
				editable: {
					mode: "popup",
					confirmation: "Are you sure you want to delete this dashboard?"
				},
				dataBound: function() {
					var grid = this;
					var model;
					//var dataItem = grid.dataItem(jQuery(e.currentTarget).closest("tr"));
					grid.tbody.find("tr").each(function() {
						model = grid.dataItem(this);
						if (model.generated == "1") {
							jQuery(this).find(".k-grid-edit").remove();
							jQuery(this).find(".k-grid-delete").remove();
							jQuery(this).find(".k-grid-run-script").remove();

						}
					});
				}

			});
// End Extensions GRID
// 
			function generateExtension(e) {
				kendo.ui.progress(jQuery("#extensionGrid"), true);
				var dataItem = this.dataItem(jQuery(e.currentTarget).closest("tr"));
				name = dataItem.name;
				parentName = dataItem.parenttab;
				type = dataItem.type;
				template = dataItem.template;
				jQuery.ajax({
					url: "index.php?module=DashboardBuilder&action=DashboardBuilderAjax&file=generate&extensionName=" + name + "&parentName=" + parentName + "&extensionType=" + type + "&templateType=" + template,
					context: document.body
				}).done(function(response) {
					var rsp = jQuery.parseJSON(response);
					alert(rsp);
					jQuery("#extensionGrid").data("kendoGrid").dataSource.read();
					kendo.ui.progress(jQuery("#extensionGrid"), false);
				});
			}
			function loadAllData() {
				var dtsource = new kendo.data.DataSource({
					transport: {
						read: {
							url: actionURL + '&kaction=modulelist',
							dataType: "json"
						}
					},
					serverFiltering: true
				});
				var dtsourceField = new kendo.data.DataSource({
					transport: {
						read: {
							url: actionURL + '&kaction=relatedfields',
							dataType: "json"
						}
					},
					serverFiltering: true
				});
				var datasourcebusinessactionslist = new kendo.data.DataSource({
					transport: {
						read: {
							url: actionURL + '&kaction=businessactionslist',
							dataType: "json"
						}
					},
					serverFiltering: true
				});
				jQuery("#tabGrid").kendoGrid({
					dataSource: {
						transport: {
							read: {
								url: actionURL + '&kaction=retrievetab&entityName=Tabs',
								dataType: "json"
							},
							update: {
								url: actionURL + '&kaction=updatetab&entityName=Tabs',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#tabGrid").data("kendoGrid").dataSource.read();
								}
							},
							destroy: {
								url: actionURL + '&kaction=deletetab&entityName=Tabs',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#tabGrid").data("kendoGrid").dataSource.read();
								}
							},
							create: {
								url: actionURL + '&kaction=createtab&entityName=Tabs',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#tabGrid").data("kendoGrid").dataSource.read();
								}
							},
							parameterMap: function(options, operation) {
								if (operation !== "read" && options.models) {
									return {models: kendo.stringify(options.models)};
								}
							}
						},
						batch: true,
						pageSize: 10,
						schema: {
							model: {
								id: "id",
								fields: {
									id: {editable: false},
									tab_label: {type: "string",
										validation: {required: {message: "Tab Label is required!"}
										}
									},
									tab_sequence: {type: "number"},
									tab_roles: {type: "string"},
									tab_users: {type: "string"}
								}
							}
						}
					},
					pageable: true,
					toolbar: ["create"],
					columns: [
						{field: "tab_label", title: "Tab Label"},
						{field: "tab_sequence", title: "Tab Sequence"},
						{field: "tab_roles", title: "Roles", editor:
									function(container, options) {

										jQuery('<input name="' + options.field + '"/>').appendTo(container).kendoComboBox({
											dataTextField: "rolename",
											dataValueField: "rolename",
											dataSource: roleslist,
										});
									}},
						{field: "tab_users", title: "Users", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '"/>').appendTo(container).kendoComboBox({
											dataSource: userslist,
											dataTextField: "username",
											dataValueField: "username"

										});
									}},
						{command: ["edit", "destroy"], title: "&nbsp;", width: "170px"}],
					editable: {
						mode: "popup",
						confirmation: "Are you sure you want to delete this tab?"
					}
				});
// End Tab GRID 

// Show Block GRID 
				jQuery("#blockGrid").kendoGrid({
					dataSource: {
						transport: {
							read: {
								url: actionURL + '&kaction=retrieveblock&entityName=Blocks',
								dataType: "json"
							},
							update: {
								url: actionURL + '&kaction=updateblock&entityName=Blocks',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#blockGrid").data("kendoGrid").dataSource.read();
								}
							},
							destroy: {
								url: actionURL + '&kaction=deleteblock&entityName=Blocks',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("blockGrid").data("kendoGrid").dataSource.read();
								}
							},
							create: {
								url: actionURL + '&kaction=createblock&entityName=Blocks',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#blockGrid").data("kendoGrid").dataSource.read();
								}
							},
							parameterMap: function(options, operation) {
								if (operation !== "read" && options.models) {
									return {models: kendo.stringify(options.models)};
								}
							}
						},
						batch: true,
						pageSize: 10,
						schema: {
							model: {
								id: "id",
								fields: {
									id: {editable: false},
									block_label: {type: "string",
										validation: {required: {message: "Block Label is required!"}
										}
									},
									block_sequence: {type: "number"},
									block_module: {type: "string", validation: {required: true}},
									block_action: {type:"string"},
									block_tab: {type: "string", validation: {required: true}},
									block_roles: {type: "string"},
									block_users: {type: "string"},

								}
							}
						}
					},
					pageable: true,
					toolbar: ["create"],
					columns: [
						{field: "block_label", title: "Block Label"},
						{field: "block_sequence", title: "Block Sequence"},
						{field: "block_module", title: "Module", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=modulelist',
														dataType: "json"
													}
												}
											},
											dataTextField: "name",
											dataValueField: "name",
											optionLabel: "Choose module",
											autoBind: false

										});
									}},
						{field: "block_action", title: "Action", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=actionlist',
														dataType: "json"
													}
												}
											},
											dataTextField: "actionname",
											dataValueField: "id",
											optionLabel: "Choose Action",
											autoBind: false

										});
									}},
						{field: "block_tab", title: "Tab", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=tablist',
														dataType: "json"
													}
												}
											},
											dataTextField: "tabname",
											dataValueField: "tabname",
											optionLabel: "Choose tab",
											autoBind: false

										});
									}},
						{field: "block_roles", title: "Roles", editor:
									function(container, options) {

										jQuery('<input name="' + options.field + '"/>').appendTo(container).kendoComboBox({
											dataSource: roleslist,
											dataTextField: "rolename",
											dataValueField: "rolename"

										});
									}},
						{field: "block_users", title: "Users", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '"/>').appendTo(container).kendoComboBox({
											dataSource: userslist,
											dataTextField: "username",
											dataValueField: "username"

										});
									}},
						{command: ["edit", "destroy"], title: "&nbsp;", width: "170px"}],
					editable: {
						mode: "popup",
						confirmation: "Are you sure you want to delete this block?"
					}

				});
// End Block GRID

// Show Field GRID 
				jQuery("#fieldGrid").kendoGrid({
					dataSource: {
						transport: {
							read: {
								url: actionURL + '&kaction=retrievefield&entityName=Fields',
								dataType: "json"
							},
							update: {
								url: actionURL + '&kaction=updatefield&entityName=Fields',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#fieldGrid").data("kendoGrid").dataSource.read();
								}
							},
							destroy: {
								url: actionURL + '&kaction=deletefield&entityName=Fields',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#fieldGrid").data("kendoGrid").dataSource.read();
								}
							},
							create: {
								url: actionURL + '&kaction=createfield&entityName=Fields',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#fieldGrid").data("kendoGrid").dataSource.read();
								}
							},
							parameterMap: function(options, operation) {
								if (operation !== "read" && options.models) {
									return {models: kendo.stringify(options.models)};
								}
							}
						},
						batch: true,
						pageSize: 10,
						schema: {
							model: {
								id: "id",
								fields: {
									id: {editable: false},
									fieldname: {type: "string",
										validation: {required: {message: "Fieldname is required!"}
										}
									},
									fieldlabel: {type: "string",
										validation: {required: {message: "Fieldlabel is required!"}
										}
									},
									fieldtype: {type: "string"},
									mandatory: {type: "boolean"},
									block: {type: "string"},
									module: {type: "string", validation: {required: true}},
									modulefield: {type: "string", validation: {required: true}},
									operator: {type: "string"},
									field_sequence: {type: "number"},
									listview: {type: "boolean"},
								}
							}
						}
					},
					pageable: true,
					toolbar: ["create"],
					columns: [
						{field: "fieldname", title: "Field Name"},
						{field: "fieldlabel", title: "Field Label"},
						{field: "fieldtype", title: "Field Type", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoComboBox({
											dataTextField: "text",
											dataValueField: "text",
											dataSource: fieldtype

										});
									}},
						{field: "mandatory", title: "Mandatory", width: "80px", template: '<input type="checkbox" id="mandatory" #= mandatory ? checked="checked" : "" # disabled="disabled" />'},
						{field: "block", title: "Block", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '"/>').appendTo(container).kendoDropDownList({
											dataSource: {
												transport: {
													read: {
														url: actionURL + '&kaction=blocklist',
														dataType: "json"
													}
												}
											},
											change: function(e) {
												var blockId = this.value();
												dtsource.filter({field: "block", operator: "eq", value: blockId});
											},
											dataTextField: "blockname",
											dataValueField: "id",
											optionLabel: "Choose block",
											autoBind: false

										});
									}},
						{field: "module", title: "Module", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '" />').appendTo(container).kendoComboBox({
											change: function(e) {
												var moduleId = this.value();
												dtsourceField.filter({field: "module", operator: "eq", value: moduleId});
											},
											dataSource: dtsource,
											dataTextField: "name",
											dataValueField: "id",
											serverFiltering: true,
											optionLabel: "Choose module",
											autoBind: false

										});
									}},
						{field: "modulefield", title: "Module Field", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: dtsourceField,
											dataTextField: "fieldname",
											dataValueField: "fieldname",
											optionLabel: "Choose field",
											autoBind: false

										});
									}},
						{field: "operator", title: "Operator", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataTextField: "text",
											dataValueField: "text",
											dataSource: fieldoperator

										});
									}},
						{field: "field_sequence", title: "Field Sequence"},
						{field: "listview", title: "Display in ListView"},
						{command: ["edit", "destroy"], title: "&nbsp;", width: "170px"}],
					editable: {
						mode: "popup",
						confirmation: "Are you sure you want to delete this field?"
					}

				});
// End Field GRID

// Show Action GRID
				jQuery("#actionGrid").kendoGrid({
					dataSource: {
						transport: {
							read: {
								url: actionURL + '&kaction=retrieveaction&entityName=Actions',
								dataType: "json"
							},
							update: {
								url: actionURL + '&kaction=updateaction&entityName=Actions',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#actionGrid").data("kendoGrid").dataSource.read();
								}
							},
							destroy: {
								url: actionURL + '&kaction=deleteaction&entityName=Actions',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									//jQuery("blockGrid").data("kendoGrid").dataSource.read();
								}
							},
							create: {
								url: actionURL + '&kaction=createaction&entityName=Actions',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#actionGrid").data("kendoGrid").dataSource.read();
								}
							},
							parameterMap: function(options, operation) {
								if (operation !== "read" && options.models) {
									return {models: kendo.stringify(options.models)};
								}
							}
						},
						batch: true,
						pageSize: 10,
						schema: {
							model: {
								id: "id",
								fields: {
									id: {editable: false},
									name: {type: "string",
										validation: {required: {message: "Action Name is required!"}
										}
									},
									label: {type: "string",
										validation: {required: {message: "Action label is required!"}
										}
									},
									block: {type: "string"},
									autoload: {type: "boolean"},
									sequence: {type: "number"}
								}
							}
						}
					},
					pageable: true,
					toolbar: ["create"],
					columns: [
						{field: "name", title: "Action Name", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: datasourcebusinessactionslist,
											dataTextField: "name",
											dataValueField: "id",
											serverFiltering: true,
											optionLabel: "Choose Action",
											autoBind: false

										});
									}},
						{field: "label", title: "Action Label"},
						{field: "block", title: "Block", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=blocklist',
														dataType: "json"
													}
												}
											},
											dataTextField: "blockname",
											dataValueField: "blockname",
											optionLabel: "Choose block",
											autoBind: false

										});
									}},
						{field: "autoload", title: "Auto Loading", width: "80px", template: '<input type="checkbox" id="autoload" #= autoload ? checked="checked" : "" # disabled="disabled" />'},
						{field: "sequence", title: "Sequence"},
						{command: ["edit", "destroy"], title: "&nbsp;", width: "170px"}],
					editable: {
						mode: "popup",
						confirmation: "Are you sure you want to delete this action?"
					}

				});
// End Action GRID
// Show Parameters GRID                        
				jQuery("#parameterGrid").kendoGrid({
					dataSource: {
						transport: {
							read: {
								url: actionURL + '&kaction=retrieveparameter&entityName=Parameters',
								dataType: "json"
							},
							update: {
								url: actionURL + '&kaction=updateparameter&entityName=Parameters',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#parameterGrid").data("kendoGrid").dataSource.read();
								}
							},
							destroy: {
								url: actionURL + '&kaction=deleteparameter&entityName=Parameters',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									//jQuery("blockGrid").data("kendoGrid").dataSource.read();
								}
							},
							create: {
								url: actionURL + '&kaction=createparameter&entityName=Parameters',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#parameterGrid").data("kendoGrid").dataSource.read();
								}
							},
							parameterMap: function(options, operation) {
								if (operation !== "read" && options.models) {
									return {models: kendo.stringify(options.models)};
								}
							}
						},
						batch: true,
						pageSize: 10,
						schema: {
							model: {
								id: "id",
								fields: {
									id: {editable: false},
									name: {type: "string",
										validation: {required: {message: "Parameter Name is required!"}
										}
									},
									label: {type: "string",
										validation: {required: {message: "Parameter label is required!"}
										}
									},
									isgeneral: {type: "boolean"},
									action: {type: "string"},
									parent_parameter:{type:"string"},
									sequence: {type: "number"}
								}
							}
						}
					},
					pageable: true,
					toolbar: ["create"],
					columns: [
						{field: "name", title: "Parameter Name"},
						{field: "label", title: "Parameter Label"},
						{field: "isgeneral", title: "General", width: "80px", template: '<input type="checkbox" id="isgeneral" #= isgeneral ? checked="checked" : "" # disabled="disabled" />'},
						{field: "action", title: "Action", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=actionlist',
														dataType: "json"
													}
												}
											},
											dataTextField: "actionname",
											dataValueField: "id",
											optionLabel: "Choose Action",
											autoBind: false

										});
									}},
						{field: "parent_parameter", title: "Parent Parameter", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=parentparameterlist',
														dataType: "json"
													}
												}
											},
											dataTextField: "parametername",
											dataValueField: "parametername",
											optionLabel: "Choose Parameter",
											autoBind: false

										});
									}},
						{field: "sequence", title: "Sequence"},
						{command: ["edit", "destroy"], title: "&nbsp;", width: "170px"}],
					editable: {
						mode: "popup",
						confirmation: "Are you sure you want to delete this parameter?"
					}

				});
//End Parameters GRID
//Show Fields Parameters GRID
				jQuery("#fieldparamGrid").kendoGrid({
					dataSource: {
						transport: {
							read: {
								url: actionURL + '&kaction=retrievefieldparam&entityName=FieldParams',
								dataType: "json"
							},
							update: {
								url: actionURL + '&kaction=updatefieldparam&entityName=FieldParams',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#fieldparamGrid").data("kendoGrid").dataSource.read();
								}
							},
							destroy: {
								url: actionURL + '&kaction=deletefieldparam&entityName=FieldParams',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									//jQuery("blockGrid").data("kendoGrid").dataSource.read();
								}
							},
							create: {
								url: actionURL + '&kaction=createfieldparam&entityName=FieldParams',
								dataType: "json",
								type: "POST",
								complete: function(e) {
									jQuery("#fieldparamGrid").data("kendoGrid").dataSource.read();
								}
							},
							parameterMap: function(options, operation) {
								if (operation !== "read" && options.models) {
									return {models: kendo.stringify(options.models)};
								}
							}
						},
						batch: true,
						pageSize: 10,
						schema: {
							model: {
								id: "id",
								fields: {
									id: {editable: false},
									name: {type: "string",
										validation: {required: {message: "Fieldname is required!"}
										}
									},
									label: {type: "string",
										validation: {required: {message: "Fieldlabel is required!"}
										}
									},
									input_name: {type: "string"},
									type: {type: "string"},
									defaul_value: {type: "string"},
									parameter: {type: "string"},
									module: {type: "string"},
									modulefield: {type: "string"},
									sequence: {type: "number"}

								}
							}
						}
					},
					pageable: true,
					toolbar: ["create"],
					columns: [
						{field: "name", title: "Field Name"},
						{field: "label", title: "Field Label"},
						{field: "input_name", title: "Input Name"},
						{field: "type", title: "Field Type", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataTextField: "text",
											dataValueField: "text",
											dataSource: fieldtype

										});
									}},
						{field: "defaul_value", title: "Default"},
						{field: "parameter", title: "Parameter", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=parameterlist',
														dataType: "json"
													}
												}
											},
											dataTextField: "parametername",
											dataValueField: "parametername",
											optionLabel: "Choose Parameter",
											autoBind: false

										});
									}},
						{field: "module", title: "Module", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '" />').appendTo(container).kendoDropDownList({
											dataSource: {transport: {
													read: {
														url: actionURL + '&kaction=modulelist',
														dataType: "json"
													}
												}
											},
											change: function(e) {
												moduleId = this.value();
												dtsourceField.filter({field: "module", operator: "eq", value: moduleId});
											},
											dataTextField: "name",
											dataValueField: "id",
											optionLabel: "Choose module"


										});
									}},
						{field: "modulefield", title: "Module Field", editor:
									function(container, options) {
										jQuery('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
											dataSource: dtsourceField,
											dataTextField: "fieldname",
											dataValueField: "fieldname",
											optionLabel: "Choose field",
											autoBind: false

										});
									}},
						{field: "sequence", title: "Sequence"},
						{command: ["edit", "destroy"], title: "&nbsp;", width: "170px"}],
					editable: {
						mode: "popup",
						confirmation: "Are you sure you want to delete this field?"
					}

				});
//End Fields Parameters GRID
				jQuery("#tabGrid").data("kendoGrid").dataSource.read();
				jQuery("#blockGrid").data("kendoGrid").dataSource.read();
				jQuery("#fieldGrid").data("kendoGrid").dataSource.read();
				jQuery("#actionGrid").data("kendoGrid").dataSource.read();
				jQuery("#parameterGrid").data("kendoGrid").dataSource.read();
				jQuery("#fieldparamGrid").data("kendoGrid").dataSource.read();

			}
		{/literal}
	</script>
</body>
