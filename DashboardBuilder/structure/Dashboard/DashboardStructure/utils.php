<?php
function findFilters($mod){
  global $adb;
$filterarray=array();
$queryfilter=$adb->pquery("Select * from vtiger_customview where entitytype=? ORDER BY cvid",array($mod));
while($queryfilter && $r=$adb->fetch_array($queryfilter)){
    $filterarray[$r['cvid']]=$r['viewname'];
}
return $filterarray;
}
function findPicklistValues($fieldname){
  global $adb;
 $picklistarray=array(); 
 $query=$adb->query("SELECT * from vtiger_$fieldname WHERE presence=1");
while($query && $row=$adb->fetch_array($query)){
    $picklistarray[$row[$fieldname."id"]]=$row["$fieldname"];
}
return $picklistarray;
}

function findRecords($modulename, $filtername) {
	global $adb, $current_user, $log;
	include_once "modules/$modulename/$modulename.php";
	$moduleinstance = CRMEntity::getInstance($modulename);
	$indexid = $moduleinstance->table_index;

	$allrecordids = array();
	$queryGenerator_ent = new QueryGenerator($modulename, $current_user);
	$query = $adb->query($queryGenerator_ent->getCustomViewQueryById($filtername));
	while ($query && $f = $adb->fetch_array($query)) {
		array_push($allrecordids, $f[$indexid]);
	}
	return $allrecordids;
}

?>