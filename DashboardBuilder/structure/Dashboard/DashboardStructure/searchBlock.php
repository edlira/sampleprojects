<?php

require_once('Smarty_setup.php');
require_once('data/CRMEntity.php');
require_once('include/utils/CommonUtils.php');
require_once('include/ListView/ListView.php');
require_once('include/utils/utils.php');
require_once('modules/CustomView/CustomView.php');
require_once("modules/$currentModule/utils.php");
$smarty = new vtigerCRM_Smarty;

include_once("modules/$currentModule/$currentModule.php");
global $adb, $log, $current_user, $currentModule, $category, $thementity, $image_path, $mod_strings, $app_strings, $list_max_entries_per_page;
$ds_blockid = $_REQUEST['ds_blockid'];

$dashboardStructure = new $currentModule();
$dashboardStructure->id = $ds_blockid;
$table = $dashboardStructure->table;
//$searchTable = $table . "results_" . $ds_blockid . "_" . $current_user->id;
$searchTable = $table . "results";
$configTable = $searchTable . "_config";

$adb->pquery("DELETE from $configTable WHERE userid=? AND blockid=?", array($current_user->id, $ds_blockid));

$listQuery = $dashboardStructure->getListViewQuery($ds_blockid, '', $_REQUEST);
$resultQuery = $dashboardStructure->getResultQuery($ds_blockid, "", $configTable);
$currentSearchTabName = getTabname($dashboardStructure->moduleid);
// Searching...
// delete previous search results
$adb->pquery("DELETE FROM $searchTable WHERE userid=? AND blockid=?", array($current_user->id, $ds_blockid));

$insertQuery = "INSERT INTO $searchTable (userid, selected, crmid, entity,blockid) ";
$result = $adb->query($insertQuery . ' ( ' . $listQuery . ' )');

$fields = $resultQuery['fields'];
$fieldArray = array();
$columnsArray = array('selected_id' . $ds_blockid => array('type' => 'string'),
	'recordid' => array('type' => 'string'),
	'recordname' => array('type' => 'string')
);

$fieldArray[] = array('field' => 'selected_id' . $ds_blockid, 'title' => '', 'width' => '50px',
	'template' => "<input name='selected_id$ds_blockid' type='checkbox' # if(selected_id$ds_blockid==='1') {# checked #}# onclick='check_object(this,\"$ds_blockid\");' value='#=recordid#'>",
	'headerTemplate' => "<input type='checkbox' name='selectall' id='selectall' onclick='toggleSelectAllGrid(this.checked,\"selected_id$ds_blockid\",\"$ds_blockid\")'>");
//$fieldArray[] = array('field' => 'recordid', 'title' => 'Title', 'template' => '<a href="index.php?module=#=vtmodule#&action=DetailView&record=#=recordid#" target="_new">#=recordname#</a>');
$fieldArray[] = array('field' => 'recordname', 'title' => 'Title', 'template' => '<a href="index.php?module=#=vtmodule#&action=DetailView&record=#=recordid#" target="_new">#=recordname#</a>');
$fieldArray[] = array('field' => 'vtmodule', 'hidden' => true);
foreach ($fields as $fldElement) {
	$fldname = $fldElement['fieldname'];
	$fldlabel = $fldElement['fieldlabel'];
	$fldmodule = $fldElement['module'];
	if($fldmodule==$currentSearchTabName) {
	$fieldArray[] = array('field' => $fldname, 'title' => "$fldlabel");
	} else {
	$fieldArray[] = array('field' => $fldname, 'title' => "$fldlabel",'template' => '<a href="index.php?module=#=relmodule#&action=DetailView&record=#=relrecordid#" target="_new">#=relrecordname#</a>');
	}
	$columnsArray[$fldname] = array('type' => 'string');
}

$columnInfo = json_encode($fieldArray);
$fieldInfo = json_encode($columnsArray);
$arr = array();
$groups = json_encode($arr);

//Parameters

$blockParameters = $dashboardStructure->getBlockParameters();
//var_dump($blockParameters[2]["paramfields"]);
$smarty->assign('PARAMETERS', $blockParameters);
//var_dump($blockParameters);
$qtot = "select count(*) from $searchTable where userid=" . $current_user->id." AND blockid=".$ds_blockid;
$total = $adb->query_result($adb->query($qtot), 0, 0);
$smarty->assign('hideSelectAll', ($total > $list_max_entries_per_page ? 'false' : 'true'));

$smarty->assign('fields', $fieldInfo);
$smarty->assign('PAGESIZE', $list_max_entries_per_page);
$smarty->assign('columns', $columnInfo);
$smarty->assign('groups', $groups);
$smarty->assign('ds_table', $searchTable);
$smarty->assign('ds_blockid', $ds_blockid);
$smarty->assign('ds_userid', $current_user->id);
$smarty->assign('MODULE', $currentModule);
$smarty->assign('SINGLE_MOD', getTranslatedString('SINGLE_' . $currentModule));
$smarty->assign('CATEGORY', $category);
$smarty->assign('THEME', $theme);
$smarty->assign('IMAGE_PATH', $image_path);
$smarty->assign("MOD", $mod_strings);
$smarty->assign("APP", $app_strings);
$dat_fmt = $current_user->date_format;
$smarty->assign("dateStr", $dat_fmt);
$smarty->assign("dateFormat", (($dat_fmt == 'dd-mm-yyyy') ? 'dd-mm-yy' : (($dat_fmt == 'mm-dd-yyyy') ? 'mm-dd-yy' : (($dat_fmt == 'yyyy-mm-dd') ? 'yy-mm-dd' : ''))));
$smarty->assign("CALENDAR_LANG", $app_strings['LBL_JSCALENDAR_LANG']);
$smarty->display("modules/$currentModule/ResultView.tpl");
?>
