<?php

/* * ***********************************************************************************************
 * Copyright 2012-2013 OpenCubed  --  This file is a part of vtMktDashboard.
 * You can copy, adapt and distribute the work under the "Attribution-NonCommercial-ShareAlike"
 * Vizsage Public License (the "License"). You may not use this file except in compliance with the
 * License. Roughly speaking, non-commercial users may share and modify this code, but must give credit
 * and share improvements. However, for proper details please read the full License, available at
 * http://vizsage.com/license/Vizsage-License-BY-NC-SA.html and the handy reference for understanding
 * the full license at http://vizsage.com/license/Vizsage-Deed-BY-NC-SA.html. Unless required by
 * applicable law or agreed to in writing, any software distributed under the License is distributed
 * on an  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the
 * License terms of Creative Commons Attribution-NonCommercial-ShareAlike 3.0 (the License).
 * ************************************************************************************************
 *  Module       : DashboardStructure
 *  Version      : 1.9
 *  Author       : OpenCubed
 * *********************************************************************************************** */
global $adb, $currentModule, $current_user, $log;

if (isset($_REQUEST['tblname']) && !empty($_REQUEST['tblname'])) {
	$ds_block = vtlib_purify($_REQUEST['ds_block']);
	$tblname = vtlib_purify($_REQUEST['tblname']);
	$configtable = $tblname . "_config";
	$respuesta = crudSelected($ds_block, $tblname, $configtable, $_REQUEST);
	echo json_encode($respuesta);
}

function crudSelected($ds_block, $tblname, $configtable, $req) {
	global $adb, $currentModule, $current_user, $log, $default_charset;
	$respuesta = array();
	switch ($req['exec']) {
		case 'List':
			$conditions = '';
			if (!empty($req['filter']['filters']) and is_array($req['filter']['filters'])) {
				foreach ($req['filter']['filters'] as $ftr) {
					if (empty($ftr['value']))
						continue;
					$fval = addslashes($ftr['value']);
					$fval = $fval == 'true' ? 1 : 0;
					$conditions.=' and selected ';  // .$ftr['field']
					switch ($ftr['operator']) {
						case 'eq':
							$op = '=';
							break;
						case 'neq':
							$op = '!=';
							break;
						case 'startswith':
							$fval = $fval . '%';
							$op = 'like';
							break;
						case 'endswith':
							$fval = '%' . $fval;
							$op = 'like';
							break;
						case 'contains':
							$op = 'like';
							$fval = '%' . $fval . '%';
							break;
						default: $op = '=';
							break;
					}
					$conditions.=" $op '$fval'";
				}
			}
			$qemp = "SELECT selected, crmid, entity FROM $tblname
					WHERE userid=" . $current_user->id . " AND blockid=" . $ds_block . $conditions;
			$qtot = "select count(*) FROM $tblname
					WHERE userid=" . $current_user->id . " AND blockid=" . $ds_block . $conditions;
			$total = $adb->query_result($adb->query($qtot), 0, 0);

			$configQuery = $adb->pquery("SELECT * FROM $configtable WHERE userid=? AND blockid=?", array($current_user->id, $ds_block));
			$selectquery = $adb->query_result($configQuery, 0, 1);
			$condquery = $adb->query_result($configQuery, 0, 2);
			$wherequery = $adb->query_result($configQuery, 0, 3);
			/* 		if (isset($req['sort'])) {
			$qemp.=' order by ';
			$ob='';
			foreach ($req['sort'] as $sf) {
				if ($sf['field']=='apellidos') {
					$ob.='lastname1 '.$sf['dir'].',lastname2 '.$sf['dir'].',';
				} else {
					$ob.=$sf['field'].' '.$sf['dir'].',';
				}
			}
				$qemp.=trim($ob,',');
			} */
			$qemp.= ' ORDER BY crmid ';
			if (isset($req['page']) and isset($req['pageSize'])) {
				$qemp.=' limit ' . (($req['page'] - 1) * $req['pageSize']) . ', ' . $req['pageSize'];
			}
			$rssr = $adb->query($qemp);
			while ($sr = $adb->fetch_array($rssr)) {
				$mod = $sr['entity'];
				$sql = $selectquery . " " . $condquery . " WHERE " . $wherequery . $sr['crmid'];


				//echo $sql;
				$rsent = $adb->query($sql);
				if ($rsent) {
					$ent = $adb->fetch_array($rsent);
				} else {
					$ent = array();
				}
				$resultsFields["selected_id$ds_block"] = $sr['selected'];
				$resultsFields["vtmodule"] = $mod;
				$resultsFields["recordid"] = $sr['crmid'];
				$modEntity = getEntityName($mod, $sr['crmid']);
				$resultsFields["recordname"] = $modEntity[$sr['crmid']];

				$nrFields = $adb->num_fields($rsent);
				for ($f = 0; $f < $nrFields; $f++) {
					$fieldDetails = $adb->field_name($rsent, $f);
					$fieldname = $fieldDetails->name;
					//Check uitype 10
					if (strpos($ent[$fieldname],"::::::") !== false) {
						$completeVal = explode("::::::",$ent[$fieldname]);
						$relmodule = $completeVal[0];
						$relid = $completeVal[1];
						$entityval = $completeVal[2];
						$resultsFields["relrecordname"] = html_entity_decode($entityval, ENT_QUOTES, $default_charset);
						$resultsFields['relmodule'] = $relmodule;
						$resultsFields['relrecordid'] = $relid;
					}
					else {
						$entityval = $ent[$fieldname];
						$resultsFields['relmodule'] = "";
						$resultsFields['relrecordid'] = "";
						$resultsFields["relrecordname"] = "";
						$resultsFields[$fieldname]=html_entity_decode($entityval, ENT_QUOTES, $default_charset);
					}
					
					//$respuesta['results'][] = $resultsFields;
				}
				$respuesta['results'][] = $resultsFields;
			}
			$respuesta['total'] = $total;
			break;
		case 'Destroy':
			break;
		case 'Update':
			$adb->pquery("update $tblname set selected=? where crmid in (" . $req['crmid'] . ') and userid=? and blockid=? ', array(($req['selid'] == 'true' || $req['selid'] == "1") ? 1 : 0, $current_user->id, $ds_block));
			break;
		case 'UpdateAll':
			$adb->pquery("update $tblname set selected=? where userid=? and blockid=?", array(($req['selid'] == 'true' || $req['selid'] == "1") ? 1 : 0, $current_user->id, $ds_block));
			break;
		case 'AreAllSelected':
			$rssel = $adb->pquery("select count(*) from $tblname where selected=0 and userid=? and blockid=?", array($current_user->id, $ds_block));
			$cnt = $adb->query_result($rssel, 0, 0);
			$respuesta = array('allselected' => ($cnt == 0 ? 1 : 0));
			break;
		case 'OneSelected':
			$rssel = $adb->pquery("select 1 from $tblname where selected=1 and userid=? and blockid=? limit 1", array($current_user->id, $ds_block));
			$respuesta = array('oneselected' => ($adb->num_rows($rssel) > 0 ? 1 : 0));
			break;
		case 'AtLeastOneEntity':
			$ent = $req['entities'];
			if (!empty($ent)) {
				$rssel = $adb->pquery("select 1 from $tblname where selected=1 and entity in (" . $ent . ') and userid=? and blockid=? limit 1', array($current_user->id, $ds_block));
				$respuesta = array('oneselected' => ($adb->num_rows($rssel) > 0 ? 1 : 0));
			} else {
				$respuesta = array('oneselected' => 0);
			}
			break;
	}
	return $respuesta;
}