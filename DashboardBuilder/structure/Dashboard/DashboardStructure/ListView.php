<?php

global $app_strings, $mod_strings, $current_language, $currentModule, $theme;
require_once('data/CRMEntity.php');
require_once('include/utils/CommonUtils.php');
require_once('include/ListView/ListView.php');
require_once('include/utils/utils.php');
require_once('modules/CustomView/CustomView.php');
require_once("modules/$currentModule/utils.php");
require_once('Smarty_setup.php');
require_once("modules/$currentModule/$currentModule.php");
ini_set('max_execution_time', 100);
//ini_set('display_errors','On');
$smarty = new vtigerCRM_Smarty;
global $adb, $log, $current_user;
$focus = new $currentModule();
$table = $focus->table;
$permitted_tabs = array();
$allRoles = getRoleSubordinates($current_user->roleid);
array_push($allRoles, $current_user->roleid);
$querytabs = $adb->pquery("SELECT * FROM $table" . "tabs dstabs
						INNER JOIN $table" . "entities ds_en ON ds_en.dsid=dstabs.id
						WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 
						ORDER BY dstabs.tab_sequence", array($currentModule, 'Tabs'));
for ($i = 0; $i < $adb->num_rows($querytabs); $i++) {
	$tabid = $adb->query_result($querytabs, $i, 'id');
	//$roles=explode(",",$adb->query_result($querytabs,$i,'tab_roles'));
	$users = explode(",", $adb->query_result($querytabs, $i, 'tab_users'));
	$roles = $adb->query_result($querytabs, $i, 'tab_roles');
	$permitted_blocks = array();

	if (in_array($roles, $allRoles) || in_array($current_user->id, $users)) {
		$tablabel = getTranslatedString($adb->query_result($querytabs, $i, 'tab_label'));
		$permitted_tabs[$tabid]['info'] = array('tab_label' => $tablabel);
		$queryblocks = $adb->pquery("SELECT * FROM $table" . "blocks dsblocks
								 INNER JOIN $table" . "entities ds_en ON ds_en.dsid=dsblocks.id
								 WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND block_tab=? 
								 ORDER BY block_sequence", array($currentModule, 'Blocks', $tabid));
		for ($j = 0; $j < $adb->num_rows($queryblocks); $j++) {
			$blockid = $adb->query_result($queryblocks, $j, 'id');

			$blockroles = $adb->query_result($queryblocks, $j, 'block_roles');
			$blockusers = explode(",", $adb->query_result($queryblocks, $j, 'block_users'));
			if (in_array($blockroles, $allRoles) || in_array($current_user->id, $blockusers)) {
				$blockname = $adb->query_result($queryblocks, $j, 'block_label');
				$blockaction=$adb->query_result($queryblocks, $j, 'block_action');
				$actionData=array();
				if(!empty($blockaction)){
					$actionQuery=$adb->pquery("SELECT dsaction.id as dsactionid, dsaction.name as actionname,dsaction.label as actionlabel,vtiger_actions.output_type
								FROM $table" . "actions dsaction
								INNER JOIN $table" . "entities ds_en ON ds_en.dsid=dsaction.id
								INNER JOIN vtiger_actions ON vtiger_actions.actionsid=dsaction.name
								WHERE ds_en.deleted=0 AND dsaction.id=?",array($blockaction));
					$actionid = $adb->query_result($actionQuery,0,'dsactionid');
					$actioname = $adb->query_result($actionQuery,0,'actionname');
					$actionlabel = $adb->query_result($actionQuery,0,'actionlabel');
					$actionoutput = $adb->query_result($actionQuery,0,'output_type');
					$actionData=array("actionid" => $actionid, 'actionname' => $actioname, 'actionlabel' => $actionlabel, 'actionoutput' => $actionoutput);
				}
				$mandatory_fields = array();
				$focus->id = $blockid;
				$all_fields = $focus->getBlockFields();
				$mandadory_fieldname = array();
				$mandatory_fieldlabel = array();
				foreach ($all_fields as $index => $info) {
					if ($info['mandatory'] == 1) {
						$mandadory_fieldname[] = $info['fieldname'];
						$mandatory_fieldlabel[] = getTranslatedString($info['fieldlabel']);
					}
				}
				$permitted_blocks[] = array('blockid' => $blockid, 'blockname' => $blockname, 'blocklabel' => getTranslatedString($blockname), 'fields' => $all_fields, 'mandatory_fieldname' => implode(",", $mandadory_fieldname), 'mandatory_fieldlabel' => implode(",", $mandatory_fieldlabel),'actiondata'=>$actionData);
			}
		}
		$permitted_tabs[$tabid]['blocks'] = $permitted_blocks;
	}
}
$smarty->assign('TABS', $permitted_tabs);
$smarty->assign('TABS_JSON', json_encode($permitted_tabs));
$smarty->assign('MODULE', $currentModule);
$smarty->assign('CATEGORY', $category);
$smarty->assign('THEME', $theme);
$smarty->assign('IMAGE_PATH', $image_path);
$smarty->assign("MOD", $mod_strings);
$smarty->assign("APP", $app_strings);
$dat_fmt = $current_user->date_format;
$smarty->assign("dateStr", $dat_fmt);
$smarty->assign("dateFormat", (($dat_fmt == 'dd-mm-yyyy') ? 'dd-mm-yy' : (($dat_fmt == 'mm-dd-yyyy') ? 'mm-dd-yy' : (($dat_fmt == 'yyyy-mm-dd') ? 'yy-mm-dd' : ''))));
$smarty->assign("CALENDAR_LANG", $app_strings['LBL_JSCALENDAR_LANG']);
$smarty->display("modules/$currentModule/index.tpl");
?>
