$.getScript("index.php?module=" + gVTModule + "&action=" + gVTModule + "Ajax&file=getjslanguage", function() {
});

function DashboardStructuresetValueFromCapture(recordid, value, target_fieldname) {
	var domnode_id = document.getElementById(target_fieldname);
	var domnode_display = document.getElementById(target_fieldname + '_display');
	if (domnode_id)
		domnode_id.value = recordid;
	if (domnode_display)
		domnode_display.value = value;
	return true;
}

function showHideStatus(sId, anchorImgId, flag) {
	oObj = eval(document.getElementById(sId));
	if (oObj.style.display == 'block') {
		oObj.style.display = 'none';
		$("#" + anchorImgId).removeClass("ui-icon-triangle-1-s");
		$("#" + anchorImgId).addClass("ui-icon-triangle-1-e");
	}
	else {
		oObj.style.display = 'block';
		$("#" + anchorImgId).removeClass("ui-icon-triangle-1-e");
		$("#" + anchorImgId).addClass("ui-icon-triangle-1-s");
	}
}

function validateMandatory(fields, labels) {
	fieldsname = fields.split(",");
	labelsname = labels.split(",");
	for (var i = 0; i < fieldsname.length; i++) {
		fldval = $("[name='" + fieldsname[i] + "']").val();
		if (trim(fldval) == '') {
			alert(labelsname[i] + alert_arr.CANNOT_BE_NONE)
			return false
		}
	}
}

function updateGridSelectAllCheckbox() {
	var index = (ds_selectedblock == 1 ? '' : ds_selectedblock);
	var all_state = false;
	var groupElements = document.getElementsByName('selected_id' + index);
	for (var i = 0; i < groupElements.length; i++) {
		if (groupElements[i].disabled)
			var state = true;
		else
			var state = groupElements[i].checked;
		if (state == false) {
			all_state = false;
			break;
		}
	}
	jQuery('#selectall' + index).attr('checked', all_state);
}

function check_object(sel_id, index, groupParentElementId) {
	$.ajax({
		url: "index.php?module="+gVTModule+"&action="+gVTModule+"Ajax&file=crudSelected&exec=Update&tblname=" + ds_selectedtable + "&selid=" + sel_id.checked + "&crmid=" + $(sel_id).val()+"&ds_block="+index,
		context: document.body
	}).done(function() {
		$.ajax({
			url: "index.php?module="+gVTModule+"&action="+gVTModule+"Ajax&file=crudSelected&exec=AreAllSelected&tblname=" + ds_selectedtable+"&ds_block="+index,
			context: document.body
		}).done(function(response) {
			var rsp = $.parseJSON(response);
			if (rsp.allselected) {
				document.getElementById("chooseAllBtn" + index).value = vtmkt_arr.UNSELECTALL;
				document.getElementById("selectallrecords" + index).value = 1;
			} else {
				document.getElementById("chooseAllBtn" + index).value = vtmkt_arr.SELECTALL;
				document.getElementById("selectallrecords" + index).value = 0;
			}
		});
	});
	updateGridSelectAllCheckbox();
}

function toggleSelectAllEntries_ListView(index) {
	var state = document.getElementById("selectallrecords" + index).value;
	var newstate = 0;
	if (state == 0) {
		//select
		document.getElementById("chooseAllBtn" + index).value = vtmkt_arr.UNSELECTALL;
		document.getElementById("selectallrecords" + index).value = 1;
		newstate = 1;
	} else {
		//unselect
		document.getElementById("chooseAllBtn" + index).value = vtmkt_arr.SELECTALL;
		document.getElementById("selectallrecords" + index).value = 0;
		newstate = 0;
	}
	jQuery.ajax({
		url: "index.php?module=" + gVTModule + "&action=" + gVTModule + "Ajax&file=crudSelected&exec=UpdateAll&ds_block=" + index + "&tblname=" + ds_selectedtable + "&selid=" + newstate,
		context: document.body
	}).done(function() {
		dsDashboardResults.read();

	});
}
function retrieveActionInput(actionid, table,blockid, userid, inputArrays,outputType) {
	jQuery.ajax({
		url: "index.php?module=" + gVTModule + "&action=" + gVTModule + "Ajax&file=crudSelected&exec=OneSelected&tblname=" + table+"&ds_block=" + blockid,
		context: document.body,
		async: false
	}).done(function(response) {
		var rsp = jQuery.parseJSON(response);
		if (rsp.oneselected) {
			var calculatedInput = "";
			console.log(actionid);
			console.log(table);
			console.log(inputArrays);
			arguments = inputArrays.split(",");
			for (var i = 0; i < arguments.length - 1; i++) {
				inputArgs = arguments[i].split(":");
				inputname = inputArgs[0];
				fieldID = inputArgs[1];
				fldval = jQuery("#" + fieldID).val();
				calculatedInput += inputname + "=" + fldval + " ";
			}
			console.log(calculatedInput);
			return runJSONAction(actionid, "dashboardSelectedBlock=" + blockid + " dashboardSelectedUser=" +userid+ " "+ calculatedInput, outputType);

		}
		else {
			jQuery.alert(vtmkt_arr.SELECTONE);
		}
	});
}


function toggleSelectAllGrid(state, relCheckName, index) {
	var obj = document.getElementsByName(relCheckName);
	if (obj) {
		var chkdvals = '';
		for (var i = 0; i < obj.length; i++) {
			if (!obj[i].disabled) {
				obj[i].checked = state;
				//check_object(obj[i],index);
				chkdvals = chkdvals + obj[i].value + ',';
			}
		}
		jQuery.ajax({
			url: "index.php?module=" + gVTModule + "&action=" + gVTModule + "Ajax&file=crudSelected&exec=Update&ds_block=" + index + "&tblname=" + ds_selectedtable + "&selid=" + (state ? '1' : '0') + "&crmid=" + chkdvals.replace(/(^,)|(,$)/g, ''),
			context: document.body
		});
	}
}

function oneSelected(table) {
	jQuery.ajax({
		url: "index.php?module=" + gVTModule + "&action=" + gVTModule + "Ajax&file=crudSelected&exec=OneSelected&tblname=" + table,
		context: document.body
	}).done(function(response) {
		var rsp = jQuery.parseJSON(response);
		if (rsp.oneselected) {
			return true;
		} 
	});
	return false;
}
