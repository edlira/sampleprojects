<?php
global $currentModule,$log;
//require_once('Smarty_setup.php');
//require_once('data/CRMEntity.php');
//require_once('include/utils/CommonUtils.php');
//require_once('include/ListView/ListView.php');
//require_once('include/utils/utils.php');
//require_once('modules/CustomView/CustomView.php');
//require_once("modules/$currentModule/utils.php");
//$smarty = new vtigerCRM_Smarty;

include_once("modules/$currentModule/$currentModule.php");
include_once("modules/$currentModule/crudSelected.php");
global $adb, $log, $current_user, $currentModule, $category, $thementity, $image_path, $mod_strings, $app_strings, $list_max_entries_per_page;
$ds_blockid = $_REQUEST['ds_blockid'];
$request_values = $_REQUEST['request_values'];
$dashboardStructure = new $currentModule();
$dashboardStructure->id = $ds_blockid;
$table = $dashboardStructure->table;
//$searchTable = $table . "results_" . $ds_blockid . "_" . $current_user->id;
$searchTable = $table . "results";
$configTable = $searchTable . "_config";

$adb->pquery("DELETE from $configTable WHERE userid=? AND blockid=?", array($current_user->id, $ds_blockid));

$arr1 = json_decode($request_values, true);
	$new_array = array('exec' => 'List');
	$arr1=array_merge($arr1, $new_array); //array('filter_adocmaster'=>'All');
	$listQuery = $dashboardStructure->getListViewQuery($ds_blockid, '', $arr1);
	$resultQuery = $dashboardStructure->getResultQuery($ds_blockid, "", $configTable);
	// Searching...
	// delete previous search results
	$adb->query("DELETE from $searchTable where userid=" . $current_user->id);
	$insertQuery = "INSERT INTO $searchTable (userid, selected, crmid, entity,blockid) ";
	$result = $adb->query($insertQuery . ' ( ' . $listQuery . ' )');

	$fields = $resultQuery['fields'];
	$fieldArray = array();
	foreach ($fields as $fldElement) {
		$fldname[] = $fldElement['fieldname'];
		$fldlabel[] = $fldElement['fieldlabel'];
	}
	$records = crudSelected($ds_blockid, $searchTable, $configTable, $arr1);
	//Parameters
	$blockParameters = $dashboardStructure->getBlockParameters();
	$qtot = "select count(*) from $searchTable where userid=" . $current_user->id." AND blockid=".$ds_blockid;
	$total = $adb->query_result($adb->query($qtot), 0, 0);
	$finalResponse=array();
$finalResponse= array('total' => $total,
			'records' => $records['results'],
			'columns' => $fldname,
			'fieldlabels' => $fldlabel,
			'blockParameters' => $blockParameters);

echo json_encode($finalResponse,true);
//$smarty->display("modules/$currentModule/ResultView.tpl");
?>
