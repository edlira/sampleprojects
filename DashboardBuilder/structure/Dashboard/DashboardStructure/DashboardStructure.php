<?php

include_once('config.php');
require_once('include/logging.php');
require_once('include/utils/utils.php');
require_once("modules/$currentModule/utils.php");

// Account is used to store vtiger_account information.
class DashboardStructure extends CRMEntity {

	var $log;
	var $db;
	var $table = 'dashboardbuilder_';
	var $tab_name = Array('actions', 'blocks', 'fieldparams', 'fields', 'parameters', 'tabs');
	var $column_fields;
	var $id;
	var $block_moduleid;

	function __construct() {
		global $log, $currentModule;
		$this->db = PearDatabase::getInstance();
		$this->log = $log;
	}

	function getBlockFields() {
		global $currentModule;
		$fields = array();
		$fieldquery = $this->db->pquery("SELECT dsfields.* from $this->table" . "fields dsfields
				INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsfields.id
				WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsfields.block=? AND dsfields.listview<>1 
				ORDER BY field_sequence", array($currentModule, "Fields", $this->id));
		while ($fieldquery && $row = $this->db->fetch_array($fieldquery)) {
			$fieldname = $row['fieldname'];
			$fieldlabel = $row['fieldlabel'];
			$fieldtype = $row['fieldtype'];
			$mandatory = $row['mandatory'];
			if ($mandatory == 1) {
				$mandatory_fields[] = $fieldname;
			}
			$relmodule = getTabName($row['module']);
			$relfield = $row['modulefield'];
			if ($fieldtype == "Filter") {
				$fieldvalues = findFilters($relmodule);
			} elseif ($fieldtype == "Picklist") {
				$fieldvalues = findPicklistValues($relfield);
			} else {
				$fieldvalues = array();
			}
			$fields[] = array('fieldname' => $fieldname, 'fieldlabel' => $fieldlabel, 'fieldtype' => $fieldtype, 'mandatory' => $mandatory, 'relmodule' => $relmodule, 'relfield' => $relfield, 'fieldvalue' => $fieldvalues);
		}
		return $fields;
	}

	function getBlockModule() {
		global $currentModule;
		$blockQuery = $this->db->pquery("SELECT block_module FROM $this->table" . "blocks dsblock
				INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsblock.id
				WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsblock.id=?", array($currentModule, "Blocks", $this->id));
		$block_module = $this->db->query_result($blockQuery, 0, 'block_module');
		return $block_module;
	}

	function getListViewQuery($blockid = "", $usewhere = '', $request_values) {
		global $current_user, $currentModule;
		if (!empty($blockid))
			$this->id = $blockid;
		$table = $this->table;
		$tableFields = $this->table . "fields";
		$tableBlocks = $this->table . "blocks";
		$block_moduleid = $this->getBlockModule();
		$this->moduleid = $block_moduleid;
		$block_modulename = getTabname($block_moduleid);
		include_once "modules/$block_modulename/$block_modulename.php";
		$module = CRMEntity::getInstance($block_modulename);
		//$query = "SELECT vtiger_crmentity.*, $module->table_name.*";
		$query = "SELECT DISTINCT {$current_user->id} as userid, 0 as selected, vtiger_crmentity.crmid, '{$block_modulename}' , {$blockid} as blockid";
		// Keep track of tables joined to avoid duplicates
		$joinedTables = array();
		$wherecond = " ";
		// Select Custom Field Table Columns if present
		if (!empty($module->customFieldTable))
		// $query .= ", " . $module->customFieldTable[0] . ".* ";
			$query .= " FROM $module->table_name";
		$query .= " INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = $module->table_name.$module->table_index";
		$joinedTables[] = $module->table_name;
		$joinedTables[] = 'vtiger_crmentity';
		// Consider custom table join as well.
		if (!empty($module->customFieldTable)) {
			$query .= " INNER JOIN " . $module->customFieldTable[0] . " ON " . $module->customFieldTable[0] . '.' . $module->customFieldTable[1] .
				" = $module->table_name.$module->table_index";
			$joinedTables[] = $module->customFieldTable[0];
		}
		$modulesQuery = $this->db->pquery("SELECT dsfields.* FROM $tableFields as dsfields
						INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsfields.id
						WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsfields.block=?
						ORDER BY dsfields.field_sequence", array($currentModule, 'Fields', $blockid));
		$listrelmodules = array();
		while ($modulesQuery && $row = $this->db->fetch_array($modulesQuery)) {
			if ($row['module'] != $block_moduleid && !in_array($row['module'], $listrelmodules)) {
				$listrelmodules[] = getTabname($row['module']);
			}
			$fieldname = $row['fieldname'];
			$modulefield = $row['modulefield'];
			$fieldtype = $row['fieldtype'];
			$operator = $row['operator'];
			$modulename = getTabname($row['module']);
			$modulefocus = CRMEntity::getInstance($modulename);
			$moduletable = $modulefocus->table_name;
			$moduleindexfield = $modulefocus->table_index;
			if ($fieldtype == 'UItype10') {
				$request_values[$fieldname] = $request_values[$fieldname . "id"];
			}
			if (isset($request_values[$fieldname]) && !empty($request_values[$fieldname])) {
				$value = $request_values[$fieldname];
				$cond = $this->getFieldsValue($modulefield, $fieldtype, $modulename, $value, $operator, $moduletable, $moduleindexfield);
				$wherecond.= " $cond ";
			}
		}
		if (count($listrelmodules) > 0) {
			$linkedModulesQuery = $this->db->pquery("SELECT distinct fieldname, columnname, relmodule FROM vtiger_field" .
				" INNER JOIN vtiger_fieldmodulerel ON vtiger_fieldmodulerel.fieldid = vtiger_field.fieldid" .
				" WHERE uitype='10' AND vtiger_fieldmodulerel.module=? AND relmodule IN ('" . implode("','", $listrelmodules) . "')", array($block_modulename));
			$linkedFieldsCount = $this->db->num_rows($linkedModulesQuery);
			for ($i = 0; $i < $linkedFieldsCount; $i++) {
				$related_module = $this->db->query_result($linkedModulesQuery, $i, 'relmodule');
				$fieldname = $this->db->query_result($linkedModulesQuery, $i, 'fieldname');
				$columnname = $this->db->query_result($linkedModulesQuery, $i, 'columnname');

				$other = CRMEntity::getInstance($related_module);
				vtlib_setup_modulevars($related_module, $other);

				if (!in_array($other->table_name, $joinedTables)) {
					$query .= " LEFT JOIN $other->table_name ON $other->table_name.$other->table_index = $module->table_name.$columnname";
					$joinedTables[] = $other->table_name;
				}
			}
		}
		$query .= "	WHERE vtiger_crmentity.deleted = 0 " . $wherecond . " " . $usewhere;
		$this->log->debug("Exit generating list view query");
		$this->log->debug($query);
		return $query;
	}

	function getFieldsValue($modulefield, $fieldtype, $modulename, $value, $operator, $moduletable, $moduleindex) {
		$cond = "";
		if (empty($operator))
			$operator = "=";
		if ($fieldtype == "Filter") {
			$filtername = getCVname($value);
			if ($filtername != "All") {
				$record_ids = findRecords($modulename, $value);
				$cond = " AND $moduletable.$moduleindex IN(" . implode(',', $record_ids) . ") ";
			}
		} elseif ($fieldtype == "Checkbox") {
			$val = empty($value) ? 0 : 1;
			$cond = " AND $moduletable.$modulefield " . $operator . " $val ";
		} elseif ($fieldtype == "Date") {
			$val = getDBInsertDateValue($value);
			$cond = " AND $moduletable.$modulefield " . $operator . " '$val' ";
		} elseif ($fieldtype == "UItype10") {
			$cond = " AND $moduletable.$moduleindex " . $operator . " $value";
		} else {
			$cond = " AND $moduletable.$modulefield " . $operator . " '$value' ";
		}
		return $cond;
	}

	function getResultQuery($blockid = "", $usewhere = '', $configTable) {
		global $current_user, $currentModule;
		if (!empty($blockid))
			$this->id = $blockid;
		$table = $this->table;
		$tableFields = $this->table . "fields";
		$tableBlocks = $this->table . "blocks";
		$block_moduleid = $this->getBlockModule();
		$this->moduleid = $block_moduleid;
		$block_modulename = getTabname($block_moduleid);
		include_once "modules/$block_modulename/$block_modulename.php";
		$module = CRMEntity::getInstance($block_modulename);
		$query = "";
		$fields = array();
		$selectQuery = "SELECT ";
		$selectQueryFields = array();
		// Keep track of tables joined to avoid duplicates
		$joinedTables = array();
		$wherecond = " ";
		// Select Custom Field Table Columns if present
		if (!empty($module->customFieldTable))
		// $query .= ", " . $module->customFieldTable[0] . ".* ";
			$query .= " FROM $module->table_name";
		$query .= " INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = $module->table_name.$module->table_index";

		$joinedTables[] = $module->table_name;
		$joinedTables[] = 'vtiger_crmentity';

		// Consider custom table join as well.
		if (!empty($module->customFieldTable)) {
			$query .= " INNER JOIN " . $module->customFieldTable[0] . " ON " . $module->customFieldTable[0] . '.' . $module->customFieldTable[1] .
				" = $module->table_name.$module->table_index";
			$joinedTables[] = $module->customFieldTable[0];
		}
		$this->log->debug("erdhi ketu");
		$modulesQuery = $this->db->pquery("SELECT module, fieldname, modulefield, fieldlabel 
				FROM $tableFields as dsfields
				INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsfields.id
				WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsfields.block=? AND dsfields.listview=1 
				ORDER BY dsfields.field_sequence", array($currentModule, 'Fields', $blockid));
		$listrelmodules = array();
		while ($modulesQuery && $row = $this->db->fetch_array($modulesQuery)) {
			if ($row['module'] != $block_moduleid && !in_array($row['module'], $listrelmodules)) {
				$listrelmodules[] = getTabname($row['module']);
			}
			if ($row['module'] == $block_moduleid) {
				$fielddetails = array('fieldname' => $row['fieldname'], 'fieldlabel' => $row['fieldlabel'], 'module' => $block_modulename);
				$selectQueryFields[] = $module->table_name . "." . $row['modulefield'] . " as `" . $row['fieldname'] . "` ";
				$fields[] = $fielddetails;
			}
			//
		}
		$this->log->debug("fushat e te njejtit modul");
		$this->log->debug($selectQueryFields);
		for ($l = 0; $l < count($listrelmodules); $l++) {
			$fldRelModule = getTabId($listrelmodules[$l]);
			$fieldsQuery = $this->db->pquery("SELECT module, fieldname, modulefield, fieldlabel 
					FROM $tableFields as dsfields
					INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsfields.id
					WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsfields.block=? 
					AND dsfields.module=? AND dsfields.listview=1 
					ORDER BY dsfields.field_sequence", array($currentModule, 'Fields', $blockid, $fldRelModule));
			while ($fieldsQuery && $row = $this->db->fetch_array($fieldsQuery)) {
				$related_module = getTabname($row['module']);
				$other = CRMEntity::getInstance($related_module);
				$fielddetails = array('fieldname' => $row['fieldname'], 'fieldlabel' => $row['fieldlabel'], 'module' => $related_module);
				$selectQueryFields[] = 'CONCAT("'.$related_module.'::::::",'.$other->table_name .'.'.$other->table_index.',"::::::",'.$other->table_name.'.'.$row["modulefield"] . ') as `' . $row['fieldname'] . '` ';
				;
				$fields[] = $fielddetails;
			}
		}
		$this->log->debug("fushat e lidhura");
		$this->log->debug($selectQueryFields);
		if (count($listrelmodules) > 0) {
			$linkedModulesQuery = $this->db->pquery("SELECT distinct fieldname, columnname, relmodule FROM vtiger_field " .
				" INNER JOIN vtiger_fieldmodulerel ON vtiger_fieldmodulerel.fieldid = vtiger_field.fieldid" .
				" WHERE uitype='10' AND vtiger_fieldmodulerel.module=? AND relmodule IN ('" . implode("','", $listrelmodules) . "')", array($block_modulename));
			$linkedFieldsCount = $this->db->num_rows($linkedModulesQuery);
			for ($i = 0; $i < $linkedFieldsCount; $i++) {
				$related_module = $this->db->query_result($linkedModulesQuery, $i, 'relmodule');
				$fieldname = $this->db->query_result($linkedModulesQuery, $i, 'fieldname');
				$columnname = $this->db->query_result($linkedModulesQuery, $i, 'columnname');

				$other = CRMEntity::getInstance($related_module);
				vtlib_setup_modulevars($related_module, $other);

				if (!in_array($other->table_name, $joinedTables)) {
					$query .= " LEFT JOIN $other->table_name ON $other->table_name.$other->table_index = $module->table_name.$columnname";
					$joinedTables[] = $other->table_name;
				}
			}
		}
		$wherecondition = $module->table_name . "." . $module->table_index . "=";
		$selectQuery.=" " . implode(",", $selectQueryFields);
		$insertQuery = "INSERT INTO $configTable (id, select_query, cond_query, where_query,userid,blockid) VALUES(?,?,?,?,?,?)";
		$result = $this->db->pquery($insertQuery, array("", $selectQuery, $query, $wherecondition,$current_user->id,$blockid));
		$result = array("fields" => $fields);
		//$query .= "	WHERE vtiger_crmentity.deleted = 0 ".$wherecond. " ".$usewhere;
		$this->log->debug("Exit generating list view query");
		$this->log->debug($query);
		return $result;
	}

	function getBlockParameters($parentID="") {
		$parameters = array();
		if(empty($parentID)){
			$additionalCond = " AND (dsparams.parent_parameter IS NULL OR dsparams.parent_parameter='') ";
		}
		else{
			$additionalCond = " AND dsparams.parent_parameter = $parentID";
		}
		$parameterquery = $this->db->pquery("SELECT dsparams.id, dsparams.name, dsparams.label, dsparams.action,dsaction.name as actionname,dsaction.label as actionlabel,vtiger_actions.output_type
				FROM $this->table" . "parameters dsparams
				INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsparams.id
				INNER JOIN $this->table" . "actions dsaction ON dsaction.id=dsparams.action
				INNER JOIN $this->table" . "entities ds_en_ac ON ds_en_ac.dsid=dsaction.id
				INNER JOIN vtiger_actions ON vtiger_actions.actionsid=dsaction.name
				WHERE ds_en.deleted=0 AND ds_en_ac.deleted=0 AND block=? $additionalCond 
				ORDER BY dsparams.sequence", array($this->id));
		while ($parameterquery && $row = $this->db->fetch_array($parameterquery)) {
			$paramid = $row['id'];
			$paramname = $row['name'];
			$paramlabel = $row['label'];
			$actionid = $row['action'];
			$actioname = $row['actionname'];
			$actionlabel = $row['actionlabel'];
			$actionoutput = $row['output_type'];
			$paramDepend = $this->getBlockParameters($paramid);
			$paramFields = $this->getParameterFields($paramid);
			$paramAction = array("actionid" => $actionid, 'actionname' => $actioname, 'actionlabel' => $actionlabel, 'actionoutput' => $actionoutput);
			$parameters[$paramid] = array('paramname' => $paramname, 'paramlabel' => $paramlabel, 'paramfields' => $paramFields, 'paramAction' => $paramAction,'paramDepend'=>$paramDepend);
		}
		return $parameters;
	}

	function getParameterFields($paramid) {
		global $currentModule;
		$fields = array();
		$fieldquery = $this->db->pquery("SELECT dsfieldparams.* from $this->table" . "fieldparams as dsfieldparams
				INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsfieldparams.id
				WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsfieldparams.parameter=? 
				ORDER BY sequence", array($currentModule, 'FieldParams', $paramid));
		while ($fieldquery && $row = $this->db->fetch_array($fieldquery)) {
			$fieldname = $row['name'];
			$fieldlabel = $row['label'];
			$fieldinputname = $row['input_name'];
			$fieldtype = $row['type'];
			$defaultValue = $row['default_value'];
			$relmodule = getTabName($row['module']);
			$relfield = $row['modulefield'];
			if ($fieldtype == "Picklist")
				$fieldvalues = findPicklistValues($relfield);
			else
				$fieldvalues = array();
			$fields[] = array('fieldname' => $fieldname, 'fieldlabel' => $fieldlabel, 'fieldinputname' => $fieldinputname, 'fieldtype' => $fieldtype, 'relmodule' => $relmodule, 'relfield' => $relfield, 'fieldvalue' => $fieldvalues, 'defaultvalue' => $defaultValue);
		}
		return $fields;
	}

	function getParameterAction($paramid) {
		global $currentModule;
		$fields = array();
		$fieldquery = $this->db->pquery("SELECT * from $this->table" . "fieldparams dsfieldparams
				INNER JOIN $this->table" . "entities ds_en ON ds_en.dsid=dsfieldparams.id
				WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND parameter=? 
				ORDER BY sequence", array($currentModule, 'Fields', $paramid));
		while ($fieldquery && $row = $this->db->fetch_array($fieldquery)) {
			$fieldname = $row['name'];
			$fieldlabel = $row['label'];
			$fieldtype = $row['type'];
			$defaultValue = $row['default_value'];
			$relmodule = getTabName($row['module']);
			$relfield = $row['modulefield'];
			if ($fieldtype == "Picklist"){
				$fieldvalues = findPicklistValues($relfield);
			}
			else {
				$fieldvalues = array();
			}
			$fields[] = array('fieldname' => $fieldname, 'fieldlabel' => $fieldlabel, 'fieldtype' => $fieldtype, 'relmodule' => $relmodule, 'relfield' => $relfield, 'fieldvalue' => $fieldvalues, 'defaultvalue' => $defaultValue);
		}
		return $fields;
	}
}
?>