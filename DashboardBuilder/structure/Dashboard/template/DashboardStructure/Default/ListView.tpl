<div id="tabs" style="padding:20px; width:95%">
	<ul>
		{foreach key=key item=item from=$TABS}
			{assign var=tabinfo value=$item.info}
			<li><a href="#tabs-{$key}">{$tabinfo.tab_label}</a></li>
			{/foreach}
	</ul>

	{foreach key=key item=item from=$TABS}
		<div id="tabs-{$key}">
			{assign var=blockinfo value=$item.blocks}
			{foreach key=keyblock item=blockitem from=$blockinfo}
				<form name="SearchView" method="POST" action="index.php" >
					<input type="hidden" name="module" value="{$MODULE}">
					<input type="hidden" name="record" value="{$ID}">
					<input type="hidden" name="mode" value="{$MODE}">
					<input type="hidden" name="action" value="searchBlock">
					<input type="hidden" name="parenttab" value="{$CATEGORY}">
					<input type="hidden" name="ds_tabid" value="{$key}">
					<input type="hidden" name="ds_blockid" value="{$blockitem.blockid}">
					<table border=0 cellspacing=0 cellpadding=0 width=100% align="center">
						<tr>
							<td style="padding:10px 5px 0px 5px">
								<table border=0 cellspacing=0 cellpadding=0 width=100% class="small">
									<tr><td colspan=4>&nbsp;</td></tr>
									<tr>
										<td colspan=4 class="dvInnerHeader">
											<div style="float:left;font-weight:bold;"><div style="float:left;"><a href="javascript:showHideStatus('tbl{$blockitem.blockid}','aid{$blockitem.blockid}');">
														<span id="aid{$blockitem.blockid}" class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
													</a></div>&nbsp;
												<font color=" #0073ea">{$blockitem.blocklabel}</font>
											</div>
										</td>
									</tr>
								</table>
								<div style="width:auto;display:none" id="tbl{$blockitem.blockid}" > 
									<table border=0 cellspacing=0 cellpadding=0 width="100%" class="small">
										<tr>
											{assign var=fieldinfo value=$blockitem.fields}
											{foreach key=index item=fielddata from=$fieldinfo}

												{include file="modules/$MODULE/FieldView.tpl"}
												{if ($index+1)% 2 == 0}
												</tr><tr>
												{/if}
											{/foreach}
											{if ($index+1)% 2 != 0}
												<td width="20%" class="dvtCellLabel"></td><td width="30%" class="dvtCellInfo"></td>
												{/if}
										</tr>

									</table>
									<center>
										<input title="{$APP.LBL_SEARCH_BUTTON_TITLE}" accessKey="{$APP.LBL_SEARCH_BUTTON_KEY}" class="ui-button ui-widget ui-state-default ui-corner-all searchbutton" type="submit" name="button" value="{$APP.LBL_SEARCH_BUTTON_LABEL}" id="searchbutton" >
										{*onclick="return validateMandatory('{$blockitem.mandatory_fieldname}', '{$blockitem.mandatory_fieldlabel}')"*}
									</center>
								</div>
							</td>
						</tr>
					</table>
				</form>
			{/foreach}
		</div>

	{/foreach}
</div>