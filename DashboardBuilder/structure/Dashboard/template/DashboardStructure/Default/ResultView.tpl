{include file="modules/$MODULE/Header.tpl"}

<form name="ResultView" method="POST" action="index.php">
	<input type="hidden" name="module" value="{$MODULE}">
	<input type="hidden" name="record" value="{$ID}">
	<input type="hidden" name="mode" value="{$MODE}">
	<input type="hidden" name="action" value="convert">
	<input type="hidden" name="convertto" value="po">
	<input type="hidden" name="parenttab" value="{$CATEGORY}">
	<input type="hidden" name="selectallrecords" id="selectallrecords" value="0">
	<input type="hidden" name="ds_blockid" value="{$ds_blockid}"> 
	<br><br><br>
	<input class="ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" type="button" id="chooseAllBtn" value="{'Select all records'|@getTranslatedString:'MarketingDashboard'}" onclick="toggleSelectAllEntries_ListView('');" {if $hideSelectAll eq 'true'} style="display:none"{/if}/>
	<div class="k-content" style="clear:both;">
		<div id="resultView{$ds_blockid}"></div>
		<script>
		var ds_selectedblock = {$ds_blockid};
		var ds_selectedtable = "{$ds_table}";
		var ds_module = "{$MODULE}";
			{literal}

				var crudServiceBaseUrl = "index.php?module=" + ds_module + "&action=" + ds_module + "Ajax&file=crudSelected";
				var dsDashboardResults = new kendo.data.DataSource({
					transport: {
						read: {
							url: crudServiceBaseUrl + "&exec=List&ds_block=" + ds_selectedblock + "&tblname=" + ds_selectedtable,
							dataType: "json"
						},
						update: {
							url: crudServiceBaseUrl + "&exec=Update&ds_block=" + ds_selectedblock + "&tblname=" + ds_selectedtable,
							dataType: "json"
						},
						destroy: {
							url: crudServiceBaseUrl + "&exec=Destroy&ds_block=" + ds_selectedblock + "&tblname=" + ds_selectedtable,
							dataType: "json"
						},
					},
					pageSize: {/literal}{$PAGESIZE}{literal},
					pageable: true,
					serverPaging: true,
					serverSorting: true,
					serverFiltering: true,
					schema: {
						model: {
							fields: {/literal}{$fields}{literal}
						},
						data: "results",
						total: "total"
					},
					group: {/literal}{$groups}{literal}
				});
				$(document).ready(function() {
					$("#resultView" + ds_selectedblock).kendoGrid({
						dataSource: dsDashboardResults,
						height: 400,
						sortable: {
							mode: "multiple",
							allowUnsort: true
						},
						groupable: true,
						scrollable: true,
						selectable: "row",
						change: function(e) {
							//var selectedCells = this.select();
							//selectedCells.find('input[type="checkbox"]').click();
						},
						dataBound: updateGridSelectAllCheckbox,
						pageable: true,
						filterable: false,
						sortable:false,
								columns:{/literal}{$columns}{literal}
					});
				});
			</script>
		{/literal}
	</div>
	<br><br>
	<div id="accordionParameters">
		{include file="modules/$MODULE/ParametersView.tpl"}
	</div>
</form>