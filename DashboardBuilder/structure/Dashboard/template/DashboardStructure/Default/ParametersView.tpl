	{foreach key=key item=paramBlock from=$PARAMETERS}
		<h3><a href="#">{$paramBlock.paramlabel}</a></h3>
		<div>
			<table style="width:100%;">
				<tr>
					{assign var=fieldinfo value=$paramBlock.paramfields}
					{assign var=actioninfo value=$paramBlock.paramAction}
					{assign var="actionid" value=$actioninfo.actionid}
					{assign var=result value=''}
					{foreach key=index item=fielddata from=$fieldinfo}
						{assign var="fldlabel" value=$fielddata.fieldlabel}
						{assign var="fldname" value=$fielddata.fieldname}
						{assign var="fldinputname" value=$fielddata.fieldinputname}
						{assign var="fldtype" value=$fielddata.fieldtype}
						{assign var="default_value" value=$fielddata.defaultvalue}
						{assign var="fldvalue" value=$fielddata.fieldvalue}
						{assign var="relmodule" value=$fielddata.relmodule}

						<td width=20% class="dvtCellLabel" align=right>
							{$fldlabel}
						</td>
						{assign var="paramID" value=$fldname}
						<td width="30%" align=left class="dvtCellInfo">
							{if $fldtype eq 'Textbox'}
								<input type="text" name="{$fldname}" id="{$fldname}" value="{$default_value}">
							{elseif $fldtype eq 'Checkbox'}
								<input type="checkbox" name="{$fldname}" id="{$fldname}" value="{$default_value}"> 
							{elseif $fldtype eq 'Date'}
								{assign var="paramID" value="jscal_field_"|cat:$fldname}
								<input name="{$fldname}" tabindex="15" id="jscal_field_{$fldname}" type="text" style="border:1px solid #bababa;" size="11" maxlength="10" class="datepicker">
								<br><font size=1><em old="(yyyy-mm-dd)">({$dateStr})</em></font>
							{elseif $fldtype eq 'UItype10'}
								{assign var="paramID" value=$fldname|cat:"id"}
								<input name="{$fldname}id" id="{$fldname}id" type="hidden" value="{$default_value}">
								<input name="{$fldname}id_display" id="{$fldname}id_display" readonly="readonly" value="" style="border: 1px solid rgb(186, 186, 186);" type="text">&nbsp;
								<img src="{'select.gif'|@vtiger_imageurl:$THEME}" tabindex="20" alt="Select" title="Select" language="javascript" onclick='return window.open("index.php?module={$relmodule}&action=Popup&html=Popup_picker&form=vtlibPopupView&forfield={$fldname}id", "test", "width=640,height=602,resizable=0,scrollbars=0,top=150,left=200");' style="cursor: pointer;" align="absmiddle">&nbsp;
								<input src="{'clear_field.gif'|@vtiger_imageurl:$THEME}" alt="Clear" title="Clear" language="javascript" onclick="this.form.{$fldname}id.value = '';
										this.form.{$fldname}_display.value = '';
										return false;" style="cursor: pointer;" align="absmiddle" type="image">
							{elseif $fldtype eq 'Picklist'}
								<select name="{$fldname}" class="singlecombo" tabindex="10">
									{foreach key=key item=value from=$fldvalue}
										{if $value eq $default_value}
											<option value="{$key}" selected>{$value}</option>
										{else}
											<option value="{$key}">{$value}</option>
										{/if}
									{/foreach}
								</select>

							{/if}
						</td>
						{if ($index+1)% 2 == 0}
						</tr><tr>
						{/if}
						{assign var="result" value=$result$fldinputname:$paramID,}
					{/foreach}
					{if ($index+1)% 2 != 0}
						<td width="20%" class="dvtCellLabel"></td><td width="30%" class="dvtCellInfo"></td>
						{/if}
				</tr>
			</table>   <!--ACTIONS -->
			<div align="center">
				<br>

				{assign var="actionname" value=$actioninfo.actionname}
				{assign var="actionlabel" value=$actioninfo.actionlabel}
				{assign var="actionoutput" value=$actioninfo.actionoutput}
				<input title="{$actionlabel}" accessKey="V" class="ui-button ui-widget ui-state-default ui-corner-all convertbutton" type="button" name="button" id="actionbutton_{$key}_{$actionid}" value="  {$actionlabel}  " onclick='retrieveActionInput("{$actionname}", "{$ds_table}", "{$ds_blockid}", "{$ds_userid}", "{$result}", "{$actionoutput}")'>
			</div>
		</div>
	{/foreach}
