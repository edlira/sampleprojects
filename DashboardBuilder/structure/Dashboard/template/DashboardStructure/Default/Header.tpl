<link href="include/kendoui/kendoui/styles/kendo.common.css" rel="stylesheet"/>
<link href="include/kendoui/kendoui/styles/kendo.default.css" rel="stylesheet"/>
<script src="modules/{$MODULE}/js/jquery-1.7.2.min.js"></script>
<script src="include/kendoui/kendoui/js/kendo.web.js"></script>
<script src="include/kendoui/kendoui/js/kendo.pager.js"></script>
<script src="modules/{$MODULE}/{$MODULE}.js"></script>
<link rel="stylesheet" type="text/css" href="modules/{$MODULE}/styles/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="modules/{$MODULE}/js/jquery-ui-1.9.2.custom.js"></script>
<script type="text/javascript" src="modules/{$MODULE}/js/jquery-multiselect.js"></script>
<script type="text/javascript" src="modules/{$MODULE}/js/jquery-multiselect-filter.js"></script>
<link rel="stylesheet" type="text/css" href="modules/{$MODULE}/styles/jquery-multiselect.css"/>
<link rel="stylesheet" type="text/css" href="modules/{$MODULE}/styles/jquery-multiselect-filter.css"/>
{literal}
	<script type="text/javascript">
		$(function() {

			$('#tabs').tabs();
			$(".searchbutton").button();
			$(".multicombo").multiselect().multiselectfilter();
			$(".singlecombo").multiselect({
				multiple: false,
				header: false,
				noneSelectedText: "Select an Option",
				selectedList: 1
			});
			$("#accordionParameters").accordion({clearStyle: true}, {collapsible: true}, {active: false});
			$(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat:{/literal} "{$dateFormat}"{literal}});
			$.extend({alert: function(message, title) {
					$("<div></div>").dialog({
						buttons: {"Ok": function() {
								$(this).dialog("close");
							}},
						close: function(event, ui) {
							$(this).remove();
						},
						resizable: false,
						title: 'Checking parameters',
						modal: true
					}).text(message);
				}
			});

		});

	</script>
{/literal}
{include file='Buttons_List1.tpl'}