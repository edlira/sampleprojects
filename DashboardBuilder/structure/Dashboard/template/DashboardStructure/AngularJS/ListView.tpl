hereee
<table width=96% align=center border="0" ng-app="demoApp" style="padding:10px;">
	<tr><td style="height:2px"><br/><br/></td></tr>
	<tr>
		<td style="padding-left:20px;padding-right:50px" class="moduleName" nowrap colspan="2">
			<h3 class="title">{$MODULE}</h3></td>
	</tr>
	<tr>
		<td ng-app="demoApp" ng-controller="{$MODULE}" ng-cloak class="showPanelBg" valign="top" style="padding:10px;" width=96%>
			<div id="pageWrapper">
				<div class="pageContent profile-view">
					<div id="profileContent">
						<div>
							{include file="modules/$MODULE/SearchView.tpl"}
						</div>
						<div>
							{include file="modules/$MODULE/ResultView.tpl"}
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>

<script>
	{literal}
angular.module('demoApp', ['ngTable', 'ui.bootstrap', 'multi-select', 'ngMaterial'])
                .controller({/literal}'{$MODULE}'{literal}, function($scope, $http, $modal, $filter, ngTableParams) {

$scope.name_dashboard ={/literal} '{$MODULE}'{literal};
$scope.structure ={/literal}{$TABS_JSON}{literal};
$scope.show_results = false;
$scope.fldValues = {};
$scope.fldValuesParam = {};
$scope.enable_action = false;
$scope.filter_val = '';
$scope.cerca_name = 'Cerca';
$scope.enable_cerca = false;
$scope.tot_length = 0;
$scope.generalSearch = function(blockid,values,actionid,outputtype) {
    console.log("tabelaaa");
   // console.log(idname);
    console.log(values);
    var tablename='tableParams';
        $scope.searchText = '';
            $scope.blc = blockid;
            $scope.vls = values;
            $scope.actionid = actionid;

            if ($scope[tablename]) {
                $scope.show_results = false;
                $scope.cerca_name = 'Ricerca';
                $scope.enable_cerca = true;
                $scope[tablename].reload();
            } else {
                $scope[tablename] = new ngTableParams({
                    page: 1, // show first page
                    count: 5 // count per page
                }, {
                    counts: [], // hide page counts control
                    getData: function($defer, params) {
                        console.log("edaaa");
                        console.log($scope.vls);
                        var allParams = JSON.stringify($scope.vls);

                        var actionResult = runJSONAction(actionid, allParams, outputtype);
                        console.log("aaaa");
                        console.log(JSON.parse(actionResult));
                        actionResult = JSON.parse(actionResult);
                        if (actionResult != null) {
                            $scope.returnedResults = actionResult.records;
                        } else {
                            $scope.returnedResults = [];
                        }
                        $scope.enable_cerca = false;
                        $scope.tot_length = $scope.returnedResults.length;
                        $scope.tot = actionResult.total;
                        $scope.columns = actionResult.columns;
                       // var keyNamefld="fieldlabels"+idname;
                        $scope.fieldlabels = actionResult.fieldlabels;
                        $http.post('index.php?module=' + $scope.name_dashboard + '&action=' + $scope.name_dashboard + 'Ajax&file=retrieveParameters&ds_blockid=' + $scope.blc + '&request_values=' + allParams)
                                .success(function(data, status) {
                            console.log("dataaa");
                            console.log(data);
                            $scope.blockParameters = data.blockParameters;
                        });
                        var orderedData = params.filter ?
                                $filter('filter')($scope.returnedResults, params.filter()) : $scope.returnedResults;

                        params.total($scope.tot_length); // set total for recalc pagination
                        $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));



                    }
                });
            }
            $scope.show_results = true;
            $scope.cerca_name = 'Cerca';
            $scope.enable_cerca = false;
            $scope.getTitle = function(val) {
                return $scope.fieldlabels[val];
            };
            $scope.checkboxes = {
                'checked': false,
                items: {}
            };
            $scope.allselected = false;
            $scope.selectAll = function() {
                angular.forEach($scope.returnedResults, function(value, key) {
                    if ($scope.allselected) {
                        $scope.checkboxes.items[value.id] = false;
                    } else {
                        $scope.checkboxes.items[value.id] = true;
                    }
                });
                $scope.allselected = !$scope.allselected;
            };
            $scope.$watch('checkboxes.items', function(values) {
                if (!$scope.users) {
                    return;
                }
                var checked = 0,
                        unchecked = 0,
                        total = $scope.users.length;
                angular.forEach($scope.users, function(item) {
                    checked += ($scope.checkboxes.items[item.id]) || 0;
                    unchecked += (!$scope.checkboxes.items[item.id]) || 0;
                });
                if ((unchecked == 0) || (checked == 0)) {
                    $scope.checkboxes.checked = (checked == total);
                }
                angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
            }, true);
        };
$scope.findWSResults = function(idname,values,actionid,outputtype) {
    console.log("tabelaaa");
    console.log(idname);
    console.log(actionid);
    console.log(values);
    var tablename='tableParams'+idname;
    $scope.fieldlabels={};
    $scope.columns={};
    $scope.blockParameters={};
        $scope.searchText = '';
            $scope.vls = values;
            $scope.actionid = actionid;
            $scope.vls = {
                'mail':'s.zeneli@studiosynthesis.biz'
            };
            if ($scope[tablename] ) {
                $scope.show_results = false;
                $scope.cerca_name = 'Ricerca';
                $scope.enable_cerca = true;
                $scope[tablename].reload();
            } else {
                $scope[tablename] = new ngTableParams({
                    page: 1, // show first page
                    count: 5 // count per page
                }, {
                    counts: [], // hide page counts control
                    getData: function($defer, params) {
                        console.log("edaaa");
                        console.log($scope.vls);
                        var allParams = JSON.stringify($scope.vls);

                        var actionResult = runJSONAction(actionid, allParams, outputtype);
                        console.log("aaaa");
                        console.log(JSON.parse(actionResult));
                        actionResult = JSON.parse(actionResult);
                        if (actionResult != null) {
                            $scope.returnedResultsParam = actionResult.records;
                        } else {
                            $scope.returnedResultsParam = [];
                        }
                        $scope.enable_cerca = false;
                        $scope.tot_length = $scope.returnedResultsParam.length;
                        $scope.tot = actionResult.total;
                        $scope.columns[idname] = actionResult.columns;
                        var keyNamefld="fieldlabels"+idname;
                        $scope.fieldlabels[idname] = actionResult.fieldlabels;
                        var orderedData = params.filter ?
                                $filter('filter')($scope.returnedResultsParam, params.filter()) : $scope.returnedResultsParam;

                        params.total($scope.tot_length); // set total for recalc pagination
                        $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));



                    }
                });
            }
            $scope.show_results = true;
            //$scope.cerca_name = 'Cerca';
            //$scope.enable_cerca = false;
           /* $scope.getTitle = function(val) {
                return $scope.fieldlabels[val];
            };*/
            $scope.checkboxesparam = {
                'checked': false,
                items: {}
            };
            $scope.allselected = false;
            $scope.selectAll = function() {
                angular.forEach($scope.returnedResultsParam, function(value, key) {
                    if ($scope.allselected) {
                        $scope.checkboxesparam.items[value.id] = false;
                    } else {
                        $scope.checkboxesparam.items[value.id] = true;
                    }
                });
                $scope.allselected = !$scope.allselected;
            };
            $scope.$watch('checkboxes.items', function(values) {
                if (!$scope.users) {
                    return;
                }
                var checked = 0,
                        unchecked = 0,
                        total = $scope.users.length;
                angular.forEach($scope.users, function(item) {
                    checked += ($scope.checkboxesparam.items[item.id]) || 0;
                    unchecked += (!$scope.checkboxes.items[item.id]) || 0;
                });
                if ((unchecked == 0) || (checked == 0)) {
                    $scope.checkboxesparam.checked = (checked == total);
                }
                angular.element(document.getElementById("select_all"+idname)).prop("indeterminate", (checked != 0 && unchecked != 0));
            }, true);
        };
                $scope.findResults = function(blockid, values) {
                    $scope.searchText = '';
                    $scope.blc = blockid;
                    $scope.vls = values;
                    if ($scope.tableParams) {
                        $scope.show_results = false;
                        $scope.cerca_name = 'Ricerca';
                        $scope.enable_cerca = true;
                        $scope.tableParams.reload();
                    } else {
                        $scope.tableParams = new ngTableParams({
                            page: 1, // show first page
                            count: 5 // count per page
                        }, {
                            counts: [], // hide page counts control
                            getData: function($defer, params) {
                                var allParams = JSON.stringify($scope.vls);
                                $http.post('index.php?module=' + $scope.name_dashboard + '&action=' + $scope.name_dashboard + 'Ajax&file=searchBlockClient&ds_blockid=' + $scope.blc + '&request_values=' + allParams)
                                        .success(function(data, status) {
                                    if (data.records != null) {
                                        $scope.returnedResults = data.records;
                                    } else {
                                        $scope.returnedResults = [];
                                    }
                                    $scope.enable_cerca = false;
                                    $scope.tot_length = $scope.returnedResults.length;
                                    $scope.tot = data.total;
                                    $scope.columns = data.columns;
                                    $scope.fieldlabels = data.fieldlabels;

                                    $scope.blockParameters = data.blockParameters;
                                    var orderedData = params.filter ?
                                            $filter('filter')($scope.returnedResults, params.filter()) : $scope.returnedResults;

                                    params.total($scope.tot_length); // set total for recalc pagination
                                    $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                                });
                            }
                        });
                    }
                    $scope.show_results = true;
                    $scope.cerca_name = 'Cerca';

                    $scope.getTitle = function(val) {
                        return $scope.fieldlabels[val];
                    };
                    $scope.checkboxes = {
                        'checked': false,
                        items: {}
                    };
                    $scope.allselected = false;
                    $scope.selectAll = function() {
                        angular.forEach($scope.returnedResults, function(value, key) {
                            if ($scope.allselected) {
                                $scope.checkboxes.items[value.recordid] = false;
                            } else {
                                $scope.checkboxes.items[value.recordid] = true;
                            }
                        });
                        $scope.allselected = !$scope.allselected;
                    };
                    $scope.$watch('checkboxes.items', function(values) {
                        if (!$scope.users) {
                            return;
                        }
                        var checked = 0,
                                unchecked = 0,
                                total = $scope.users.length;
                        angular.forEach($scope.users, function(item) {
                            checked += ($scope.checkboxes.items[item.recordid]) || 0;
                            unchecked += (!$scope.checkboxes.items[item.recordid]) || 0;
                        });
                        if ((unchecked == 0) || (checked == 0)) {
                            $scope.checkboxes.checked = (checked == total);
                        }
                        angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
                    }, true);
                };
                $scope.runAction = function(actionid, values, selectedids, outputType) {
                    var arr_sel = '';
                    $scope.enable_action = true;
                    var count = 0;
                    angular.forEach(selectedids, function(value, key) {
                        if (value == true) {
                            arr_sel += (count === 0 ? key : ',' + key);
                            count++;
                        }
                    });
                    var senddata = {
                        'recordid': arr_sel,
                    };
                    if (values != undefined) {
                        values = angular.extend(values, senddata);
                    } else {
                        values = senddata;
                    }
                    runJSONAction(actionid, JSON.stringify(values), outputType);
                    $scope.enable_action = false;
                };

            });
	{/literal}
</script>