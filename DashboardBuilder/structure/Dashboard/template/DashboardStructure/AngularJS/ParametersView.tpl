<accordion close-others="oneAtATime">
	<accordion-group ng-repeat="action in blockParameters" heading="{literal}{{action.paramlabel}}{/literal}" id="{literal}{{action.paramname}}{/literal}">
		<accordion close-others="oneAtATime" >
		<accordion-group ng-repeat="widget in action.paramDepend" heading="{literal}{{widget.paramlabel}}{/literal}" id="{literal}{{widget.paramname}}{/literal}">
		<table width="100%" style="width:100%;" border="0" ng-table="tableParams{literal}{{widget.paramname}}{/literal}"  >
			<tr>
				<td style="text-align: left;width:3%;" >
					<input type="checkbox" ng-model="select_all[widget.paramname]" id="select_all{literal}{{widget.paramname}}{/literal}" name="filter-checkbox" ng-click="selectAll();"/>
				</td>
				<!--<td width="5%" style="text-align: left;">
					<b>Title</b><br/>
				</td>-->
				<td ng-repeat="fieldlabel in  fieldlabels[widget.paramname]" style="text-align: left;padding-top:15px;width:14%;padding-right: 0px;margin-right: 0px;">
					<b>{literal}{{fieldlabel}}{/literal}</b>
				</td>
			</tr>
			<tr ng-repeat="records in $data">
				<td width="1%" style="text-align: left;width:3%;" >
					<input type="checkbox" ng-model="checkboxesparam.items[records.recordid]" />
				</td>
				<td height="10" ng-repeat="(key, value) in  columns[widget.paramname]" style="height:15px;padding-top:15px;text-align: left;width:14%;padding-right: 0px;margin-right: 0px;">
					<span>{literal}{{records[value]}}{/literal}</span>
				</td>
			</tr>
		</table>
			<table>
			
			<tr> 
				<td ng-repeat="fieldsdata in widget.paramfields">
				{literal}{{fieldsdata.fieldlabel}}{/literal}
				<span ng-switch on="fieldsdata.fieldtype">
					<textarea ng-switch-when="Textbox" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.relfield]"></textarea>
					<input type="checkbox" ng-switch-when="Checkbox" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.relfield]"/>
					<input type="date" ng-switch-when="Date" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.relfield]"/>
					<input type="text" ng-switch-when="UItype10" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.relfield]" />
					<select name="field"  tabindex="10" ng-switch-when="Picklist" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.relfield]">
						<option ng-repeat="fld in fieldsdata.fieldvalue" value="{literal}{{fld}}{/literal}">{literal}{{fld}}{/literal}</option>
					</select>
				</span>
			</td>
		</tr>
		<tr> 
			<td>
				<div id="buttons" align="center">
					<md-button class="md-raised md-primary add-action" ng-disabled="enable_action"
						ng-click="findWSResults(widget.paramname,checkboxesparam.items,224350,'html');">
					{literal}{{widget.paramAction.actionlabel}}{/literal}
					</md-button>
				</div> 
			</td>
		</tr>
	</table>
	</accordion-group>
</accordion>
			<table>
			<tr> 
				<td ng-repeat="fields in action.paramfields">
				{literal}{{fields.fieldlabel}}{/literal}
				<span ng-switch on="fields.fieldtype">
					<textarea ng-switch-when="Textbox" ng-model="fldValuesParam[action.paramAction.actionid][fields.relfield]"></textarea>
					<input type="checkbox" ng-switch-when="Checkbox" ng-model="fldValuesParam[action.paramAction.actionid][fields.relfield]"/>
					<input type="date" ng-switch-when="Date" ng-model="fldValuesParam[action.paramAction.actionid][fields.relfield]"/>
					<input type="text" ng-switch-when="UItype10" ng-model="fldValuesParam[action.paramAction.actionid][fields.relfield]" />
					<select name="field"  tabindex="10" ng-switch-when="Picklist" ng-model="fldValuesParam[action.paramAction.actionid][fields.relfield]">
						<option ng-repeat="fld in fields.fieldvalue" value="{literal}{{fld}}{/literal}">{literal}{{fld}}{/literal}</option>
					</select>
				</span>
			</td>
		</tr>
		<tr> 
			<td>
				<div id="buttons" align="center">
					<md-button class="md-raised md-primary add-action" ng-disabled="enable_action"
						ng-click="findWSResults(action.paramname,tabaction.paramAction.actionname,fldValuesParam[action.paramAction.actionid],checkboxes.items,action.paramAction.actionoutput);">
					{literal}{{action.paramAction.actionlabel}}{/literal}
					</md-button>
				</div> 
			</td>
		</tr>
	</table>

</accordion-group>
</accordion>