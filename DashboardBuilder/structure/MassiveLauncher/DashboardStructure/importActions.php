<?php
global $adb;
$dirImport="import";
$fileList=array();
if(is_dir($dirImport))
{
    foreach (scandir($dirImport) as $files)
    {
        if ($files!="." && $files !="..")
        {
          $path_parts = pathinfo($files);
          $extension = $path_parts["extension"];
          if ($extension=="csv")
          $csvList=$files;
        }

    }
}
$actionQuery = "SELECT * FROM  vtiger_actions 
                INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_actions.actionsid
                WHERE ce.deleted=0 AND moduleactions=?
                AND actions_status=? ";
$res = $adb->pquery($actionQuery, array('DashboardStructure', 'Active'));
for ($i = 0; $i < $adb->num_rows($res); $i++) {
    $actionsid = $adb->query_result($res, $i, 'actionsid');
    $actionsname = $adb->query_result($res, $i, 'reference');
    $fileList[] = array('id' => $actionsid, 'actionname' => $actionsname,'filelist'=>$csvList);
}
$allfiles = json_encode($fileList);
echo $allfiles;

?>
