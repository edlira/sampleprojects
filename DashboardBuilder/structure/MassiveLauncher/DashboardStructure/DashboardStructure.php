<?php
require_once('data/CRMEntity.php');
require_once('include/utils/CommonUtils.php');
require_once('include/ListView/ListView.php');
require_once('include/utils/utils.php');
require_once('modules/CustomView/CustomView.php');
require_once('Smarty_setup.php');
ini_set('max_execution_time',500);
$smarty = new vtigerCRM_Smarty;
global $adb,$log,$app_strings,$current_user,$theme,$currentModule,$mod_strings,$image_path,$category;
$actionids=array();
$actionQuery="SELECT * FROM  vtiger_actions 
      INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_actions.actionsid
      WHERE ce.deleted=0 AND elementtype_action=?
      AND actions_status=? " ;
 $res=$adb->pquery($actionQuery,array('Import','Active'));
for($i=0;$i<$adb->num_rows($res);$i++){
    $actionsid=$adb->query_result($res,$i,'actionsid');
    $actionsname=$adb->query_result($res,$i,'reference');
    $actionids[$actionsid]=$actionsname;
}

$smarty->assign('MODULE', $currentModule);
$smarty->assign('SINGLE_MOD', getTranslatedString('SINGLE_'.$currentModule));
$smarty->assign('CATEGORY', $category);
$smarty->assign('THEME', $thementity);
$smarty->assign('IMAGE_PATH', $image_path);
$smarty->assign("MOD", $mod_strings);
$smarty->assign("APP", $app_strings);
$smarty->assign('actionsarray', $actionids);
$smarty->display("modules/DashboardStructure/index.tpl");
?>
