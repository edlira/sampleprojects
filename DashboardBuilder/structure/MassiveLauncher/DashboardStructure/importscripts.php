<?php
require_once 'config.inc.php';
global $root_directory,$log;
$fileList=array();
$dirImport="cron/modules/ImportExport";
$myaction=$_REQUEST['kaction'];
if($myaction=='readfiles'){
    $adb->query("Delete from importlauncher");
    if(is_dir($dirImport)){
        foreach (scandir($dirImport) as $files){
            if ($files!="." && $files !=".." && strpos($files, '.') !== (int) 0){
            $instertquery=$adb->pquery("INSERT INTO importlauncher VALUES(?,?)",array('',$files));
            $selquery=$adb->query("SELECT id FROM importlauncher ORDER BY id DESC");
            $fileid=$adb->query_result($selquery,0,'id');
            $fileList[]=array('id'=>$fileid,'scriptname'=>$files);
            }

        }
    }
    $allfiles=json_encode($fileList);
 echo $allfiles;
}
elseif($myaction=='renamefiles'){
    $models=$_REQUEST['models'];
    $model_values=array();
    $model_values=json_decode($models);
    $mv=$model_values[0];
    $id_script=$mv->id;
    $new_name=$mv->scriptname;
    $q=$adb->pquery("SELECT * FROM importlauncher WHERE id=?",array($id_script));
    $old_name=$adb->query_result($q,0,'filename');
    shell_exec("cd $root_directory");
    shell_exec("mv cron/modules/ImportExport/$old_name cron/modules/ImportExport/$new_name");
    $adb->pquery("UPDATE importlauncher set filename=? where id=?",array($new_name,$id_script));
    
}
?>
