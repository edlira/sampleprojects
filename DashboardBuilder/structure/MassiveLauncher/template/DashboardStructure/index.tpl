<link href="include/kendoui/styles/kendo.common.css" rel="stylesheet"/>
<link href="include/kendoui/styles/kendo.uniform.css" rel="stylesheet"/>
<script src="modules/DashboardStructure/js/jquery-1.7.2.min.js"></script>
<script src="include/kendoui/js/kendo.web.js"></script>
<script src="include/kendoui/js/kendo.pager.js"></script>
<script src="modules/DashboardStructure/DashboardStructure.js"></script>
<link rel="stylesheet" type="text/css" href="modules/DashboardStructure/styles/jquery-ui-1.9.2.custom.css" />
<script type="text/javascript" src="modules/DashboardStructure/js/jquery-ui-1.9.2.custom.js"></script>
{literal}
    <script type="text/x-kendo-template" id="templaterefresh">
        <div class="toolbar">
        <input type="button" name="refreshbutt" id="refreshbutt" value="Refresh" class="k-button k-button-icontext"></input>
        </div>
    </script>
{/literal}
{literal}
    <script type="text/x-kendo-template" id="templatescript">
        <div class="toolbar">
        <input type="button" name="refreshscript" id="refreshscript" value="Refresh" class="k-button k-button-icontext"></input>
        </div>
    </script>
{/literal}
<script type="text/javascript">
    {literal}
      $(document).ready(function() {

          $("#importFiles").kendoUpload({
              upload: onUpload,
              async: {
                  saveUrl: "index.php?module=DashboardStructure&action=DashboardStructureAjax&file=manageFile",
                  autoUpload: true
              }

          });
          function onUpload(e) {
              var files = e.files;
              $.each(files, function() {
                  if (this.extension != ".csv") {
                      $.alert("Only .csv files can be uploaded", "Warning");
                      e.preventDefault();
                  }
              });
          }
          $("#tabs").tabs();
          $('#tabs').bind('tabsshow', function(event, ui) {
              if (ui.panel.id == "tabs-2") {
                  refreshGrid("scriptlist");
              }
          });
      });
      function refreshGrid(gridname) {
          var grid = $("#" + gridname).data("kendoGrid");
          grid.dataSource.read();

      }
      $.extend({alert: function(message, title) {
              $("<div></div>").dialog({
                  buttons: {"Ok": function() {
                          $(this).dialog("close");
                      }},
                  close: function(event, ui) {
                      $(this).remove();
                  },
                  resizable: false,
                  title: 'Warning!',
                  modal: true
              }).text(message);
          }
      });

    {/literal}
</script>
<style type='text/css'>
    {literal}
        .error {
            font-size:10px;
            color:#FF0000;
            font-style:italic;
        }
        ul.k-upload-files{
            height:250px;
            /*width:95%;*/
            overflow-y:scroll;
        }
        div.k-dropzone {
            border: 1px solid #c5c5c5; 
            height:100px;
            background-color:#FFFFCC;
            /*    width:95%;*/
            text-align:center;
        }

        div.k-dropzone em {
            visibility: visible;
        }
        #grid .k-toolbar
        {
            min-height: 27px;
        }
        .height-label .width-label
        {
            vertical-align: middle;
            padding-right: .5em;
        }
        #height, #width
        {
            vertical-align: middle;
        }
        .toolbar {
            float: right;
            margin-right: .8em;
        }

    {/literal}
</style>
{include file='Buttons_List1.tpl'}
<div id="tabs" style="padding:20px; width:95%">
    <ul>
        <li><a href="#tabs-1">{$MOD.importfiles}</a></li>
        <li><a href="#tabs-2">{$MOD.importscripts}</a></li>
    </ul>
    <div id="tabs-1">
        <div id="example" class="k-content" style="display:table;width:100%">
            <div id="grid" style="display:table-cell; width:50%;padding:20px;">
                <input name="importFiles" id="importFiles" type="file" />
            </div>
            <div id="grid1" style="display:table-cell;" > </div>
            <script>
                {literal}
                var myurl = 'index.php?module=DashboardStructure&action=DashboardStructureAjax&file=importfiles';
                $(document).ready(function() {
                    kendo.culture("en-US");
                    var dataSource = new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: myurl,
                                dataType: "json"

                            }
                        },
                        batch: true,
                        pageSize: 15,
                        schema: {
                            model: {
                                fields: {
                                    filename: {type: "string"},
                                    lastdate: {type: "string"}
                                }
                            }
                        }

                    });
                    $("#grid1").kendoGrid({
                        dataSource: dataSource,
                        pageable: true,
                        groupable: false,
                        height: 350,
                        filterable: false,
                        sortable: true,
                        toolbar: kendo.template($("#templaterefresh").html()),
                        resizable: true,
                        columns: [
                            {field: "filename", title: "Filename"},
                            {field: "lastdate", title: "Last Modified Date"},
                            {command: [{text: "Run", click: runfile, name: "run-file"}, {text: "Delete", click: deletefile, name: "delete-file"}, {text: "Download", click: downloadfile, name: "download-file"}], title: "Actions", width: "300px"}]

                    });
                    function runfile(e) {
                        kendo.ui.progress($("#grid1"), true);
                        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                        filename = dataItem.filename;
                        runAction(60544,filename,'Alert');
                        kendo.ui.progress($("#grid1"), false);
                    }
                    function deletefile(e) {
                        kendo.ui.progress($("#grid1"), true);
                        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                        filename = dataItem.filename;
                        $.ajax({
                            type: "POST",
                            url: "index.php",
                            data: "module=DashboardStructure&action=DashboardStructureAjax&ajax=true&file=removeFile&type=file&delfilename=" + filename,
                            success:
                                    function(result) {
                                        kendo.ui.progress($("#grid1"), false);
                                        $("#grid1").data("kendoGrid").dataSource.read();
                                    }
                        });
                    }
                    function downloadfile(e) {
                        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                        filename = dataItem.filename;
                        window.location.href = "import/" + filename;
                    }
                    $('#refreshbutt').bind('click', function(event) {
                        $("#grid1").data("kendoGrid").dataSource.read();
                    });
                });
                {/literal}
            </script>
        </div>
    </div>
      <div id="tabs-2">
    <div id="example1" class="k-content" style="display:table;width:100%">       
    <div id="scriptlist"></div>
    <div id="windowresults"></div>
    <br> <br>
    {literal}
        <script>              
        var myurl2='index.php?module=DashboardStructure&action=DashboardStructureAjax&file=importActions';
        $(document).ready(function () {
        kendo.culture("en-US"); 
             var datasourceretrievefiles=new kendo.data.DataSource({
                transport: {
                    read: {
                            url:'index.php?module=DashboardStructure&action=DashboardStructureAjax&file=importfiles',
                            dataType: "json"
                          }
                         },
                             serverFiltering: true
                });
        var  dataSource = new kendo.data.DataSource({
        transport: {
            read:  {
                    url: myurl2,                                          
                    dataType: "json"
                    }   
                },  
        batch:true,
        pageSize: 15,                      
    
        schema: {
        model: {
        id:"id",
        fields:{
        actionname: {type: "string",editable: false, },
        filelist: { type: "string"},
        }
        }
        }
        
        });
        $("#scriptlist").kendoGrid({
        dataSource: dataSource,
        pageable: true,
        groupable: false,
        height: 350, 
        filterable:false,
        sortable: true,
        resizable: true,
        editable:true,
        toolbar:kendo.template($("#templatescript").html()),
        columns:[
        {field:"actionname",title:"Action Name"},
        {field:"filelist",title:"CSV files",editor: retrievefiles},
        { command: [{ text: "Run", click: execscript,name:"run-script" }], title: "Actions" , width: "400px"}],
    
        }); 
        function retrievefiles(container, options) {
                $('<input name="' + options.field + '">').appendTo(container).kendoDropDownList({
                dataTextField: "filename",
                dataValueField: "filename",
                dataSource:datasourceretrievefiles

                });
            } 
       function execscript(e) {
            kendo.ui.progress($("#execscript"), true);
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            filelist = dataItem.filelist;
            id = dataItem.id;
            parameters="csvname="+filelist;
            runNewAction(id,parameters , 'Alert') 
            kendo.ui.progress($("#execscript"), false);
        }
  
        $('#refreshscript').bind('click',function (event){
        $("#scriptlist").data("kendoGrid").dataSource.read();
        });
        });
        {/literal}
    </script>
    </div>         
    </div>
