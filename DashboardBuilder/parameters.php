<?php

global $adb, $log, $currentModule;
$kaction = $_REQUEST['kaction'];
$dashboardName = $_REQUEST['dsName'];
$entityName = $_REQUEST['entityName'];
$records = array();

function findRoleid($rolname) {
	global $adb;
	$sql = "SELECT roleid FROM vtiger_role WHERE rolename=?";
	$result = $adb->pquery($sql, array($rolname));
	$roleid = $adb->query_result($result, 0, "roleid");
	return $roleid;
}

function findTabName($tabid) {
	global $adb, $dashboardName;
	$tabquery = $adb->pquery("SELECT tab_label FROM dashboardbuilder_tabs dstab
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dstab.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dstab.id=?", array($dashboardName, "Tabs", $tabid));
	$tabname = $adb->query_result($tabquery, 0, 'tab_label');
	return $tabname;
}

function findTabId($tabname) {
	global $adb, $dashboardName;
	$tabquery = $adb->pquery("SELECT id FROM dashboardbuilder_tabs dstab
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dstab.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dstab.tab_label=?", array($dashboardName, "Tabs", $tabname));
	$tabid = $adb->query_result($tabquery, 0, 'id');
	return $tabid;
}

function findBlockName($blockid) {
	global $adb, $dashboardName;
	$blockquery = $adb->pquery("SELECT block_label FROM dashboardbuilder_blocks dsblock
                                INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsblock.id
                                WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsblock.id=?", array($dashboardName, "Blocks", $blockid));
	$blockname = $adb->query_result($blockquery, 0, 'block_label');
	return $blockname;
}

function findBlockId($blockname) {
	global $adb, $dashboardName;
	$blockquery = $adb->pquery("SELECT id FROM dashboardbuilder_blocks dsblock
                               INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsblock.id
                               WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsblock.block_label=?", array($dashboardName, "Blocks", $blockname));
	$blockid = $adb->query_result($blockquery, 0, 'id');
	return $blockid;
}

function findActionName($actionid) {
	global $adb, $dashboardName;
	$actionquery = $adb->pquery("SELECT dsaction.name FROM dashboardbuilder_actions dsaction
                                 INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsaction.id
                                 WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsaction.id=?", array($dashboardName, "Actions", $actionid));
	$actionname = findBusinessActionName($adb->query_result($actionquery, 0, 'name'));
	return $actionname;
}

function findActionId($actionname) {
	global $adb, $dashboardName;
	$actionquery = $adb->pquery("SELECT id FROM dashboardbuilder_actions dsaction
                                INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsaction.id
                                WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsaction.name=?", array($dashboardName, "Actions", $actionname));
	$actionid = $adb->query_result($actionquery, 0, 'id');
	return $actionid;
}

function findParameterName($parameterid) {
	global $adb, $dashboardName;
	$parameterquery = $adb->pquery("SELECT dsparameter.name FROM dashboardbuilder_parameters dsparameter
                                    INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsparameter.id
                                    WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsparameter.id=?", array($dashboardName, "Parameters", $parameterid));
	$parametername = $adb->query_result($parameterquery, 0, 'name');
	return $parametername;
}

function findParameterId($parametername) {
	global $adb, $dashboardName;
	$parameterquery = $adb->pquery("SELECT id FROM dashboardbuilder_parameters dsparameter
                                    INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsparameter.id
                                    WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND dsparameter.name=?", array($dashboardName, "Parameters", $parametername));
	$parameterid = $adb->query_result($parameterquery, 0, 'id');
	return $parameterid;
}

function findBusinessActionName($actionid) {
	global $adb;
	$actionQuery = $adb->pquery("SELECT reference FROM  vtiger_actions 
                    INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_actions.actionsid
                    WHERE ce.deleted=0 AND actionsid=? ", array($actionid));
	$actioname = $adb->query_result($actionQuery, 0, 'reference');
	return $actioname;
}

//TO DO:
function addNewEntity($dashboardbuilder, $entityname) {
	global $adb;
	$entityQuery = $adb->pquery("INSERT INTO dashboardbuilder_entities VALUES (?,?,?,?,?,?)", array("", $entityname, $dashboardbuilder, '', '', 0));
	$lastID = $adb->getLastInsertID();
	return $lastID;
}

if ($kaction == 'roleslist') {
	$content = array();
	$allroles = $adb->query("SELECT * FROM vtiger_role");
	while ($allroles && $row = $adb->fetch_array($allroles)) {
		$content['id'] = $row['roleid'];
		$content['rolename'] = $row['rolename'];
		$records[] = $content;
	}
	echo json_encode($records);
}
if ($kaction == 'userslist') {
	$content = array();
	$allusers = $adb->query("SELECT * FROM vtiger_users WHERE deleted=0");
	while ($allusers && $row = $adb->fetch_array($allusers)) {
		$content['id'] = $row['id'];
		$content['username'] = $row['user_name'];
		$records[] = $content;
	}
	echo json_encode($records);
}
if ($kaction == 'relatedfields') {
	$fieldarray = array();
	$filters = $_REQUEST['filter']['filters'][0]['value'];
	if ($filters != '') {
		$query = $adb->query("SELECT * FROM vtiger_field WHERE tabid=$filters");
		$count = $adb->num_rows($query);
		for ($i = 0; $i < $count; $i++) {
			$fieldarray[$i]['id'] = $adb->query_result($query, $i, 'fieldid');
			$fieldarray[$i]['fieldname'] = $adb->query_result($query, $i, 'fieldname');
		}
		//echo json_encode($fieldarray);
	}
	echo json_encode($fieldarray);
}
if ($kaction == 'modulelist') {
	$modulearray = array();
	$filters = $_REQUEST['filter']['filters'][0]['value'];
	if ($filters != '') {
		$getBlockModule = $adb->query("SELECT vtiger_tab.name FROM vtiger_tab 
                             INNER JOIN dashboardbuilder_blocks ON dashboardbuilder_blocks.block_module=vtiger_tab.tabid
                             WHERE  dashboardbuilder_blocks.id=$filters");
		$moduleName = $adb->query_result($getBlockModule, 0, "name");
		$getRelModules = $adb->query("SELECT DISTINCT vtiger_fieldmodulerel.relmodule as name
                            FROM vtiger_tab 
                            INNER JOIN vtiger_fieldmodulerel ON vtiger_tab.name=vtiger_fieldmodulerel.module 
                            WHERE module='$moduleName'");
		$nr = $adb->num_rows($getRelModules);
		$modulearray[0]['id'] = getTabid($moduleName);
		$modulearray[0]['name'] = $moduleName;
		for ($i = 0; $i < $nr; $i++) {
			$name = $adb->query_result($getRelModules, $i, 'name');
			$modulearray[$i + 1]['id'] = getTabid($name);
			$modulearray[$i + 1]['name'] = $name;
		}
	} else {
		$allmodules = $adb->query("SELECT * FROM vtiger_tab WHERE presence=0 AND isentitytype=1");
		$count = $adb->num_rows($allmodules);
		for ($i = 0; $i < $count; $i++) {
			$modulearray[$i]['id'] = $adb->query_result($allmodules, $i, 'tabid');
			$modulearray[$i]['name'] = $adb->query_result($allmodules, $i, 'name');
		}
	}
	echo json_encode($modulearray);
}
if ($kaction == 'businessactionslist') {
	$actionarray = array();
	$actionQuery = "SELECT * FROM  vtiger_actions 
                    INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_actions.actionsid
                    WHERE ce.deleted=0 AND moduleactions=? AND actions_status=? ";
	$res = $adb->pquery($actionQuery, array($currentModule, 'Active'));
	for ($i = 0; $i < $adb->num_rows($res); $i++) {
		$actionarray[$i]['id'] = $adb->query_result($res, $i, 'actionsid');
		$actionarray[$i]['name'] = $adb->query_result($res, $i, 'reference');
	}
	echo json_encode($actionarray);
}
if ($kaction == 'parenttablist') {
	$sql = $adb->query("SELECT * FROM vtiger_parenttab WHERE visible=0 ORDER BY sequence");
	$count = $adb->num_rows($sql);
	$parenttablist = array();
	for ($i = 0; $i < $count; $i++) {
		$parenttablist[$i]['id'] = $adb->query_result($sql, $i, 'parenttabid');
		$parenttablist[$i]['name'] = $adb->query_result($sql, $i, 'parenttab_label');
	}
	echo json_encode($parenttablist);
}

function moduleExists($modulename) {
	global $adb;
	$selecttab = $adb->pquery("SELECT tabid FROM vtiger_tab WHERE name=?", array($modulename));
	if ($adb->num_rows($selecttab) > 0)
		echo "no";
	else
		echo "yes";
}

/*
 * CRUD EXTENSION
 */
if ($kaction == 'retrieveextension') {
	$allblocks = $adb->query("SELECT * FROM dashboardbuilder_extensions");
	while ($allblocks && $row = $adb->fetch_array($allblocks)) {
		$content['id'] = $row['id'];
		$content['name'] = $row['name'];
		$content['label'] = $row['label'];
		$content['type'] = $row['type'];
		$content['template'] = $row['template'];
		$content['parenttab'] = $row['parenttab'];
		$content['generated'] = $row['generated'] == 1 ? true : false;
		$records[] = $content;
	}
	global $log;
	$log->debug("this is ext");
	$log->debug($records);
	echo json_encode($records);
} elseif ($kaction == 'updateextension') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];

	$adb->pquery("UPDATE dashboardbuilder_extensions SET name=?,label=?,type= ?,template=?, parenttab=? WHERE id=?", array($mv->name, $mv->label, $mv->type, $mv->template, $mv->parenttab, $mv->id));
} elseif ($kaction == 'deleteextension') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;

	$adb->pquery("DELETE FROM dashboardbuilder_extensions  WHERE id=?", array($id));
	//echo $query;  
} elseif ($kaction == 'createextension') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];

	$adb->pquery("INSERT INTO dashboardbuilder_extensions(id,name,label,type,template, parenttab) VALUES(?,?,?,?,?,?)", array('', $mv->name, $mv->label, $mv->type, $mv->template, $mv->parenttab));
	echo true;
}
/*
 * CRUD TAB
 */
if ($kaction == 'retrievetab') {
	$alltabs = $adb->pquery("SELECT * from dashboardbuilder_tabs dstab
                            INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dstab.id
                            WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, $entityName));
	while ($alltabs && $row = $adb->fetch_array($alltabs)) {
		$content['id'] = $row['id'];
		$content['tab_label'] = $row['tab_label'];
		$content['tab_sequence'] = $row['tab_sequence'];
		$role = getRoleName($row['tab_roles']);
		$content['tab_roles'] = $role ? $role : "";
		$user = getUserName($row['tab_users']);
		$content['tab_users'] = $user ? $user : "";
		$records[] = $content;
	}
	echo json_encode($records);
} elseif ($kaction == 'updatetab') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];

	$adb->pquery("UPDATE dashboardbuilder_tabs SET tab_label=?,tab_sequence=?,tab_roles=?,tab_users=? WHERE id=?", array($mv->tab_label, $mv->tab_sequence, findRoleid($mv->tab_roles), getUserId_Ol($mv->tab_users), $mv->id));
} elseif ($kaction == 'deletetab') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;

	$adb->pquery("UPDATE dashboardbuilder_entities SET deleted=1 WHERE dsid=?", array($id));
	echo "true";
} elseif ($kaction == 'createtab') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$roleids = findRoleid($mv->tab_roles);
	$userids = getUserId_Ol($mv->tab_users);
	$newID = addNewEntity("Tabs", $dashboardName);
	$adb->pquery("INSERT INTO dashboardbuilder_tabs(id,tab_label,tab_sequence,tab_roles,tab_users) values(?,?,?,?,?)", array($newID, $mv->tab_label, $mv->tab_sequence, $roleids, $userids));
	echo true;
}
/*
 * CRUD BLOCK
 */ elseif ($kaction == 'retrieveblock') {
	$log->debug("now retrieve");
	$allblocks = $adb->pquery("SELECT * from dashboardbuilder_blocks dsblock
                            INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsblock.id
                            WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, $entityName));
	while ($allblocks && $row = $adb->fetch_array($allblocks)) {
		$content['id'] = $row['id'];
		$content['block_label'] = $row['block_label'];
		$content['block_sequence'] = $row['block_sequence'];
		$content['block_module'] = getTabname($row['block_module']);
		$blockRole = getRoleName($row['block_roles']);
		$content['block_roles'] = $blockRole ? $blockRole : "";
		$blockUser = getUserName($row['block_users']);
		$content['block_users'] = $blockUser ? $blockUser : "";
		$tabName = findTabName($row['block_tab']);
		$content['block_tab'] = $tabName ? $tabName : "";
		$content['block_action'] = findActionName($row['block_action']);

		$records[] = $content;
	}
	$log->debug($records);
	echo json_encode($records);
} elseif ($kaction == 'updateblock') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$log->debug('modeliiii update');
	$log->debug($mv);
	$adb->pquery("UPDATE dashboardbuilder_blocks SET block_label=?,block_sequence=?,block_module=?, block_tab=?,block_roles=?,block_users=?,block_action=? WHERE id=?", array($mv->block_label, $mv->block_sequence, getTabid("$mv->block_module"), findTabId($mv->block_tab), findRoleid($mv->block_roles), getUserId_Ol($mv->block_users), $mv->block_action, $mv->id));
} elseif ($kaction == 'deleteblock') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;
	$adb->pquery("UPDATE dashboardbuilder_entities SET deleted=1 WHERE dsid=?", array($id));
	echo true;
} elseif ($kaction == 'createblock') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$moduleName = getTabid($mv->block_module);
	$roleids = findRoleid($mv->block_roles);
	$userids = getUserId_Ol($mv->block_users);
	$tabid = findTabId($mv->block_tab);
	$newID = addNewEntity("Blocks", $dashboardName);
	$adb->pquery("INSERT INTO dashboardbuilder_blocks(id,block_label,block_sequence,block_module,block_tab,block_roles,block_users,block_action) values(?,?,?,?,?,?,?,?)", array($newID, $mv->block_label, $mv->block_sequence, $moduleName, $tabid, $roleids, $userids, $mv->block_action));
	echo true;
} elseif ($kaction == 'tablist') {
	$tabsarray = array();
	$alltabs = $adb->pquery("SELECT * from dashboardbuilder_tabs dstab
                            INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dstab.id
                            WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, "Tabs"));
	$count = $adb->num_rows($alltabs);

	for ($i = 0; $i < $count; $i++) {
		$tabsarray[$i]['id'] = $adb->query_result($alltabs, $i, 'id');
		$tabsarray[$i]['tabname'] = $adb->query_result($alltabs, $i, 'tab_label');
	}
	echo json_encode($tabsarray);
}
/*
 * CRUD FIELDS
 */ elseif ($kaction == 'retrievefield') {
	$allblocks = $adb->pquery("SELECT * FROM dashboardbuilder_fields dsfield
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsfield.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, 'Fields'));
	while ($allblocks && $row = $adb->fetch_array($allblocks)) {
		$content['id'] = $row['id'];
		$content['fieldname'] = $row['fieldname'];
		$content['fieldlabel'] = $row['fieldlabel'];
		$content['fieldtype'] = $row['fieldtype'];
		$content['mandatory'] = $row['mandatory'] == 1 ? true : false;
		$blockName = findBlockName($row['block']);
		$content['block'] = $blockName ? $blockName : "";
		$tabName = getTabname($row['module']);
		$content['module'] = $tabName ? $tabName : "";
		$content['modulefield'] = $row['modulefield'];
		$content['operator'] = $row['operator'];
		$content['field_sequence'] = $row['field_sequence'];
		$content['listview'] = $row['listview'] == 1 ? true : false;
		$records[] = $content;
	}
	echo json_encode($records);
} elseif ($kaction == 'updatefield') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$blockId = $mv->block;
	$tabId = $mv->module;
	$adb->pquery("UPDATE dashboardbuilder_fields SET fieldname=?,fieldlabel=?,fieldtype=?,mandatory=?,block=?,module=?,modulefield=?,operator=?, field_sequence=?, listview=? WHERE id=", array($mv->fieldname, $mv->fieldlabel, $mv->fieldtype, $mv->mandatory, $blockId, $tabId, $mv->modulefield, $mv->operator, $mv->field_sequence, $mv->listview, $mv->id));
	echo true;
} elseif ($kaction == 'deletefield') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;
	$adb->pquery("UPDATE dashboardbuilder_entities SET deleted=1 WHERE dsid=?", array($id));
	echo true;
} elseif ($kaction == 'createfield') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$newID = addNewEntity("Fields", $dashboardName);
	$adb->pquery("INSERT INTO dashboardbuilder_fields(id,fieldname,fieldlabel,fieldtype,mandatory,block,module,modulefield,operator,field_sequence,listview) VALUES(?,?,?,?,?,?,?,?,?,?,?)", array($newID, $mv->fieldname, $mv->fieldlabel, $mv->fieldtype, $mv->mandatory, $mv->block, $mv->module, $mv->modulefield, $mv->operator, $mv->field_sequence, $mv->listview));
	echo true;
} elseif ($kaction == 'blocklist') {
	$blocksarray = array();
	$allblocks = $adb->pquery("SELECT dsblock.* FROM dashboardbuilder_blocks dsblock
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsblock.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, "Blocks"));
	$count = $adb->num_rows($allblocks);

	for ($i = 0; $i < $count; $i++) {
		$blocksarray[$i]['id'] = $adb->query_result($allblocks, $i, 'id');
		$blocksarray[$i]['blockname'] = $adb->query_result($allblocks, $i, 'block_label');
	}
	echo json_encode($blocksarray);
}
/*
 * CRUD ACTIONS
 */ elseif ($kaction == 'retrieveaction') {
	$allblocks = $adb->pquery("SELECT dsaction.* FROM dashboardbuilder_actions dsaction
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsaction.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, $entityName));
	while ($allblocks && $row = $adb->fetch_array($allblocks)) {
		$content['id'] = $row['id'];
		$name = findBusinessActionName($row['name']);
		$content['name'] = $name ? $name : "";
		$content['label'] = $row['label'];
		$content['block'] = findBlockName($row['block']);
		$content['autoload'] = $row['autoload'] == 1 ? true : false;
		;
		$content['sequence'] = $row['sequence'];
		$records[] = $content;
	}
	echo json_encode($records);
} elseif ($kaction == 'updateaction') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$adb->pquery("UPDATE dashboardbuilder_actions SET name=?,label=?,block=?,autoload=?,sequence=? WHERE id=?", array($mv->name, $mv->label, findBlockId("$mv->block"), $mv->autoload, $mv->sequence, $mv->id));
} elseif ($kaction == 'deleteaction') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;

	$adb->pquery("UPDATE dashboardbuilder_entities SET deleted=1 WHERE dsid=?", array($id));
	//echo $query;  
} elseif ($kaction == 'createaction') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$newID = addNewEntity("Actions", $dashboardName);
	$adb->pquery("INSERT INTO dashboardbuilder_actions(id,name,label,block,autoload,sequence) VALUES(?,?,?,?,?,?)", array($newID, $mv->name, $mv->label, findBlockId($mv->block), $mv->autoload, $mv->sequence));
	echo true;
}
/*
 * CRUD PARAMETERS
 */ elseif ($kaction == 'retrieveparameter') {
	$allblocks = $adb->pquery("SELECT dsparameter.* FROM dashboardbuilder_parameters dsparameter
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsparameter.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, $entityName));
	while ($allblocks && $row = $adb->fetch_array($allblocks)) {
		$content['id'] = $row['id'];
		$content['name'] = $row['name'];
		$content['label'] = $row['label'];
		$content['isgeneral'] = $row['isgeneral'] == 1 ? true : false;
		$content['action'] = findActionName($row['action']);
		$content['parent_parameter'] = findParameterName($row['parent_parameter']);
		$content['sequence'] = $row['sequence'];
		$records[] = $content;
	}
	echo json_encode($records);
} elseif ($kaction == 'updateparameter') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$adb->pquery("UPDATE dashboardbuilder_parameters SET name=?,label=?,isgeneral=?,action=?,parent_parameter=?,sequence=? WHERE id=?", array($mv->name, $mv->label, $mv->isgeneral, $mv->action, findParameterId($mv->parent_parameter), $mv->sequence, $mv->id));
} elseif ($kaction == 'deleteparameter') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;

	$adb->pquery("UPDATE dashboardbuilder_entities SET deleted=1 WHERE dsid=?", array($id));
} elseif ($kaction == 'createparameter') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$newID = addNewEntity("Parameters", $dashboardName);
	$adb->pquery("INSERT INTO dashboardbuilder_parameters(id,name,label,isgeneral,action,parent_parameter,sequence) VALUES(?,?,?,?,?,?,?)", array($newID, $mv->name, $mv->label, $mv->isgeneral, $mv->action, findParameterId($mv->parent_parameter), $mv->sequence));
} elseif ($kaction == 'actionlist') {
	$blocksarray = array();
	$allblocks = $adb->pquery("SELECT dsaction.id,dsaction.name FROM dashboardbuilder_actions dsaction
                               INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsaction.id
                               WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, 'Actions'));
	$count = $adb->num_rows($allblocks);

	for ($i = 0; $i < $count; $i++) {
		$blocksarray[$i]['id'] = $adb->query_result($allblocks, $i, 'id');
		$blocksarray[$i]['actionname'] = findBusinessActionName($adb->query_result($allblocks, $i, 'name'));
	}
	$log->debug("hereee");
	$log->debug($blocksarray);
	echo json_encode($blocksarray);
}
/*
 * CRUD FIELDS PARAMETERS
 */
if ($kaction == 'retrievefieldparam') {
	$allblocks = $adb->pquery("SELECT dsfieldparam.* from dashboardbuilder_fieldparams dsfieldparam
                               INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsfieldparam.id
                               WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, $entityName));
	while ($allblocks && $row = $adb->fetch_array($allblocks)) {
		$content['id'] = $row['id'];
		$content['name'] = $row['name'];
		$content['label'] = $row['label'];
		$content['input_name'] = $row['input_name'];
		$content['type'] = $row['type'];
		$content['default_value'] = $row['default_value'];
		$content['parameter'] = findParameterName($row['parameter']);
		$tabName = getTabname($row['module']);
		$content['module'] = $tabName ? $tabName : "";
		$content['modulefield'] = $row['modulefield'];
		$content['sequence'] = $row['sequence'];
		$records[] = $content;
	}
	echo json_encode($records);
} elseif ($kaction == 'updatefieldparam') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$adb->pquery("UPDATE dashboardbuilder_fieldparams SET name=?,label=?,input_name=?,type=?,default_value=?,parameter=?,module=?,modulefield=?,sequence=? WHERE id=?", array($mv->name, $mv->label, $mv->input_name, $mv->type, $mv->default_value, findParameterId($mv->parameter), $mv->module, $mv->modulefield, $mv->sequence, $mv->id));
} elseif ($kaction == 'deletefieldparam') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$id = $mv->id;

	$adb->pquery("UPDATE dashboardbuilder_entities SET deleted=1 WHERE dsid=?", array($id));
	//echo $query;  
} elseif ($kaction == 'createfieldparam') {
	$models = $_REQUEST['models'];
	$model_values = array();
	$model_values = json_decode($models);
	$mv = $model_values[0];
	$newID = addNewEntity("FieldParams", $dashboardName);
	$adb->pquery("INSERT INTO dashboardbuilder_fieldparams(id,name,label,input_name,type,default_value,parameter,module,modulefield,sequence) values(?,?,?,?,?,?,?,?,?,?)", array($newID, $mv->name, $mv->label, $mv->input_name, $mv->type, $mv->default_value, findParameterId($mv->parameter), $mv->module, $mv->modulefield, $mv->sequence));
} elseif ($kaction == 'parameterlist') {
	$blocksarray = array();
	$allblocks = $adb->pquery("SELECT dsparameter.* FROM dashboardbuilder_parameters dsparameter
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsparameter.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0", array($dashboardName, "Parameters"));
	$count = $adb->num_rows($allblocks);

	for ($i = 0; $i < $count; $i++) {
		$blocksarray[$i]['id'] = $adb->query_result($allblocks, $i, 'id');
		$blocksarray[$i]['parametername'] = $adb->query_result($allblocks, $i, 'name');
	}
	echo json_encode($blocksarray);
} elseif ($kaction == 'parentparameterlist') {
	$blocksarray = array();
	$allblocks = $adb->pquery("SELECT dsparameter.* FROM dashboardbuilder_parameters dsparameter
                              INNER JOIN dashboardbuilder_entities ds_en ON ds_en.dsid=dsparameter.id
                              WHERE ds_en.name=? AND ds_en.entity=? AND ds_en.deleted=0 AND (dsparameter.parent_parameter IS NULL OR dsparameter.parent_parameter='')", array($dashboardName, "Parameters"));
	$count = $adb->num_rows($allblocks);

	for ($i = 0; $i < $count; $i++) {
		$blocksarray[$i]['id'] = $adb->query_result($allblocks, $i, 'id');
		$blocksarray[$i]['parametername'] = $adb->query_result($allblocks, $i, 'name');
	}
	echo json_encode($blocksarray);
}
?>
