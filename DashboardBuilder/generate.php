<?php

global $log, $adb;
$result = array();
$moduleName = $_REQUEST['extensionName'];
$parentName = $_REQUEST['parentName'];
$extensionType = $_REQUEST['extensionType'];
$templateType = $_REQUEST['templateType'];
if(empty($templateType)){
	$templateType = "Default";
}
$baseFolder = "modules/DashboardBuilder/structure/$extensionType/DashboardStructure";
if (!file_exists("modules/$moduleName/$moduleName.php")) {
	shell_exec("mkdir modules/$moduleName");
	//chmod("modules/$moduleName/", 0777);
	shell_exec("cp -r $baseFolder/*  modules/$moduleName");
	shell_exec('mv  modules/' . $moduleName . '/DashboardStructure.php modules/' . $moduleName . '/' . $moduleName . '.php');
	shell_exec('mv  modules/' . $moduleName . '/DashboardStructureAjax.php modules/' . $moduleName . '/' . $moduleName . 'Ajax.php');
	shell_exec('mv  modules/' . $moduleName . '/DashboardStructure.js modules/' . $moduleName . '/' . $moduleName . '.js');
	shell_exec('mv  modules/' . $moduleName . '/addNewExtensionDashboardStructure.php modules/' . $moduleName . '/addNewExtension' . $moduleName . '.php');
	//shell_exec("chmod 777 -R modules/$moduleName/");

	$str = file_get_contents("modules/$moduleName/$moduleName.php");
	$str = str_replace("DashboardStructure", "$moduleName", $str);
	file_put_contents("modules/$moduleName/$moduleName.php", $str);
	$str = file_get_contents("modules/$moduleName/$moduleName.js");
	$str = str_replace("DashboardStructure", "$moduleName", $str);
	file_put_contents("modules/$moduleName/$moduleName.js", $str);
	$str = file_get_contents("modules/$moduleName/addNewExtension$moduleName.php");
	$str = str_replace("DASHBOARD_NAME", "$moduleName", $str);
	$str = str_replace("PARENT_NAME", "$parentName", $str);
	file_put_contents("modules/$moduleName/addNewExtension$moduleName.php", $str);
	shell_exec("cp modules/$moduleName/addNewExtension$moduleName.php  addNewExtension$moduleName.php");

	shell_exec("mkdir Smarty/templates/modules/$moduleName");
	//chmod("Smarty/templates/modules/$moduleName/", 0777);
	shell_exec("cp -r modules/DashboardBuilder/structure/$extensionType/template/DashboardStructure/$templateType/*  Smarty/templates/modules/$moduleName");
	//shell_exec("chmod 777 -R Smarty/templates/modules/$moduleName/");


	shell_exec("php addNewExtension$moduleName.php");
	$adb->pquery("UPDATE dashboardbuilder_extensions SET generated=1 WHERE name=?", array($moduleName));
	$result['message'] = "The dashboad is generated successfully";
} else {
	$result['message'] = $moduleName . " is already installed in the system.";
}
if (empty($result['message'])) {
	$result['message'] = "An error occurred processing your request";
}
echo json_encode($result['message'], true);
?>
