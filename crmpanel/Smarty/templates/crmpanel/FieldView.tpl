{assign var="fldlabel" value=$fielddata.fieldlabel}
{assign var="fldname" value=$fielddata.fieldname}
{assign var="fldtype" value=$fielddata.fieldtype}
{assign var="typeofdata" value=$fielddata.mandatory}
{assign var="fldvalue" value=$fielddata.fieldvalue}
{assign var="relmodule" value=$fielddata.relmodule}
{if $typeofdata eq '1'}
        {assign var="mandatory_field" value="*"}
{else}
        {assign var="mandatory_field" value=""}
{/if}
<td width=20% class="dvtCellLabel" align=right>
    <font color="red">{$mandatory_field}</font>
   {$fldlabel}
</td>

<td width="30%" align=left class="dvtCellInfo">
{if $fldtype eq 'Textbox'}
   <input type="text" name="{$fldname}">
{elseif $fldtype eq 'Checkbox'}
   <input type="checkbox" name="{$fldname}"> 
{elseif $fldtype eq 'Date'}  
   <input name="{$fldname}" tabindex="15" id="jscal_field_{$fldname}" type="text" style="border:1px solid #bababa;" size="11" maxlength="10" class="datepicker">
    <br><font size=1><em old="(yyyy-mm-dd)">({$dateStr})</em></font>
{elseif $fldtype eq 'UItype10'}
   <input name="{$fldname}id" id="{$fldname}id" type="hidden" value="">
   <input name="{$fldname}id_display" id="{$fldname}id_display" readonly="readonly" value="" style="border: 1px solid rgb(186, 186, 186);" type="text">&nbsp;
   <img src="{'select.gif'|@vtiger_imageurl:$THEME}" tabindex="20" alt="Select" title="Select" language="javascript" onclick='return window.open("index.php?module={$relmodule}&action=Popup&html=Popup_picker&form=vtlibPopupView&forfield={$fldname}id","test","width=640,height=602,resizable=0,scrollbars=0,top=150,left=200");' style="cursor: pointer;" align="absmiddle">&nbsp;
   <input src="{'clear_field.gif'|@vtiger_imageurl:$THEME}" alt="Clear" title="Clear" language="javascript" onclick="this.form.{$fldname}id.value=''; this.form.{$fldname}id_display.value=''; return false;" style="cursor: pointer;" align="absmiddle" type="image">
{elseif $fldtype eq 'Picklist' || $fldtype eq 'Filter'}
    <select name="{$fldname}" class="singlecombo" tabindex="10">
	 {foreach key=key item=value from=$fldvalue}
		<option value="{$key}">{$value}</option>
	 {/foreach}
    </select>
    
{/if}
</td>