<div class="summary-panel column3" style="text-align: center;">
	<div class="column  first" style="text-align: center;margin-left:250px;">
		<h3>Results</h3>
	</div>
</div>
<div id="opportunities" class="pageRows">
	<table width="100%" style="width:100%;" border="0" ng-table="tableParams">
		<tr>
			<td style="text-align: left;width:3%;" >
				<input type="checkbox" ng-model="select_all" id="select_all" name="filter-checkbox" ng-click="selectAll();"/>
			</td>
			<!--<td width="5%" style="text-align: left;">
				<b>Title</b><br/>
			</td>-->
			<td ng-repeat="fieldlabel in  fieldlabels" style="text-align: left;padding-top:15px;width:14%;padding-right: 0px;margin-right: 0px;">
				<b>{literal}{{fieldlabel}}{/literal}</b>
			</td>
		</tr>
		<tr ng-repeat="(recKey,records) in $data">
			<td width="1%" style="text-align: left;width:3%;" >
				<input type="checkbox" ng-model="checkboxes.items[records.id]" />
			</td>
			<td height="10"   ng-repeat="(key, value) in  columns" style="height:15px;padding-top:15px;text-align: left;width:14%;padding-right: 0px;margin-right: 0px;">
		            <span ng-if="!editmode || !checkboxes.items[records.id] || value==='id'">{literal}{{records[value]}}{/literal}</span>
                            <md-input-container ng-if="editmode && checkboxes.items[records.id] && value!=='id'"><input  style="width:100%; background-color: lightcyan" type="text" ng-model ="records[value]"></md-input-container>
			</td>
                        <td width="1%" style="text-align: left;width:3%; padding-top:15px" >
                        <md-button class="md-raised md-primary add-action" ng-disabled="!checkboxes.items[records.id]" ng-click="saveRecordChanges(records,recKey);">{literal}{{editButtonName}}{/literal}</md-button>
			</td>
		</tr>
	</table>


	<div>
		{include file="modules/$MODULE/ParametersView.tpl"}
	</div>
</div> 