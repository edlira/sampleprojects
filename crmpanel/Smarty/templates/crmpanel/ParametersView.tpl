<accordion close-others="oneAtATime" >
		<accordion-group ng-if="action.paramlabel!=='Save Changes'" ng-repeat="action in blockParameters" id="{literal}{{action.paramname}}{/literal}">
			<accordion-heading>
				<span ng-click="handleAccordion(action.paramAction.actionautoload,action.paramname,fldValuesParam[action.paramAction.actionid],action.paramAction.actionname,action.paramAction.actionoutput,checkboxes.items,'',checkboxesparam[action.paramname].items);">{literal}{{action.paramlabel}}{/literal}</span>
			</accordion-heading>
			<table width="100%" style="width:100%;" border="0" ng-table="tableParams{literal}{{action.paramname}}{/literal}">
				<tr>
					<td style="text-align: left;width:3%;" >
						<input type="checkbox" ng-model="select_all[action.paramname]" id="select_all{literal}{{action.paramname}}{/literal}" name="filter-checkbox" ng-click="selectAll();"/>
					</td>
					<!--<td width="5%" style="text-align: left;">
						<b>Title</b><br/>
					</td>-->
					<td ng-repeat="fieldlabel in  fieldlabels[action.paramname]" style="text-align: left;padding-top:15px;width:14%;padding-right: 0px;margin-right: 0px;">
						<b>{literal}{{fieldlabel}}{/literal}</b>
					</td>
				</tr>
			<tr ng-repeat="records in $data">
				<td width="1%" style="text-align: left;width:3%;" >
					<input type="checkbox" ng-model="checkboxesparam[action.paramname].items[records.id]" ng-click="defaultValues();" />
				</td>
				<td height="10" ng-repeat="(key, value) in  columns[action.paramname]" style="height:15px;padding-top:15px;text-align: left;width:14%;padding-right: 0px;margin-right: 0px;">
					<span>{literal}{{records[value]}}{/literal}</span>
				</td>
			</tr>
		</table>
		<table>
			<tr> 
				<td ng-repeat="fields in action.paramfields">
				{literal}{{fields.fieldlabel}}{/literal}
				<span ng-switch on="fields.fieldtype">
					<textarea ng-switch-when="Textbox" ng-model="fldValuesParam[action.paramAction.actionid][fields.fieldname]"></textarea>
					<input type="checkbox" ng-switch-when="Checkbox" ng-model="fldValuesParam[action.paramAction.actionid][fields.fieldname]"/>
					<input type="date" ng-switch-when="Date" ng-model="fldValuesParam[action.paramAction.actionid][fields.fieldname]"/>
					<input type="text" ng-switch-when="UItype10" ng-model="fldValuesParam[action.paramAction.actionid][fields.fieldname]" />
					<select name="field"  tabindex="10" ng-switch-when="Picklist" ng-model="fldValuesParam[action.paramAction.actionid][fields.fieldname]">
						<option ng-repeat="fld in fields.fieldvalue" value="{literal}{{fld}}{/literal}">{literal}{{fld}}{/literal}</option>
					</select>
				</span>
			</td>
		</tr>
		<tr> 
			<td>
			{literal}{{fldValuesParam[action.paramAction.actionid]}}{/literal}
			<div id="buttons" align="center">
				<md-button class="md-ink-ripple md-primary add-action" ng-disabled="enable_action"  
						   ng-click="findWSResults(action.paramname,fldValuesParam[action.paramAction.actionid],action.paramAction.actionname,action.paramAction.actionoutput,checkboxes.items,'',checkboxesparam[action.paramname].items);">
				{literal}{{action.paramAction.actionlabel}}{/literal}
			</md-button>
			<md-button class="md-ink-ripple md-primary add-action" ng-disabled="enable_action" ng-show="action.paramAction.paramname=='upgradeparams'" 
					   ng-click="findWSResults(action.paramname,fldValuesParam[action.paramAction.actionid],action.paramAction.actionname,action.paramAction.actionoutput,checkboxes.items,'',checkboxesparam[action.paramname].items);">
			{literal}{{action.paramAction.actionlabel}}{/literal}
		</md-button>
	</div> 
</td>
</tr>
</table>
<accordion close-others="oneAtATime" >
	<accordion-group ng-if="widget.paramlabel!=='Delete Subscription' && widget.paramlabel!=='Edit Subscription' " ng-repeat="widget in action.paramDepend" id="{literal}{{widget.paramname}}{/literal}">
		<accordion-heading>
			<span ng-click="handleAccordion(widget.paramAction.actionautoload,widget.paramname,fldValuesParam[widget.paramAction.actionid],widget.paramAction.actionname,widget.paramAction.actionoutput,checkboxes.items,checkboxesparam[action.paramname].items,checkboxesparam[widget.paramname].items,action.paramname);">{literal}{{widget.paramlabel}}{/literal}</span>
		</accordion-heading>
			<table width="100%" style="width:100%;" border="0" ng-table="tableParams{literal}{{widget.paramname}}{/literal}">
				<tr>
					<td style="text-align: left;width:3%;" >
						<input type="checkbox" ng-model="select_all[widget.paramname]" id="select_all{literal}{{widget.paramname}}{/literal}" name="filter-checkbox" ng-click="selectAll();"/>
					</td>
					<!--<td width="5%" style="text-align: left;">
						<b>Title</b><br/>
					</td>-->
					<td ng-repeat="fieldlabel in  fieldlabels[widget.paramname]" style="text-align: left;padding-top:15px;width:14%;padding-right: 0px;margin-right: 0px;">
						<b>{literal}{{fieldlabel}}{/literal}</b>
					</td>
				</tr>
				<tr ng-repeat="records in $data">
					<td width="1%" style="text-align: left;width:3%;" >
						<input type="checkbox" ng-model="checkboxesparam[widget.paramname].items[records.id]"/>
					</td>
					<td height="10" ng-repeat="(key, value) in  columns[widget.paramname]" style="height:15px;padding-top:15px;text-align: left;width:14%;padding-right: 0px;margin-right: 0px;">
						<span>{literal}{{records[value]}}{/literal}</span>
					</td>
				</tr>
			</table>
			<table>
				<tr> 
					<td ng-repeat="fieldsdata in widget.paramfields">

					{literal}{{fieldsdata.fieldlabel}}{/literal}
					<span ng-switch on="fieldsdata.fieldtype">

						<textarea ng-switch-when="Textbox" name="{literal}{{fieldsdata.fieldname}}{/literal}" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.fieldname]"></textarea>
						<input type="checkbox" name="{literal}{{fieldsdata.fieldname}}{/literal}" ng-switch-when="Checkbox" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.fieldname]"/>
						<input type="text" name="{literal}{{fieldsdata.fieldname}}{/literal}" ng-switch-when="UItype10" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.fieldname]" />
						<input type="date" ng-switch-when="Date" name="{literal}{{fieldsdata.fieldname}}{/literal}"  ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.fieldname]"/>					
						<select name="field"  name="{literal}{{fieldsdata.fieldname}}{/literal}" tabindex="10" ng-switch-when="Picklist" ng-model="fldValuesParam[widget.paramAction.actionid][fieldsdata.fieldname]">
							<option ng-repeat="fld in fieldsdata.fieldvalue" value="{literal}{{fld}}{/literal}">{literal}{{fld}}{/literal}</option>
						</select>
					</span>
				</td>
			</tr>
			<tr> 
				<td>
					<div id="buttons" align="center">
						<md-button class="md-ink-ripple md-primary add-action" ng-disabled="enable_action" ng-show="widget.paramname!='upgradeparams'" 								   ng-click="findWSResults(widget.paramname,fldValuesParam[widget.paramAction.actionid],widget.paramAction.actionname,widget.paramAction.actionoutput,checkboxes.items,checkboxesparam[action.paramname].items,checkboxesparam[widget.paramname].items,action.paramname);">
						{literal}{{widget.paramAction.actionlabel}}{/literal}
					</md-button>
					<md-button class="md-ink-ripple md-primary add-action" ng-disabled="enable_action" ng-show="widget.paramname=='upgradeparams'"
							   ng-click="findWSResults(widget.paramname,fldValuesParam[widget.paramAction.actionid],widget.paramAction.actionname,widget.paramAction.actionoutput,checkboxes.items,checkboxesparam['upgradableproducts'].items,checkboxesparam[widget.paramname].items,'upgradableproducts');">
					{literal}{{widget.paramAction.actionlabel}}{/literal}
				</md-button>

			</div> 
		</td>
	</tr>
</table>
</accordion-group>
</accordion-group>
</accordion>
</accordion-group>
</accordion-group>
</accordion>

