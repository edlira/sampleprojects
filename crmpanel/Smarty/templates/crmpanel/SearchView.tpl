<div id="profileDetails" >
	<div id="profileHeader">
		<md-content class="md-padding">
			<md-tabs md-dynamic-height md-border-bottom>
				<md-tab ng-repeat="tabs in structure" label="{literal}{{tabs.info.tab_label}}{/literal}" >
					<md-content class="md-padding">
							<span layout="row" layout-align="center center">
								<h5>{literal}{{tabs.info.tab_label}}{/literal}</h5>
							</span>
						<accordion close-others="oneAtATime">
							<accordion-group ng-repeat="blocks in tabs.blocks" heading="{literal}{{blocks.blocklabel}}{/literal}" id="{literal}{{blocks.blockid}}{/literal}">
								<div>
									<table border=0 cellspacing=0 cellpadding=0 width="100%" style="width:100%;">
										<tr ng-repeat="fields in blocks.fields" ng-init="sec_col=$index+1 ">
											<td style="text-align: left;" width="70">
											{literal}{{fields.fieldlabel}}{/literal}
										</td>
										<td ng-switch on="fields.fieldtype" style="text-align: right;" width="100"> 
									<md-input-container md-no-float>
										<input type="text" ng-switch-when="Textbox" ng-model="fldValues[blocks.blockid][fields.fieldname]" style="height:33px;" placeholder="Type here ..." value="s.zeneli@studiosynthesis.biz">
									</md-input-container>
									<input type="checkbox" ng-switch-when="Checkbox" ng-model="fldValues[blocks.blockid][fields.fieldname]"/>
									<input type="date" ng-switch-when="Date" style="width:70px;" ng-model="fldValues[blocks.blockid][fields.fieldname]"/>
									<md-input-container md-no-float>
										<input type="text" ng-switch-when="UItype10" />
									</md-input-container>
									<md-select name="{literal}{{fields.fieldname}}{/literal}"  ng-switch-when="Filter" ng-model="fldValues[blocks.blockid][fields.fieldname]" placeholder="Select a filter">
										<md-option ng-repeat="(key, value) in fields.fieldvalue" value="{literal}{{key}}{/literal}">{literal}{{value}}{/literal}</md-option>
									</md-select>
									<md-select name="{literal}{{fields.fieldname}}{/literal}"  ng-switch-when="Picklist" ng-model="fldValues[blocks.blockid][fields.fieldname]" placeholder="Select a valuer">
										<md-option ng-repeat="(key, value) in fields.fieldvalue" value="{literal}{{key}}{/literal}">{literal}{{value}}{/literal}</md-option>
									</md-select><!--ng-repeat="(key, value) in fields.fieldvalue"-->
									</td>
									</tr>
								</table>
								<center>
									{*<md-button class="md-raised md-primary" ng-disabled="enable_cerca" ng-click="findResults(blocks.blockid,fldValues[blocks.blockid])">{literal}{{cerca_name}}{/literal}</md-button>*}
									<md-button class="md-raised md-primary" ng-disabled="enable_cerca" ng-click="generalSearch(blocks.blockid,fldValues[blocks.blockid],blocks.actiondata.actionname,'html')">{literal}{{blocks.actiondata.actionlabel}}{/literal}</md-button>
								</center>
							</div>
						</accordion-group>
					</accordion>

				</md-content>
			</md-tab>
		</md-tabs>
	</md-content>
</div>
</div>
