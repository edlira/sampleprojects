<table width=96% align=center border="0" ng-app="demoApp" style="padding:10px;">
	<tr><td style="height:2px"><br/><br/></td></tr>
	<tr>
		<td style="padding-left:20px;padding-right:50px" class="moduleName" nowrap colspan="2">
			<h3 class="title">{$MODULE}</h3></td>
	</tr>
	<tr>
		<td ng-app="demoApp" ng-controller="{$MODULE}" ng-cloak class="showPanelBg" valign="top" style="padding:10px;" width=96%>
			<div id="pageWrapper">
				<div class="pageContent profile-view">
					<div id="profileContent">
						<div>
							{include file="modules/$MODULE/SearchView.tpl"}
						</div>
						<div>
							{include file="modules/$MODULE/ResultView.tpl"}
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>

<script>
	{literal}
		angular.module('demoApp', ['ngTable', 'ui.bootstrap', 'multi-select', 'ngMaterial'])
				.controller({/literal}'{$MODULE}'{literal}, function ($scope, $http, $modal, $filter, ngTableParams) {

					$scope.name_dashboard ={/literal} '{$MODULE}'{literal};
					$scope.structure ={/literal}{$TABS_JSON}{literal};
					$scope.show_results = false;
					$scope.fldValues = {};
					$scope.fldValuesParam = {};
					$scope.enable_action = false;
					$scope.filter_val = '';
					$scope.cerca_name = 'Cerca';
					$scope.enable_cerca = false;
					$scope.tot_length = 0;
					$scope.returnedResultsParam = {};
					$scope.checkboxesparam = {};
					$scope.listRecord = {};
					$scope.totalLength = {};
					$scope.tot = {};
					$scope.blockParameters = {};
					$scope.put_date = function (actionid, fld) {

						$scope.fldValuesParam[actionid][fld] = $filter('date')($scope.fldValuesParam[actionid][fld], 'yyyy-MM-dd');

					};

					$scope.handleAccordion = function (autoType, idname, values, actionid, outputtype, selectedids, parentids, subids, idparentname) {
						console.log(autoType);
						if (autoType == "1") {
							$scope.findWSResults(idname, values, actionid, outputtype, selectedids, parentids, subids, idparentname);
						}
					}
					$scope.editButtonName = 'Edit';
					$scope.put_date = function (actionid, fld) {
						$scope.fldValuesParam[actionid][fld] = $filter('date')($scope.fldValuesParam[actionid][fld], 'yyyy-MM-dd');
					};

					$scope.saveRecordChanges = function (rec, reckey) {
						$scope.editmode = !$scope.editmode;
						if (!$scope.editmode) {
							$scope.editButtonName = 'Edit';
							var editActionID = '';
							var editOutputType = '';
							var resp = {};
							$scope.enable_action = true;
							var senddata = {
								'object': JSON.stringify(rec),
							};
							angular.forEach($scope.blockParameters, function (item) {

								if (item.paramAction.actionlabel === "Save Changes") {
									editActionID = item.paramAction.actionname;
									editOutputType = item.paramAction.actionoutput;
								}
							});
							resp = runJSONAction(editActionID, JSON.stringify(senddata), editOutputType);
							var respobject = JSON.parse(resp);
							var focusrecord = JSON.parse(respobject.updatedRecord);
							for (var fld in $scope.returnedResults[reckey]) {
								$scope.returnedResults[reckey][fld] = focusrecord[fld];
							}
							$scope['tableParams'].reload();
							console.log(respobject);
							if (respobject.requestStatus >= 400) {
								var err = JSON.parse(respobject.responseErrorStatus);
								var alertStatus = '';
								angular.forEach(err, function (value, key) {
									alertStatus += key + ' : ' + JSON.stringify(value) + '\n';
								});
								//alert(respobject.responseErrorStatus);
								alert(alertStatus);
							}
							else {
								alert("Data Saved Successfully");
							}


							$scope.enable_action = false;
						}
						else {
							$scope.editButtonName = 'Save';
						}

					};

					$scope.generalSearch = function (blockid, values, actionid, outputtype) {
						var tablename = 'tableParams';
						$scope.searchText = '';
						$scope.blc = blockid;
						$scope.vls = values;
						$scope.actionid = actionid;
						//$scope.blockParameters=[];
						//$scope.returnedResultsParam=[];
						if ($scope[tablename]) {
							$scope.show_results = false;
							$scope.cerca_name = 'Ricerca';
							$scope.enable_cerca = true;
							$scope[tablename].reload();
						} else {
							$scope[tablename] = new ngTableParams({
								page: 1, // show first page
								count: 5 // count per page
							}, {
								counts: [], // hide page counts control
								getData: function ($defer, params) {
									var allParams = JSON.stringify($scope.vls);

									var actionResult = runJSONAction(actionid, allParams, outputtype);
									actionResult = JSON.parse(actionResult);
									if (actionResult != null) {
										$scope.returnedResults = actionResult.records;
									} else {
										$scope.returnedResults = [];
									}
									$scope.enable_cerca = false;
									$scope.tot_length = $scope.returnedResults.length;
									$scope.tot = actionResult.total;
									$scope.columns = actionResult.columns;
									// var keyNamefld="fieldlabels"+idname;
									$scope.fieldlabels = actionResult.fieldlabels;
									$http.post('index.php?module=' + $scope.name_dashboard + '&action=' + $scope.name_dashboard + 'Ajax&file=retrieveParameters&ds_blockid=' + $scope.blc + '&request_values=' + allParams)
											.success(function (data, status) {
												$scope.blockParameters = data.blockParameters;
											});
									var orderedData = params.filter ?
											$filter('filter')($scope.returnedResults, params.filter()) : $scope.returnedResults;

									params.total($scope.tot_length); // set total for recalc pagination
									$defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));



								}
							});
						}
						$scope.show_results = true;
						$scope.cerca_name = 'Cerca';
						$scope.enable_cerca = false;
						$scope.getTitle = function (val) {
							return $scope.fieldlabels[val];
						};
						$scope.checkboxes = {
							'checked': false,
							items: {}
						};
						$scope.allselected = false;
						$scope.selectAll = function () {
							angular.forEach($scope.returnedResults, function (value, key) {
								if ($scope.allselected) {
									$scope.checkboxes.items[value.id] = false;
								} else {
									$scope.checkboxes.items[value.id] = true;
								}
							});
							$scope.allselected = !$scope.allselected;
						};
						$scope.$watch('checkboxes.items', function (values) {
							if (!$scope.users) {
								return;
							}
							var checked = 0,
									unchecked = 0,
									total = $scope.users.length;
							angular.forEach($scope.users, function (item) {
								checked += ($scope.checkboxes.items[item.id]) || 0;
								unchecked += (!$scope.checkboxes.items[item.id]) || 0;
							});
							if ((unchecked == 0) || (checked == 0)) {
								$scope.checkboxes.checked = (checked == total);
							}
							angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
						}, true);
					};
					$scope.findWSResults = function (idname, values, actionid, outputtype, selectedids, parentids, subids, idparentname) {
						var tablename = 'tableParams' + idname;
						$scope.fieldlabels[idname] = {};
						$scope.columns[idname] = {};
						//$scope.returnedResultsParam[idname]={};
						$scope.checkboxesparam[idname] = {};
						$scope.listRecord[idname] = 0;
						$scope.tot[idname] = 0;
						//$scope.blockParameters={};
						$scope.searchText = '';
						$scope.vls = values;
						$scope.actionid = actionid;
						$scope.selectedids = selectedids;
						$scope.parentids = parentids;
						$scope.subids = subids;
						if ($scope[tablename]) {
							$scope.show_results = false;
							$scope.cerca_name = 'Ricerca';
							$scope.enable_cerca = true;
							$scope[tablename].reload();
						} else {
							$scope[tablename] = new ngTableParams({
								page: 1, // show first page
								count: 5 // count per page
							}, {
								counts: [], // hide page counts control
								getData: function ($defer, params) {
									var arr_sel = '';
									var count = 0;
									angular.forEach($scope.selectedids, function (value, key) {
										if (value == true) {
											arr_sel += (count === 0 ? key : ',' + key);
											count++;
										}
									});
									var parent_arr_sel = '';
									var parent_count = 0;
									angular.forEach($scope.parentids, function (value, key) {
										if (value == true) {
											var foundVal = key;
											if ($scope.returnedResultsParam[idparentname][key] != undefined) {
												if ($scope.returnedResultsParam[idparentname][key]['objectinfo'] != undefined) {
													foundVal = JSON.stringify($scope.returnedResultsParam[idparentname][key]['objectinfo']);
												}
											}
											parent_arr_sel += (parent_count === 0 ? foundVal : ',' + foundVal);
											parent_count++;
										}
									});
									var element_arr_sel = '';
									var element_count = 0;
									angular.forEach($scope.subids, function (value, key) {
										if (value == true) {
											var foundVal = key;
											if ($scope.returnedResultsParam[idname][key]['objectinfo'] != undefined) {
												foundVal = JSON.stringify($scope.returnedResultsParam[idname][key]['objectinfo']);
											}
											element_arr_sel += (element_count === 0 ? foundVal : ',' + foundVal);
											element_count++;
										}
									});
									var senddata = {
										'mail': arr_sel,
										'id': parent_arr_sel,
										'elementid': element_arr_sel
									};
									if ($scope.vls != undefined) {
										$scope.vls = angular.extend($scope.vls, senddata);
									} else {
										$scope.vls = senddata;
									}
									var allParams = JSON.stringify($scope.vls);
									var actionResult = runJSONAction(actionid, allParams, outputtype);
									actionResult = JSON.parse(actionResult);
									if (actionResult != null) {
										$scope.returnedResultsParam[idname] = actionResult.records;
									} else {
										$scope.returnedResultsParam[idname] = [];
									}
									$scope.enable_cerca = false;
									$scope.totalLength[idname] = $scope.returnedResultsParam[idname].length;
									$scope.tot[idname] = actionResult.total;
									$scope.columns[idname] = actionResult.columns;
									$scope.fieldlabels[idname] = actionResult.fieldlabels;
									var orderedData = params.filter ?
											$filter('filter')($scope.returnedResultsParam[idname], params.filter()) : $scope.returnedResultsParam[idname];

									params.total($scope.totalLength[idname]); // set total for recalc pagination
									$defer.resolve($scope.listRecord[idname] = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));



								}
							});
						}
						$scope.show_results = true;
						//$scope.cerca_name = 'Cerca';
						//$scope.enable_cerca = false;
						/* $scope.getTitle = function(val) {
						 return $scope.fieldlabels[val];
						 };*/

						$scope.allselected[idname] = false;
						$scope.selectAll[idname] = function () {
							angular.forEach($scope.returnedResultsParam[idname], function (value, key) {
								if ($scope.allselected[idname]) {
									$scope.checkboxesparam[idname].items[value.id] = false;
								} else {
									$scope.checkboxesparam[idname].items[value.id] = true;
								}
							});
							$scope.allselected[idname] = !$scope.allselected[idname];
						};
						$scope.$watch('checkboxesparam[' + idname + '].items', function (values) {
							if (!$scope.listRecord[idname]) {
								return;
							}
							var checked = 0,
									unchecked = 0,
									total = $scope.listRecord[idname].length;
							angular.forEach($scope.listRecord[idname], function (item) {
								checked += ($scope.checkboxesparam[idname].items[item.id]) || 0;
								unchecked += (!$scope.checkboxesparam[idname].items[item.id]) || 0;
							});
							if ((unchecked == 0) || (checked == 0)) {
								$scope.checkboxesparam[idname].checked = (checked == total);
							}
							angular.element(document.getElementById("select_all" + idname)).prop("indeterminate", (checked != 0 && unchecked != 0));
						}, true);
					};
					$scope.findResults = function (blockid, values) {
						$scope.searchText = '';
						$scope.blc = blockid;
						$scope.vls = values;
						if ($scope.tableParams) {
							$scope.show_results = false;
							$scope.cerca_name = 'Ricerca';
							$scope.enable_cerca = true;
							$scope.tableParams.reload();
						} else {
							$scope.tableParams = new ngTableParams({
								page: 1, // show first page
								count: 5 // count per page
							}, {
								counts: [], // hide page counts control
								getData: function ($defer, params) {
									var allParams = JSON.stringify($scope.vls);
									$http.post('index.php?module=' + $scope.name_dashboard + '&action=' + $scope.name_dashboard + 'Ajax&file=searchBlockClient&ds_blockid=' + $scope.blc + '&request_values=' + allParams)
											.success(function (data, status) {
												if (data.records != null) {
													$scope.returnedResults = data.records;
												} else {
													$scope.returnedResults = [];
												}
												$scope.enable_cerca = false;
												$scope.tot_length = $scope.returnedResults.length;
												$scope.tot = data.total;
												$scope.columns = data.columns;
												$scope.fieldlabels = data.fieldlabels;

												$scope.blockParameters = data.blockParameters;
												var orderedData = params.filter ?
														$filter('filter')($scope.returnedResults, params.filter()) : $scope.returnedResults;

												params.total($scope.tot_length); // set total for recalc pagination
												$defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

											});
								}
							});
						}
						$scope.show_results = true;
						$scope.cerca_name = 'Cerca';

						$scope.getTitle = function (val) {
							return $scope.fieldlabels[val];
						};
						$scope.checkboxes = {
							'checked': false,
							items: {}
						};
						$scope.allselected = false;
						$scope.selectAll = function () {
							angular.forEach($scope.returnedResults, function (value, key) {
								if ($scope.allselected) {
									$scope.checkboxes.items[value.recordid] = false;
								} else {
									$scope.checkboxes.items[value.recordid] = true;
								}
							});
							$scope.allselected = !$scope.allselected;
						};
						$scope.$watch('checkboxes.items', function (values) {
							if (!$scope.users) {
								return;
							}
							var checked = 0,
									unchecked = 0,
									total = $scope.users.length;
							angular.forEach($scope.users, function (item) {
								checked += ($scope.checkboxes.items[item.recordid]) || 0;
								unchecked += (!$scope.checkboxes.items[item.recordid]) || 0;
							});
							if ((unchecked == 0) || (checked == 0)) {
								$scope.checkboxes.checked = (checked == total);
							}
							angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
						}, true);
					};
					$scope.runAction = function (actionid, values, selectedids, outputType) {
						var arr_sel = '';
						$scope.enable_action = true;
						var count = 0;
						angular.forEach(selectedids, function (value, key) {
							if (value == true) {
								arr_sel += (count === 0 ? key : ',' + key);
								count++;
							}
						});
						var senddata = {
							'recordid': arr_sel,
						};
						if (values != undefined) {
							values = angular.extend(values, senddata);
						} else {
							values = senddata;
						}
						runJSONAction(actionid, JSON.stringify(values), outputType);
						$scope.enable_action = false;
					};

				});
	{/literal}
</script>
