<?php

function registerUnitInfo($ID) {
	require_once("modules/Emails/mail.php");
	require_once("modules/hpsmlog/hpsmlog.php");
	global $adb;
	date_default_timezone_set('Europe/Rome');
	$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
	$client = new SOAPClient($wsdl_url, array('keep_alive' => false));
	//It needs to be UTF-8
	$client->soap_defencoding = 'UTF-8';
	$client->http_encoding = '';
	$client->decode_utf8 = false;

	$password = "";
	$userID = "";
	$queryString = "Select *     
            from vtiger_project
            join vtiger_account on accountid=linktoaccountscontacts
            join vtiger_accountbillads on accountid=accountaddressid
            JOIN vtiger_cities ON vtiger_accountbillads.bill_city = citiesid
            where projectid=?";
	$query = $adb->pquery($queryString, array($ID));
	$result = $adb->fetch_array($query);
	//addressInfo
	$address1 = $result["bill_street"];
	// $address1="Via Rivale 16R";
	$address2 = "";
	$address3 = "";
	$city = $result["cityname"];
	//$city="GENOVA";
	//$zipcode=16129;
	$countryISO2 = "IT";
	$zipcode = $result["bill_code"];

	$addressInfo = array("address1" => $address1, "address2" => $address2, "address3" => $address3, "city" => $city,
		"countryISO2" => $countryISO2, "zipcode" => $zipcode);
	//addressInfo--END
	//info
	$additionalInfo = $result["projectname"];
	$communicationLanguageISO2 = "IT";
	$companyName = $result["accountname"];
	//$companyName="VIDEOTEST SERVICE Societ 01";
	$firstName = $result["nome"];
	$lastName = $result["cognome"];
	$title = "Mr";
	$vatNumber = $result["codicefiscale"];
	$email = $result["email1"];
	$fax = $result["fax"];
	$fixedPhone = $result["phone"];
	$mobilePhone = $result["cellulare"];
	$outOfOfficePhone = $result["cellulare"];
	if (empty($result["noadverts"]))
		$notSendAdverts = false;
	else
		$notSendAdverts = true;
	$noSurvey = '';
	//$notSendAdverts=false;

	$info = array("activity" => $additionalInfo, "addressInfo" => $addressInfo, "communicationLanguageISO2" => $communicationLanguageISO2,
		"companyName" => $companyName, "firstName" => $firstName, "lastName" => $lastName,
		"title" => $title, "vatNumber" => $vatNumber, "email" => $email, "fax" => $fax, "fixedPhone" => $fixedPhone, "mobilePhone" => $mobilePhone,
		"outOfOfficePhone" => $outOfOfficePhone, "noSurvey" => $noSurvey, "notSendAdverts" => $notSendAdverts);

	//info--END
	//unitInfo
	$bootLanguageISO2 = "IT";
	$modelName = "";
	$modelCode = $result["model_number"];
	$serialNumber = $result["serial_number"];
	$purDate = $result["customer_purchase_data"] . " 00:00:00";
	$datetime = new DateTime($purDate);

	$purchaseDate = str_replace("+", ".", $datetime->format(DateTime::ISO8601)) . "Z";
	//$purchaseDate=$result["customer_purchase_data"];
	$premiumServiceReference = "";
	$unitInfo = array("bootLanguageISO2" => $bootLanguageISO2, "modelName" => $modelName,
		"modelCode" => $modelCode, "serialNumber" => $serialNumber, "purchaseDate" => $purchaseDate,
		"premiumServiceReference" => $premiumServiceReference);

	//unitInfo--END
	$error = 0;
	$params = array("userId" => $userID, "password" => $password, "info" => $info, "unitInfo" => $unitInfo);
	try {
		$resultResponse = $client->registerUnit(array("param" => $params));
	} catch (SoapFault $fault) {
		// Check for errors
		$error = 1;
	}
	$successful = $resultResponse->return->successful;
	if ($error == 1) {
		$err = $fault->faultcode . " " . $fault->faultstring;
		$date = date('Y-m-d');
		$logFile = "logs/sonyintegrationRegisterUnit$date.log";


		$datetime = date('l jS \of F Y h:i:s A');
		$LogContent = "\n Sony Register Unit with ID ::: $ID $datetime \n";
		$LogContent.=$err;

		$to = "e.dushku@studiosynthesis.biz";
		$from = "Teknema-Sony Integration";
		$from_email = "support@evolutivo.it";
		$subject = "Sony Register Unit with ID ::: $ID on $datetime ERROR!";
		$contents = $subject . " \n For further information please look at the log file $logFile";
		send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

		error_log($LogContent, 3, $logFile);
		$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' Register Unit: $err') WHERE crmid=$ID");
	} elseif ($successful != 1) {
		$focus = new hpsmlog();
		$focus->column_fields['assigned_user_id'] = 1;
		$focus->column_fields['chiamata'] = $resultResponse->return->errorCode . " " . $resultResponse->return->errorMessage;
		$focus->column_fields['incidentid'] = 0;
		$focus->column_fields['project'] = $ID;
		$focus->column_fields['citta'] = '';
		$focus->column_fields['tiposcript'] = "RegisterUnit";
		$focus->saveentity("hpsmlog");

		$err = $resultResponse->return->errorCode . " " . $resultResponse->return->errorMessage;
		$date = date('Y-m-d');
		$logFile = "logs/sonyintegrationRegisterUnit$date.log";


		$datetime = date('l jS \of F Y h:i:s A');
		$LogContent = "\n Sony Register Unit with ID ::: $ID $datetime \n";
		$LogContent.=$err;

		$to = "e.dushku@studiosynthesis.biz";
		$from = "Teknema-Sony Integration";
		$from_email = "support@evolutivo.it";
		$subject = "Sony Register Unit with ID ::: $orig_id on $datetime ERROR!";
		$contents = $subject . " \n For further information please look at the log file $logFile";
		send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

		error_log($LogContent, 3, $logFile);
		$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' Register Unit:$err') WHERE crmid=$ID");
	}
}

function getUnitInfo($ID) {
	require_once("modules/Emails/mail.php");
	require_once("modules/hpsmlog/hpsmlog.php");
	date_default_timezone_set('Europe/Rome');
	global $adb;

	$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
	$client = new SOAPClient($wsdl_url, array('keep_alive' => false));

	$client->soap_defencoding = 'UTF-8';
	$client->http_encoding = '';
	$client->decode_utf8 = false;

	$queryUnitInfo = $adb->pquery("Select model_number,serial_number,customer_purchase_data from vtiger_project where projectid=?", array($ID));

	$password = "";
	$userID = "";
	$modelName = "";
	$modelCode = $adb->query_result($queryUnitInfo, 0, 0);
	$serialNumber = $adb->query_result($queryUnitInfo, 0, 1);
	$purDate = $adb->query_result($queryUnitInfo, 0, 2);
	$datetime = new DateTime($purDate);

	$purchaseDate = str_replace("+", ".", $datetime->format(DateTime::ISO8601)) . "Z";
	$purchaseCountry;
	$error = 0;
	$params = array("userId" => $userID, "password" => $password, "modelName" => $modelName, "modelCode" => $modelCode,
		"serialNumber" => $serialNumber, "purchaseDate" => $purchaseDate, "purchaseCountry" => $purchaseCountry);
	try {
		$result = $client->getUnitInfo(array("param" => $params));
	} catch (SoapFault $fault) {
		// Check for errors
		$error = 1;
	}
	$successful = $result->return->successful;
	$statusProduct = $result->return->serialNumberStatus;
	if ($error == 1) {
		$err = $fault->faultcode . " " . $fault->faultstring;
		$date = date('Y-m-d');
		$logFile = "logs/sonyintegrationgetUnitInfo$date.log";


		$datetime = date('l jS \of F Y h:i:s A');
		$LogContent = "\n Sony getUnitInfo with ID ::: $ID $datetime \n";
		$LogContent.=$err;

		$to = "e.dushku@studiosynthesis.biz";
		$from = "Teknema-Sony Integration";
		$from_email = "support@evolutivo.it";
		$subject = "Sony getUnitInfo with ID ::: $ID on $datetime ERROR!";
		$contents = $subject . " \n For further information please look at the log file $logFile";
		send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

		error_log($LogContent, 3, $logFile);
		$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getUnitInfo:$err') WHERE crmid=$ID");
		return 0;
	} elseif (!$successful || $statusProduct === "unregistered") {
		switch ($result->return->errorCode) {

			case "1345":
			case "1211":
				registerUnitInfo($ID);
				break;
		}
		if ($statusProduct === "unregistered")
			registerUnitInfo($ID);
		$focus = new hpsmlog();
		$focus->column_fields['assigned_user_id'] = 1;
		$focus->column_fields['chiamata'] = $result->return->errorCode . " " . $result->return->errorMessage;
		$focus->column_fields['incidentid'] = 0;
		$focus->column_fields['project'] = $ID;
		$focus->column_fields['citta'] = '';
		$focus->column_fields['tiposcript'] = "getUnitInfo";
		$focus->saveentity("hpsmlog");

		$err = $result->return->errorCode . " " . $result->return->errorMessage;
		$date = date('Y-m-d');
		$logFile = "logs/sonyintegrationgetUnitInfo$date.log";


		$datetime = date('l jS \of F Y h:i:s A');
		$LogContent = "\n Sony getUnitInfo with ID ::: $ID $datetime \n";
		$LogContent.=$err;

		$to = "e.dushku@studiosynthesis.biz";
		$from = "Teknema-Sony Integration";
		$from_email = "support@evolutivo.it";
		$subject = "Sony getUnitInfo with ID ::: $ID on $datetime ERROR!";
		$contents = $subject . " \n $err. For further information please look at the log file $logFile";
		send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

		error_log($LogContent, 3, $logFile);
		$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getUnitInfo: $err') WHERE crmid=$ID");
		return 1;
	}
	else {

//            $focusProject=  CRMEntity::getInstance($module);
//            $focusProject->retrieve_entity_info($ID,$module);
//            $focusProject->mode='edit';
		$jsonResponse = json_encode($result->return);
		$resp = json_decode($jsonResponse, true);
		//$warrantyEndDate=date("d-m-Y",substr($result->return->eventInfo->warrantyEndDate,0,10));
		//$iswarranty=$warrantyEndDate>$datenow?true:false;
		// $purchaseDate=date("d-m-Y",strtotime(substr($resp['purchaseDate'],0,10)));
		$purchaseDate = substr($resp['purchaseDate'], 0, 10);
		$productDescription = $resp['productDescription'];
		$productManufacturer = $resp['productManufacturer'];
		$productOnSalesDate = substr($resp['productOnSalesDate'], 0, 10);
		$productOrigin = $resp['productOrigin'];
		$productOsType = $resp['productOsType'];
		$productSalesType = $resp['productSalesType'];
		$purchaseDateAccepted = substr($resp['purchaseDateAccepted'], 0, 10);
		$warrantyEndDate = substr($resp['warrantyEndDate'], 0, 10);
		//$focus->column_fields["iwyes"]=$iswarranty;
		//$focusProject->column_fields["customer_purchase_data"]=$purchaseDate;
		$adb->pquery("Update vtiger_project set product_description=?,
               product_manufacturer=?,productonsalesdate=?,productorigin=?,productostype=?,
               productsalestype=?,purchasedateaccepted=?,warrantyenddate=?,purchaseDateAccepted=?
               where projectid=?", array($productDescription, $productManufacturer,
			$productOnSalesDate, $productOrigin, $productOsType,
			$productSalesType, $purchaseDateAccepted, $warrantyEndDate, $purchaseDateAccepted, $ID));
		//$focusProject->saveentity("Project");
		$err = $result->return->errorCode . " " . $result->return->errorMessage;

		$focus = new hpsmlog();
		$focus->column_fields['assigned_user_id'] = 1;
		$focus->column_fields['chiamata'] = json_encode($result->return);
		$focus->column_fields['incidentid'] = 0;
		$focus->column_fields['project'] = $ID;
		$focus->column_fields['citta'] = '';
		$focus->column_fields['tiposcript'] = "getUnitInfo";
		$focus->saveentity("hpsmlog");

		$err = $result->return->errorCode . " " . $result->return->errorMessage;
		$date = date('Y-m-d');
		$logFile = "logs/sonyintegrationgetUnitInfo$date.log";


		$datetime = date('l jS \of F Y h:i:s A');
		$LogContent = "\n Sony getUnitInfo with ID ::: $ID $datetime \n";
		$LogContent.=$err;

		$to = "e.dushku@studiosynthesis.biz";
		$from = "Teknema-Sony Integration";
		$from_email = "support@evolutivo.it";
		$subject = "Sony getUnitInfo with ID ::: $ID on $datetime OK!";
		$contents = $subject . " \n $err. For further information please look at the log file $logFile $purchaseDate";
		send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

		error_log($LogContent, 3, $logFile);
		if ($successful)
			$err = "success";
		$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getUnitInfo: $err') WHERE crmid=$ID");
		return 1;
	}
}

function register_serviceevents($entity) {
	global $adb, $log;
	include_once("include/database/PearDatabase.php");
	include_once("include/utils/utils.php");
	include_once("include/utils/CommonUtils.php");
	require_once("modules/Emails/mail.php");
	require_once("modules/hpsmlog/hpsmlog.php");
	require_once("modules/Project/Project.php");
	require_once("modules/Users/Users.php");
	global $adb, $log;
	global $current_user;
	if (empty($current_user)) {
		$current_user = new Users();
		$current_user->retrieveCurrentUserInfoFromFile(1);
	}
	$socket_context = stream_context_create(
			array('http' => array('protocol_version' => 1.0))
	);
	$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
	$client = new SOAPClient($wsdl_url, array('keep_alive' => false, 'trace' => true));
	//It needs to be UTF-8
	$client->soap_defencoding = 'UTF-8';
	$client->http_encoding = '';
	$client->decode_utf8 = false;
	$orig_pot = $entity->getId();
	$orig_id = explode("x", $orig_pot);
	$projectid = $orig_id[1];

	$module = "Project";
	$focus = CRMEntity::getInstance($module);
	$focus->retrieve_entity_info($projectid, $module);
	$condition1 = ($focus->column_fields["brand"] == 'Sony' && (stripos($focus->column_fields["projectstatus"], "Hold") === false) &&
			empty($focus->column_fields["project_id"]) && (
			stripos($focus->column_fields["projectname"], "CIN") !== false ||
			stripos($focus->column_fields["projectname"], "PUR") !== false ||
			stripos($focus->column_fields["projectname"], "DOA") !== false ||
			stripos($focus->column_fields["projectname"], "CID") !== false ) &&
			stripos($focus->column_fields["macrostatus"], "Valid Call") === false &&
			stripos($focus->column_fields["macrostatus"], "Canceled") === false &&
			stripos($focus->column_fields["macrostatus"], "--Nessuno--") === false &&
			$focus->column_fields["macrostatus"] != "" &&
			stripos($focus->column_fields["macrostatus"], "Pending Customer/Vendor") === false ) ? true : false;

	$condition2 = ($focus->column_fields["brand"] == 'Sony' &&
			empty($focus->column_fields["project_id"]) &&
			stripos($focus->column_fields["projectname"], "HR") !== false) ? true : false;
	if ($condition1 || $condition2) {
		$accountid = $focus->column_fields['linktoaccountscontacts'];
		$unitInfoResult = getUnitInfo($projectid);
		if ($unitInfoResult == 1) {
			$module = "Project";
			$focus = CRMEntity::getInstance($module);
			$focus->retrieve_entity_info($projectid, $module);

			$catid = $focus->column_fields['linktobuyer'];
			$iswarranty = $focus->column_fields["iwyes"];
			if ($iswarranty == 1) {
				$catQuery = $adb->pquery("SELECT sitename FROM vtiger_account
			INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_account.accountid 
WHERE ce.deleted=0 AND accountid=?", array($catid));
				$catcode = $adb->query_result($catQuery, 0, 'sitename');
				$logisticsType = $focus->column_fields['logistictype'];
				if ($logisticsType == "Nessuno")
					$logisticsType = "";
				$isDoa = $focus->column_fields['doa'];
				$doaIsPreSales = $focus->column_fields['doaispresales'];

//$serviceTypequery=$adb->pquery("SELECT * FROM vtiger_servicetype st
//                                INNER JOIN vtiger_crmentity ce ON ce.crmid=st.servicetypeid
//                                LEFT JOIN vtiger_symptom symptom ON symptom.symptomid=st.symptom
//                                LEFT JOIN vtiger_defect defect ON defect.defectid=st.defect
//                                LEFT JOIN vtiger_condition cond ON cond.conditionid=st.cond
//                                LEFT JOIN vtiger_section section ON section.sectionid=st.section
//                                LEFT JOIN vtiger_repair repair ON repair.repairid=st.repair
//                                WHERE ce.deleted=0 AND st.project=?",array($projectid));
				$serviceTypequery = $adb->pquery("SELECT symptom.cd_symptom,cond.cd_condition FROM vtiger_project
                                LEFT JOIN vtiger_symptom symptom ON symptom.symptomid=linktosymptom
                                LEFT JOIN vtiger_condition cond ON cond.conditionid=linktocondition
                                WHERE projectid=?", array($projectid));
				$accountquery = $adb->pquery("SELECT *,
                            billcountry.isocode as billcountryiso,billcity.cityname as billcityname, 
                            shipcountry.isocode as shipcountryiso,shipcity.cityname as shipcityname,
                            referenzaaziendale
                            FROM vtiger_account
                            INNER JOIN vtiger_crmentity
                                    ON vtiger_account.accountid = vtiger_crmentity.crmid
                            LEFT JOIN vtiger_accountbillads
                                    ON vtiger_accountbillads.accountaddressid = vtiger_account.accountid
                            LEFT JOIN vtiger_country billcountry
                                    ON billcountry.countryid=vtiger_accountbillads.bill_country
                            LEFT JOIN vtiger_cities billcity
                                    ON billcity.citiesid=vtiger_accountbillads.bill_city
                            LEFT JOIN vtiger_accountshipads
                                    ON vtiger_accountshipads.accountaddressid = vtiger_account.accountid
                            LEFT JOIN vtiger_country shipcountry
                                    ON shipcountry.countryid=vtiger_accountshipads.ship_country
                            LEFT JOIN vtiger_cities shipcity
                                    ON shipcity.citiesid=vtiger_accountshipads.ship_city
                    WHERE accountid=?", array($accountid));


				$additionalInfo = $focus->column_fields['notearrivo'];
				$aepBookingReference = "";
//$doaIsPreSales=false;
//$eventStartDate=$focus->column_fields['startdate'];
//$datetime = new DateTime($focus->column_fields["startdate"]);
				$datetime = new DateTime('now');
				$datetime->modify('-2 hours');
				$eventStartDate = str_replace("+", ".", $datetime->format(DateTime::ISO8601)) . "Z";
//$eventStartDate=date("Y/m/dTH:i:s.000Z", $focus->column_fields["startdate"]);
//"3/12/2013 19:31:09";
//$eventStartDate=$tmparrdate->format("d/m/Y H:i:s");
				$temp = explode("-", $focus->column_fields["projectname"]);
				$temp1 = $temp[1];
//$logisticsType="";
//switch($temp1){
//  case "CIN":$logisticsType=0;break;
//  case "PUR":$logisticsType=2;break;
//}
				$mainAscReferenceId = $projectid . "";
				$serviceEventType = $focus->column_fields["tiposerviceevent"];

				//$temp[2];
				//$focus->column_fields["tipoaviso"];
//switch($serviceEventType){
//   case "IHREP": $logisticsType="";break;
//   case "AEPB2CFWD": $logisticsType="";break;
//   case "AEPB2BFWD": $logisticsType="";break;
//}
//$subAscId="IT00833CTK0";
				$subAscId = $catcode;
				$AscId = "VIT0017185161";
				//$adb->query_result($accountquery,0,'referenzaaziendale');
				//"IT00833CTK0";
				$subAscReferenceId = $focus->column_fields["linktobuyer"];
//$subAscReferenceId=$catcode;
				$info = array("mainAscReferenceId" => $mainAscReferenceId, "subAscId" => $subAscId, "subAscReferenceId" => $subAscReferenceId,
					"serviceEventType" => $serviceEventType, "logisticsType" => $logisticsType, "aepBookingReference" => $aepBookingReference,
					"additionalInfo" => $additionalInfo, "isDoa" => $isDoa, "doaIsPreSales" => $doaIsPreSales, "eventStartDate" => $eventStartDate, 'AscId' => $AscId);


				$address1 = substr($adb->query_result($accountquery, 0, 'bill_street'), 0, 30);
				$address2 = "";
				$address3 = "";
				$city = strtoupper($adb->query_result($accountquery, 0, 'billcityname'));
//$city="GENOVA";
				$countryISO2 = $adb->query_result($accountquery, 0, 'billcountryiso');
				$zipcode = $adb->query_result($accountquery, 0, 'bill_code');
//$zipcode=16129;
				$addressInfo = array("address1" => $address1, "address2" => $address2, "address3" => $address3, "city" => $city,
					"countryISO2" => $countryISO2, "zipcode" => $zipcode);

				$communicationLanguageISO2 = "IT";
				$companyName = $adb->query_result($accountquery, 0, 'accountname');
				$email = $adb->query_result($accountquery, 0, 'email1');
				$fax = $adb->query_result($accountquery, 0, 'fax');
				;
				$firstName = $adb->query_result($accountquery, 0, 'nome');
				$fixedPhone = $adb->query_result($accountquery, 0, 'phone');
				;
				$lastName = $adb->query_result($accountquery, 0, 'cognome');
				$mobilePhone = $adb->query_result($accountquery, 0, 'cellulare');
				;
				$noSurvey = true;

				$notSendAdverts = true;
				$outOfOfficePhone = "";
				$reference = "";
				$title = "Mr";
//$adb->query_result($accountquery,0,'gender');
				$customerInfo = array("reference" => $reference, "communicationLanguageISO2" => $communicationLanguageISO2,
					"companyName" => $companyName, "firstName" => $firstName, "lastName" => $lastName, "title" => $title,
					"email" => $email, "fax" => $fax, "fixedPhone" => $fixedPhone, "mobilePhone" => $mobilePhone, "outOfOfficePhone" => $outOfOfficePhone,
					"noSurvey" => $noSurvey, "notSendAdverts" => $notSendAdverts,
					"addressInfo" => $addressInfo);

				$deliveryquery = $adb->pquery("SELECT *,
                            billcountry.isocode as billcountryiso,billcity.cityname as billcityname,
                            referenzaaziendale
                            FROM vtiger_account
                            INNER JOIN vtiger_crmentity
                                    ON vtiger_account.accountid = vtiger_crmentity.crmid
                            LEFT JOIN vtiger_accountbillads
                                    ON vtiger_accountbillads.accountaddressid = vtiger_account.accountid
                            LEFT JOIN vtiger_country billcountry
                                    ON billcountry.countryid=vtiger_accountbillads.bill_country
                            LEFT JOIN vtiger_cities billcity
                                    ON billcity.citiesid=vtiger_accountbillads.bill_city
                    WHERE accountid=?", array($subAscReferenceId));
				$deliveryaddress1 = $adb->query_result($deliveryquery, 0, 'bill_street');
				$deliveryaddress2 = "";
				$deliveryaddress3 = "";
				$deliverycity = strtoupper($adb->query_result($deliveryquery, 0, 'billcityname'));
//$deliverycity="GENOVA";
				$deliverycountryISO2 = $adb->query_result($deliveryquery, 0, 'billcountryiso');
				;
				$deliveryzipcode = $adb->query_result($deliveryquery, 0, 'bill_code');
				;
//$deliveryzipcode=16129;
				$deliveryAddressInfo = array("address1" => $deliveryaddress1, "address2" => $deliveryaddress2, "address3" => $deliveryaddress3, "city" => $deliverycity,
					"countryISO2" => $deliverycountryISO2, "zipcode" => $deliveryzipcode);


				$deliveryInfocompanyName = "";
				$deliveryInfoemail = "";
				$deliveryInfofax = "";
				$deliveryInfofirstName = "";
				$deliveryInfofixedPhone = "";
				$deliveryInfolastName = "";
				$deliveryInfomobilePhone = "";
				$deliveryInfooutOfOfficePhone = "";
				$deliveryInforeference = "";

				$deliveryInfo = array("reference" => $reference, "companyName" => $companyName,
					"firstName" => $firstName, "lastName" => $lastName, "email" => $email,
					"fax" => $fax, "fixedPhone" => $fixedPhone,
					"mobilePhone" => $mobilePhone, "outOfOfficePhone" => $outOfOfficePhone,
					"deliveryAddressInfo" => $deliveryAddressInfo);


				$datetimeDealer = new DateTime($focus->column_fields["incoming_document_data"]);

				$arrivedAtDealer = str_replace("+", ".", $datetimeDealer->format(DateTime::ISO8601)) . "Z";

				$irisCode = $adb->query_result($adb->pquery("Select cd_condition from vtiger_condition where conditionid=?", array($focus->column_fields['linktocondition'])), 0, 0);

				//getIrisCode($focus->column_fields['cd_condition']);
				$hasPhysicalDamage = $focus->column_fields["difettiesterni"];
				$modelName = "";
				$modelCode = $focus->column_fields["model_number"];
				$serialNumber = $focus->column_fields["serial_number"];
				$datetimePurchase = new DateTime($focus->column_fields["customer_purchase_data"]);

				$purchaseDate = str_replace("+", ".", $datetimePurchase->format(DateTime::ISO8601)) . "Z";
				$premiumServiceReference = "";
				$qrSymptom = $adb->pquery("Select cd_symptom from vtiger_symptom where symptomid=?", array($focus->column_fields['linktosymptom']));
				$symptomIrisCode = $adb->query_result($qrSymptom, 0);


				$unitInfo = array("modelName" => $modelName, "modelCode" => $modelCode, "serialNumber" => $serialNumber, "symptomIrisCode" => $symptomIrisCode, "conditionIrisCode" => $irisCode,
					"purchaseDate" => $purchaseDate, "hasPhysicalDamage" => $hasPhysicalDamage, "premiumServiceReference" => $premiumServiceReference,
					"arrivedAtDealer" => $arrivedAtDealer,
					"customerInfo" => $customerInfo, "deliveryInfo" => $deliveryInfo);
				$password = "";
				$userID = "";
				$params = array("userId" => $userID, "password" => $password, "info" => $info, "unitInfo" => $unitInfo);

				$error = 0;
				try {
					$result = $client->registerServiceEvent(array("param" => $params));
//$logFile="logs/aaarequesttestnow$date.log";
//$resp=$client->__getLastRequest();
//error_log($resp,3,$logFile) ;
//exit;
				} catch (SoapFault $fault) {
					// Check for errors
					$error = 1;
				}
				$successful = $result->return->successful;
				$final_result = "";
				if ($error == 1) {
					$err = $fault->faultcode . " " . $fault->faultstring;
					$date = date('Y-m-d');
					$logFile = "logs/sonyintegrationRegisterServiceEvent$date.log";


					$datetime = date('l jS \of F Y h:i:s A');
					$LogContent = "\n Sony Register SE with ID ::: $projectid $datetime \n";
					$LogContent.=$err;

					$to = "e.dushku@studiosynthesis.biz";
					$from = "Teknema-Sony Integration";
					$from_email = "support@evolutivo.it";
					$subject = "Sony Register SE with ID ::: $projectid on $datetime ERROR!";
					$contents = $subject . " \n For further information please look at the log file $logFile";
					send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

					error_log($LogContent, 3, $logFile);
					$adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$err' WHERE projectid=?", array($projectid));
					//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' Register ServiceEvent: $err') WHERE crmid=$projectid");
					$final_result = $err;
				} elseif (!$successful) {

//    switch($result->return->errorCode){
//        
//        case "1121": 
					$focushpsmlog = new hpsmlog();
					$focushpsmlog->column_fields['assigned_user_id'] = 1;
					$focushpsmlog->column_fields['chiamata'] = $result->return->errorCode . " " . $result->return->errorMessage;
					$focushpsmlog->column_fields['incidentid'] = 0;
					$focushpsmlog->column_fields['project'] = $projectid;
					$focushpsmlog->column_fields['citta'] = '';
					$focushpsmlog->column_fields['tiposcript'] = "RegisterServiceEvent";
					$focushpsmlog->saveentity("hpsmlog");

					$err = $result->return->errorCode . " " . $result->return->errorMessage;
					$date = date('Y-m-d');
					$logFile = "logs/sonyintegrationRegisterServiceEvent$date.log";


					$datetime = date('l jS \of F Y h:i:s A');
					$LogContent = "\n Sony Register SE with ID ::: $projectid $datetime \n";
					$LogContent.=$err;

					$to = "e.dushku@studiosynthesis.biz";
					$from = "Teknema-Sony Integration";
					$from_email = "support@evolutivo.it";
					$subject = "Sony Register SE with ID ::: $projectid on $datetime ERROR!";
					$contents = $subject . " \n For further information please look at the log file $logFile";
					send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
					$error_msg = $result->return->errorCode . " " . $result->return->errorMessage;
					$adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$error_msg' WHERE projectid=?", array($projectid));
					//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' Register ServiceEvent: $err') WHERE crmid=$projectid");

					error_log($LogContent, 3, $logFile);
					$final_result = $err;
//            break; 
//    }
				} else {
					global $log;

					$jsonResponse = json_encode($result->return);
					$resp = json_decode($jsonResponse, true);
					$incidentid = $resp['eventInfo']['caseId'];
					$unitWarrantyStatus = $resp['eventInfo']['unitWarrantyStatus'];
					if ($unitWarrantyStatus === "OOW" && $iswarranty) {
						$msg_desc = 'Prodotto Fuori Garanzia. Caricate la POP per verificare';
						$adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$msg_desc' WHERE projectid=?", array($projectid));
					}
					$adb->pquery("Update vtiger_project set project_id=?,open_date=? where projectid=?", array($incidentid, date("Y-m-d"), $projectid));
					$focushpsmlog = new hpsmlog();
					$focushpsmlog->column_fields['assigned_user_id'] = 1;
					$focushpsmlog->column_fields['chiamata'] = json_encode($result->return);
					$focushpsmlog->column_fields['incidentid'] = $incidentid;
					$focushpsmlog->column_fields['project'] = $projectid;
					$focushpsmlog->column_fields['citta'] = '';
					$focushpsmlog->column_fields['tiposcript'] = "RegisterServiceEvent";
					$focushpsmlog->saveentity("hpsmlog");
					$final_result = json_encode($result->return);
					$Projectquery = $adb->pquery("SELECT projectname FROM vtiger_project WHERE projectid=?", array($projectid));
					$projectname = $adb->query_result($Projectquery, 0, 'projectname');
					require_once("sonyIntegration/getServiceEventStatusLimited.php");
					getServiceEventStatus($entity);

					$Attachmentquery = $adb->pquery("SELECT * FROM vtiger_notes
                            join vtiger_attachments on filename=name
                            INNER JOIN vtiger_senotesrel ON vtiger_senotesrel.notesid= vtiger_notes.notesid
                            WHERE vtiger_senotesrel.crmid=? AND folderid=4", array($projectid));
					if ($adb->num_rows($Attachmentquery > 0)) {
						require_once("sonyIntegration/addAttachment.php");
						addAttachmentPOP($projectid);
					}
//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' Register ServiceEvent:success') WHERE crmid=$projectid");
				}
			}
			return $final_result;
		}
	}
}

?>
