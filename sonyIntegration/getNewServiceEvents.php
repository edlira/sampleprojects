<?php

include_once("include/database/PearDatabase.php");
include_once("include/utils/utils.php");
include_once("modules/counties/counties.php");
include_once("modules/cities/cities.php");
include_once("modules/Accounts/Accounts.php");
include_once("modules/Project/Project.php");
include_once("modules/hpsmlog/hpsmlog.php");

global $adb,$log;
date_default_timezone_set("Europe/Rome");
ini_set("display_errors","Off");
//error_reporting(E_ALL & ~E_WARNING);

  $socket_context = stream_context_create(
	   array('http' => array('protocol_version'  => 1.0))
  );
$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url, array('keep_alive' => false));
	//It needs to be UTF-8
$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;

$error = 0; 
try{
$result = $client->getNewServiceEvents(array('param'=>array('maxRowsReturned'=>100,'password'=>'PN8LKK','userId'=>'ITETEKNEM')));
}
catch(SoapFault $fault){
	// Check for errors
$error = 1; 
$err =$fault->faultcode." ". $fault->faultstring();
 echo '<h2>Error</h2><pre>' . $err . '</pre>';
}
if($error==0){
$j=0;
$val=array();
foreach($result->return->newRequest as $value){
	$info=$result->return->newRequest->info;    
		$id=$info->mainAscId;
		$caseId=$info->caseId;
		$serviceEventType=$info->serviceEventType;
		$logisticsType=$info->logisticsType;
		$aepBookingReference=$info->aepBookingReference;
		$unitInfo =$result->return->newRequest->unitInfo;
		$modelName =$unitInfo->modelName;
		$modelCode =$unitInfo->modelCode;
		$srn =$unitInfo->serialNumber;
		$purchaseDate =$unitInfo->purchaseDate;
		$hasPhysicalDamage =$unitInfo->hasPhysicalDamage;
		$premiumServicesInfo =$unitInfo->premiumServicesInfo;
			 $premiumServiceReference =$premiumServicesInfo->premiumServiceReference;
			 $premiumServiceType =$premiumServicesInfo->premiumServiceType;
		$symptomIrisCode =$unitInfo->symptomIrisCode;
		$conditionIrisCode =$unitInfo->conditionIrisCode;
		$productOrigin =$unitInfo->productOrigin;
		$productOsType =$unitInfo->productOsType;
		$bootLanguageISO2 =$unitInfo->bootLanguageISO2;
		$osLogin =$unitInfo->osLogin;
		$osPassword =$unitInfo->osPassword;
		$unitWarrantyEndDate =$unitInfo->unitWarrantyEndDate;
		$unitWarrantyStatus =$unitInfo->unitWarrantyStatus;
		$repairInWarranty =$unitInfo->repairInWarranty;
		$additionalInfo =$unitInfo->additionalInfo;
		$communicationLanguageISO2 =$unitInfo->communicationLanguageISO2;
		$companyName =$unitInfo->companyName;
		$firstName =$unitInfo->firstName;
		$lastName =$unitInfo->lastName;
		$title =$unitInfo->title;
		$email =$unitInfo->email;
		$fax =$unitInfo->fax;
		$fixedPhone =$unitInfo->fixedPhone;
		$mobilePhone =$unitInfo->mobilePhone;
		$outOfOfficePhone =$unitInfo->outOfOfficePhone;
		$interventionDate =$unitInfo->interventionDate;
	$customerInfo =$result->return->newRequest->customerInfo;
	$reference =$customerInfo->reference;
	$companyName =$customerInfo->companyName;
	$contactName =$customerInfo->contactName;
	$email =$customerInfo->email;
	$fax =$customerInfo->fax;
	$fixedPhone =$customerInfo->fixedPhone;
	$mobilePhone =$customerInfo->mobilePhone;
	$outOfOfficePhone =$customerInfo->outOfOfficePhone;
	$addressInfo =$customerInfo->addressInfo;
		$address1 =$addressInfo->address1;
		$address2 =$addressInfo->address2;
		$address3 =$addressInfo->address3;
		$city =$addressInfo->city;
		$countryISO2 =$addressInfo->countryISO2;
		$zipcode =$addressInfo->zipcode;

$projectQuery=$adb->pquery("SELECT projectid FROM vtiger_project
			  INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_project.projectid
			  WHERE ce.deleted=0 AND project_id=?",array($caseId));
$nrProject=$adb->num_rows($projectQuery);
if($nrProject<1){
$co1=$adb->pquery("SELECT * FROM vtiger_country 
				   INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_country.countryid
				   WHERE ce.deleted=0 AND vtiger_country.isocode= ?",array($countryISO2));
$countryid=$adb->query_result($co1,"countryid");
$cityQuery=$adb->pquery("SELECT * FROM vtiger_cities 
						 INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_cities.citiesid
						 WHERE ce.deleted=0 AND vtiger_cities.cityname LIKE ?",array(strtoupper($city)));
if($adb->num_rows($cityQuery)>0){
	$cityname=$adb->query_result($cityQuery,0,"cityname");
	$cityid=$adb->query_result($cityQuery,0,"citiesid");
}
else{
	$cityFocus=new cities();
	$cityFocus->column_fields['cityname']=$city;
	//$ac1->column_fields['county']=$st;
	$cityFocus->column_fields['country']=$countryid;
	$cityFocus->column_fields['postal_code']=$zipcode;
	$cityFocus->column_fields['assigned_user_id']=17971 ;
	$cityFocus->saveentity("cities");
	$cityid=$cityFocus->id;
}
//if( $mobilePhone!='' && !(preg_match("/^\+39/", $mobilePhone))){
//if (preg_match("/^39/",$mobilePhone)){
//    $logsch=$caseId.' Formato Errato (cellulare)  '.$mobile.'<br>';
//    $logs[$indp].=$logsch;
//    $mobilePhone="00".$mobilePhone;
//}
//else{
//  $mobile="+39".$mobile;
//      

$focusAccount=new Accounts();
$focusAccount->column_fields['accountname']=$firstName.' '.$lastName;
$focusAccount->column_fields['ragione_sociale']=$firstName.' '.$lastName;
$focusAccount->column_fields['nome']=$firstName;
$focusAccount->column_fields['cognome']=$lastName;
$focusAccount->column_fields['mbrand']='Sony';
$focusAccount->column_fields['cellulare']=$mobilePhone;
$focusAccount->column_fields['phone']=$fixedPhone;
$focusAccount->column_fields['accounttype']='Cliente finale';
$focusAccount->column_fields['email1']=$email;
$focusAccount->column_fields['bill_street']=$address1;
$focusAccount->column_fields['ship_street']=$address1;
$focusAccount->column_fields['bill_code']=$zipcode;
$focusAccount->column_fields['ship_code']=$zipcode;
$focusAccount->column_fields['bill_city']=$cityid;
$focusAccount->column_fields['ship_city']=$cityid;
//$focusAccount->column_fields['bill_state']=$st;
//$focusAccount->column_fields['ship_state']=$st;
$focusAccount->column_fields['bill_country']=$countryid;
$focusAccount->column_fields['ship_country']=$countryid;
$focusAccount->column_fields['assigned_user_id']=17971 ;
$focusAccount->column_fields['vaioasc']='Teknema';
$focusAccount->column_fields['level']=1;
$focusAccount->column_fields['newsletter']=1;
//$focusAccount->column_fields['hpsmid']=$value->ContactID;
$focusAccount->saveentity("Accounts");

$qrSymptom=$adb->pquery("SELECT symptomid 
						FROM vtiger_symptom 
						INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_symptom.symptomid
						WHERE ce.deleted=0 AND cd_symptom=?",array($symptomIrisCode));
$symptomid=$adb->query_result($qrSymptom,0,'symptomid');
$irisCode=$adb->pquery("SELECT conditionid FROM vtiger_condition 
						INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_condition.conditionid
						WHERE ce.deleted=0 AND cd_condition=?",array($conditionIrisCode));
$conditionid=$adb->query_result($irisCode,0,'conditionid');
$iwyes=$unitWarrantyStatus=="OOW"?false:true;

$projectFocus=new Project();
$projectFocus->column_fields['brand']='Sony';
$projectFocus->column_fields['productDescription']=$modelName;
$projectFocus->column_fields['model_number']=$modelCode;
$projectFocus->column_fields["tiposerviceevent"]=$serviceEventType;
$projectFocus->column_fields['logistictype']=$logisticsType;
$projectFocus->column_fields["serial_number"]=$srn;
$projectFocus->column_fields["seriale"]=$modelCode."-".$srn;
$projectFocus->column_fields["difettiesterni"]=$hasPhysicalDamage;
$projectFocus->column_fields['linktosymptom']=$symptomid;
$projectFocus->column_fields['linktocondition']=$conditionid;
$projectFocus->column_fields['productorigin']=$productOrigin;
$projectFocus->column_fields['productostype']=$productOsType;
$projectFocus->column_fields['iwyes']=$iwyes;
$projectFocus->column_fields['notearrivo']=$additionalInfo;
$projectFocus->column_fields['linktoaccountscontacts']=$focusAccount->id;
$projectFocus->column_fields['linktobuyer']=1928566;
$projectFocus->column_fields['progetto']=1297024;
$projectFocus->column_fields['projectname']='SNY '.$caseId;
$projectFocus->column_fields['project_id']=$caseId;
$projectFocus->column_fields['customer_purchase_data']=$purchaseDate;
$projectFocus->column_fields['assigned_user_id']=17971;
$projectFocus->column_fields['ship_codepr']=$zipcode;
$projectFocus->column_fields['ship_citypr']=$city;
$projectFocus->column_fields['mphone']=$mobilePhone;
$projectFocus->column_fields['ship_streetpr']=$address1;
$projectFocus->column_fields['emailcontact']=$email;
$projectFocus->column_fields['claimtype']="WARRANTY";
$projectFocus->column_fields['logistictype']="Nessuno";
$projectFocus->saveentity("Project");

$focus = new hpsmlog(); 
$focus->column_fields['assigned_user_id']=1;
$focus->column_fields['chiamata']= $projectFocus->id;
$focus->column_fields['incidentid']=$caseId;
$focus->column_fields['citta']=$cityid;
$focus->save("hpsmlog");
}
}
}
?>
