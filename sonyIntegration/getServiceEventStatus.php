<?php
//function getServiceEventStatus($entity){   
global $adb,$log;
require_once("modules/Emails/mail.php");
require_once("modules/hpsmlog/hpsmlog.php");
require_once("modules/Project/Project.php");
require_once("data/CRMEntity.php");
$allrecords=array();
if (isset($argv) && !empty($argv)) {
	//$projectid = $argv[1];
	$input = $argv[1];
}
if($input=='cron'){
date_default_timezone_set('Europe/Rome');
$currentdate = new DateTime();
$yesterday=$currentdate->modify( '-1 day' );
$y= $yesterday->format("Y-m-d");

$allpro=$adb->pquery("SELECT * FROM vtiger_project proj
			  INNER JOIN vtiger_crmentity ce ON ce.crmid=proj.projectid
			  WHERE ce.deleted=0 AND brand=? AND modifiedtime>=? AND currstatussc !='Repair Completed' 
			  AND template=0 AND project_id<>'' AND projectstatus NOT LIKE '%hold%' AND projectstatus NOT LIKE '%template%' ",array('Sony',$y));
while($allpro && $row=$adb->fetch_array($allpro)){
	$projectid=$row['projectid'];
	array_push($allrecords,$projectid);
}
}
else{
	 array_push($allrecords,$input);
}
for($i=0;$i<count($allrecords);$i++){

$projectid=$allrecords[$i];

  $socket_context = stream_context_create(
	   array('http' => array('protocol_version'  => 1.0))
  );
$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url, array('keep_alive' => false,'trace'=>true));
	//It needs to be UTF-8
$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;

$queryString="SELECT project_id,projectid,product_description,model_number,seriale
			  FROM vtiger_project
			  INNER JOIN vtiger_crmentity ON crmid=projectid
			  WHERE projectid=?";
$query=$adb->pquery($queryString,array($projectid));
$info=array();
$caseID=$adb->query_result($query,0,"project_id");
$mainAscReferenceId=$projectid;
$modelName=$adb->query_result($query,0,"product_description");
$modelCode=$adb->query_result($query,0,"model_number");
$serialNumber=$adb->query_result($query,0,"seriale");
$includePreviousEventsInfo=$adb->query_result($query,0,"incl_prev_se_info");

$info=array('caseId'=>$caseID,'mainAscReferenceId'=>$mainAscReferenceId,'modelName'=>$modelName,'modelCode'=>$modelCode,'serialNumber'=>$serialNumber,'includePreviousEventsInfo'=>$includePreviousEventsInfo);

$logFile="logs/mylog$date.log";
$logparam="";
error_log($logparam,3,$logFile) ;
$password="";
$userID="";
$params=array("userId"=>$userID,"password"=>$password,"info"=>$info);
try{
	$result =  $client->getServiceEventStatus(array("param"=>$params));
//    $resp=$client->__getLastResponse();
//var_dump($resp);exit;
  }
	catch(SoapFault $fault){
		// Check for errors
		$error = 1; 
   }
   $final_result="";

$response=$result->return;
$successful=$response->successful;
$eventStatus=$response->currentEventInfo->eventStatus;
if ($error==1) {
						$err =$fault->faultcode." ". $fault->faultstring;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationgetServiceEventStatus$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony getServiceEventStatus with ID ::: $projectid $datetime \n";              
						$LogContent.=$err; 

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony getServiceEventStatus with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
					   // $adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getServiceEventStatus: $err') WHERE crmid=$projectid");
						$final_result=$err;
		}
		elseif(!$successful){
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
						$focus->column_fields['incidentid']=0;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="getServiceEventStatus";
						$focus->saveentity("hpsmlog");

						$err =$result->return->errorCode." ". $result->return->errorMessage;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationgetServiceEventStatus$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony getServiceEventStatus with ID ::: $projectid $datetime \n";              
						$LogContent.=$err; 

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony getServiceEventStatus with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getServiceEventStatus: $err') WHERE crmid=$projectid");
						$final_result=$err;
}
else {
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=json_encode($result->return);
						$focus->column_fields['incidentid']=$caseID;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="getServiceEventStatus";
						$focus->saveentity("hpsmlog");
						 $hpsmqueryStringSS="SELECT substatushp
											FROM vtiger_substatushpsm
											INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_substatushpsm.substatushpsmid
											WHERE ce.deleted= 0 AND  macrostatusbrand=? AND tiporelazione=? AND substatusmacro=? ";
						$hpsmResult=$adb->pquery($hpsmqueryStringSS,array('Sony','Substatus SC Sony',$eventStatus));
						$substatushp=$adb->query_result($hpsmResult,0,'substatushp');
						$purchaseDateAccepted=$result->return->unitInfo->purchaseDate;
			$unitWarrantyStatus=$result->return->unitInfo->unitWarrantyStatus;
						if($unitWarrantyStatus=="IW")
						$adb->pquery("UPDATE vtiger_project SET warranty_status=? WHERE projectid=?",array("In Garanzia",$projectid));
					   /* $projectmodule="Project";
						$pcfocus=  CRMEntity::getInstance($projectmodule);
						$pcfocus->retrieve_entity_info($projectid,$projectmodule);
						$pcfocus->column_fields['currstatussc']=$statusDescription;
						$pcfocus->mode='edit';
						$pcfocus->id=$projectid;
						$pcfocus->saveentity($projectmodule);*/
						$adb->pquery("UPDATE vtiger_project SET currstatussc=?,purchasedateaccepted=? WHERE projectid=?",array($substatushp,$purchaseDateAccepted,$projectid));
						$final_result="success";
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getServiceEventStatus: success') WHERE crmid=$projectid");
}
}
echo $final_result;

//}

?>
