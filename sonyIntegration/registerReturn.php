<?php

function registerReturn($entity) {
	global $adb, $log;
	require_once("modules/Emails/mail.php");
	require_once("modules/hpsmlog/hpsmlog.php");
	require_once("modules/Project/Project.php");
	require_once("data/CRMEntity.php");
	require_once("modules/Users/Users.php");
	require_once("modules/PCDetails/PCDetails.php");
	   global $adb,$log;
	   global $current_user;
	   if(empty($current_user)){
		  $current_user = new Users();
		  $current_user->retrieveCurrentUserInfoFromFile(1);

	   }         
	$socket_context = stream_context_create(
			array('http' => array('protocol_version' => 1.0))
	);
	require_once ('lib/nusoap.php');
	$wsdl_url = 'https://ibiss.crse.com/cse-return-module/webservices/ReturnRegistrationWebService_v2?wsdl';
	$client = new SOAPClient($wsdl_url, array("trace" => true));
	//It needs to be UTF-8
	$client->soap_defencoding = 'UTF-8';
	$client->http_encoding = '';
	$client->decode_utf8 = false;

	$orig_pot = $entity->getId();
	$orig_id = explode("x", $orig_pot);
	$projectid = $orig_id[1];

	$focus = CRMEntity::getInstance("Project");
	$focus->retrieve_entity_info($projectid, "Project");
	$mainAscReferenceId = $projectid;
	$caseID = $focus->column_fields['project_id'];
	$brand = $focus->column_fields['brand'];
	$project_id = $focus->column_fields['project_id'];
	$currstatussc = $focus->column_fields['currstatussc'];
	if (!empty($project_id) && $brand == 'Sony' && ($currstatussc == 'Repair Completed' || $currstatussc == 'In-home completed')) {

		$Partsquery = $adb->pquery("SELECT claimtype,pcdetailsid,quantity,pcd.son,productname,ascid,vtiger_project.serial_number,fineriparazione 
						  FROM vtiger_pcdetails pcd
						  JOIN vtiger_products ON linktoproduct=productid
						  JOIN vtiger_project ON pcd.project=projectid
						  INNER JOIN vtiger_servicetype st ON st.linktopcdetails=pcd.pcdetailsid
						  WHERE pcd.project=? AND prodottoreale=1 AND rmaid='' AND isoptional=0 ", array($projectid));
		$nr = $adb->num_rows($Partsquery);
		for ($i = 0; $i < $nr; $i++) {
			$fineriparazione = $adb->query_result($Partsquery, $i, 'fineriparazione');
			$claimType = $adb->query_result($Partsquery, $i, 'claimtype');
			if ($fineriparazione == 'DOA') {
				$claimType = "DOAFUNCTIONALISSUE";
				$mainAscReferenceId = "";
			}
//"UNDERPERFORMANCE";
			$ascMaterialId = $adb->query_result($Partsquery, $i, 'pcdetailsid');
			$sonyPartNumber = $adb->query_result($Partsquery, $i, 'productname');
			$mispickedPartNumberReceived = "";
			$modelName = "";
			$serialNumber = $adb->query_result($Partsquery, $i, 'serial_number');
			$returnQty = $adb->query_result($Partsquery, $i, 'quantity');
//if(empty($returnQty))
			$returnQty = 1;
			$son = $adb->query_result($Partsquery, $i, 'son');
//$son=0;
			$gpToolRmaId = "";
			$aepBookingReference = "";
			$faultCode = "";

			$returnRequest = array("claimType" => $claimType, "mainAscReferenceId" => $mainAscReferenceId, "ascMaterialId" => $ascMaterialId,
				"sonyPartNumber" => $sonyPartNumber, "mispickedPartNumberReceived" => $mispickedPartNumberReceived, "modelName" => $modelName,
				"serialNumber" => $serialNumber, "returnQty" => $returnQty, "son" => $son, "gpToolRmaId" => $gpToolRmaId,
				"aepBookingReference" => $aepBookingReference, "faultCode" => $faultCode);
			$date = date('Y-m-d');
			$logFile = "logs/mylog$date.log";
			$logparam = "";
			foreach ($returnRequest as $key => $value) {
				$logparam.= $key . " =>" . $value;
			}
			error_log($logparam, 3, $logFile);
			$password = "PN8LKK";
			$userID = "ITETEKNEM";
			$params = array("userId" => $userID, "password" => $password, "returnRequest" => $returnRequest);
			try {
				$result = $client->registerReturn(array('registerReturnRequest' => $params));
			} catch (SoapFault $fault) {

				// Check for errors
				$error = 1;
			}
			$logFile="logs/eeeemylog$date.log";
			error_log("vjen",3,$logFile);
			$successful = $result->return->success;
			$final_result = "";
			if ($error == 1) {
				$err = $fault->faultcode . " " . $fault->faultstring;
				$date = date('Y-m-d');
				$logFile = "logs/sonyintegrationaddAttachment$date.log";


				$datetime = date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony registerReturn with ID ::: $projectid $datetime \n";
				$LogContent.=$err;
				//print_r($fault);

				$to = "e.deko@studiosynthesis.biz";
				$from = "Teknema-Sony Integration";
				$from_email = "support@evolutivo.it";
				$subject = "Sony registerReturn with ID ::: $projectid on $datetime ERROR!";
				$contents = $subject . " \n For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
				error_log($LogContent, 3, $logFile);
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' registerReturn: $err') WHERE crmid=$projectid");
				$final_result = $err;
			} elseif (!$successful) {
				$focus = new hpsmlog();
				$focus->column_fields['assigned_user_id'] = 1;
				$focus->column_fields['chiamata'] = "PCDetaildID: " . $ascMaterialId . " - " . $result->return->errorCode . " " . $result->return->errorMessage;
				$focus->column_fields['incidentid'] = 0;
				$focus->column_fields['project'] = $projectid;
				$focus->column_fields['citta'] = '';
				$focus->column_fields['tiposcript'] = "registerReturn";
				$focus->saveentity("hpsmlog");

				$err = $result->return->errorCode . " " . $result->return->errorMessage;
				$date = date('Y-m-d');
				$logFile = "logs/sonyintegrationaddAttachment$date.log";


				$datetime = date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony registerReturn with ID ::: $projectid $datetime \n";
				$LogContent.=$err;

				$to = "e.deko@studiosynthesis.biz";
				$from = "Teknema-Sony Integration";
				$from_email = "support@evolutivo.it";
				$subject = "Sony registerReturn with ID ::: $projectid on $datetime ERROR!";
				$contents = $subject . " \n For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
				$error_msg = "PCDetaildID: " . $ascMaterialId . " - " . $result->return->errorCode . " " . $result->return->errorMessage;
				$adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$error_msg' WHERE projectid=?", array($projectid));

				error_log($LogContent, 3, $logFile);
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' registerReturn: $err') WHERE crmid=$projectid");
				$final_result = $err;
			} else {
				$focus = new hpsmlog();
				$focus->column_fields['assigned_user_id'] = 1;
				$focus->column_fields['chiamata'] = "PCDetaildID: " . $ascMaterialId . " - " . json_encode($result->return);
				$focus->column_fields['incidentid'] = $caseID;
				$focus->column_fields['project'] = $projectid;
				$focus->column_fields['citta'] = '';
				$focus->column_fields['tiposcript'] = "registerReturn";
				$focus->saveentity("hpsmlog");
				$final_result = json_encode($result->return);
				$rmaid = $result->return->rmaId;
				$rmaDocumentUrl = $result->return->rmaDocumentUrl;
				$pcmodule = "PCDetails";
				$pcfocus = CRMEntity::getInstance($pcmodule);
				$pcfocus->retrieve_entity_info($ascMaterialId, $pcmodule);
				$pcfocus->column_fields['rmaid'] = $rmaid;
				$pcfocus->column_fields['pcdetail_doc'] = $rmaDocumentUrl;
				$pcfocus->mode = 'edit';
				$pcfocus->id = $ascMaterialId;
				$pcfocus->save($pcmodule);
				$final_result = "success";
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' registerReturn: $final_result') WHERE crmid=$projectid");
			}
		}
		return $final_result;
	}
}

?>
