<?php
function getServiceEventStatus($entity){   
global $adb,$log;
require_once("modules/Emails/mail.php");
require_once("modules/hpsmlog/hpsmlog.php");
require_once("modules/Project/Project.php");
require_once("data/CRMEntity.php");

  $socket_context = stream_context_create(
	   array('http' => array('protocol_version'  => 1.0))
  );
$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url, array('keep_alive' => false,'trace'=>true));
	//It needs to be UTF-8
$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;

$orig_pot = $entity->getId(); 
$orig_id = explode("x",$orig_pot);
$projectid = $orig_id[1];
$queryString="SELECT project_id,projectid,product_description,model_number,seriale, incl_prev_se_info,iwyes,warranty_status
			  FROM vtiger_project
			  INNER JOIN vtiger_crmentity ON crmid=projectid
			  WHERE projectid=? AND projectstatus NOT LIKE '%hold%' AND projectstatus NOT LIKE '%template%' ";
$query=$adb->pquery($queryString,array($projectid));
$info=array();
$caseID=$adb->query_result($query,0,"project_id");
$mainAscReferenceId=$projectid;
$modelName=$adb->query_result($query,0,"product_description");
$modelCode=$adb->query_result($query,0,"model_number");
$serialNumber=$adb->query_result($query,0,"seriale");
$includePreviousEventsInfo=$adb->query_result($query,0,"incl_prev_se_info");
$iswarranty=$adb->query_result($query,0,"iwyes");
$warranty_status=$adb->query_result($query,0,"warranty_status");

$info=array('caseId'=>$caseID,'mainAscReferenceId'=>$mainAscReferenceId,'modelName'=>$modelName,'modelCode'=>$modelCode,'serialNumber'=>$serialNumber,'includePreviousEventsInfo'=>$includePreviousEventsInfo);

$logFile="logs/mylog$date.log";
$logparam="";
//foreach($returnRequest as $key=>$value){
//    $logparam.= $key." =>".$value;
//}
error_log($logparam,3,$logFile) ;
$password="";
$userID="";
$params=array("userId"=>$userID,"password"=>$password,"info"=>$info);
try{
	$result =  $client->getServiceEventStatus(array("param"=>$params));
//    $resp=$client->__getLastResponse();
  }
	catch(SoapFault $fault){
		// Check for errors
		$error = 1; 
   }
   $final_result="";

$response=$result->return;
$successful=$response->successful;
$eventStatus=$response->currentEventInfo->eventStatus;
if ($error==1) {
						$err =$fault->faultcode." ". $fault->faultstring;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationgetServiceEventStatus$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony getServiceEventStatus with ID ::: $projectid $datetime \n";              
						$LogContent.=$err; 

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony getServiceEventStatus with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getServiceEventStatus: $err') WHERE crmid=$projectid");
						$final_result=$err;
		}
		elseif(!$successful){
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
						$focus->column_fields['incidentid']=0;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="getServiceEventStatus";
						$focus->saveentity("hpsmlog");

						$err =$result->return->errorCode." ". $result->return->errorMessage;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationgetServiceEventStatus$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony getServiceEventStatus with ID ::: $projectid $datetime \n";              
						$LogContent.=$err; 

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony getServiceEventStatus with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getServiceEventStatus: $err') WHERE crmid=$projectid");
						$final_result=$err;
}
else {
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=json_encode($result->return->currentEventInfo->eventStatus);
						$focus->column_fields['incidentid']=$caseID;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="getServiceEventStatus";
						$focus->saveentity("hpsmlog");
						 $hpsmqueryStringSS="SELECT substatushp
											FROM vtiger_substatushpsm
											INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_substatushpsm.substatushpsmid
											WHERE ce.deleted= 0 AND  macrostatusbrand=? AND tiporelazione=? AND substatusmacro=? ";
						$hpsmResult=$adb->pquery($hpsmqueryStringSS,array('Sony','Substatus SC Sony',$eventStatus));
						$substatushp=$adb->query_result($hpsmResult,0,'substatushp');
						$purchaseDateAccepted=$result->return->unitInfo->purchaseDate;
						$unitWarrantyStatus=$result->return->unitInfo->unitWarrantyStatus;
						if($unitWarrantyStatus==="OOW" && $iswarranty && ($warranty_status=="--Nessuno--"|| empty($warranty_status))){
						   $msg_desc='Prodotto Fuori Garanzia. Caricate la POP per verificare';
						   $adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$msg_desc' WHERE projectid=?",array($projectid));
						}
					   /* $projectmodule="Project";
						$pcfocus=  CRMEntity::getInstance($projectmodule);
						$pcfocus->retrieve_entity_info($projectid,$projectmodule);
						$pcfocus->column_fields['currstatussc']=$statusDescription;
						$pcfocus->mode='edit';
						$pcfocus->id=$projectid;
						$pcfocus->saveentity($projectmodule);*/
						$adb->pquery("UPDATE vtiger_project SET currstatussc=?,purchasedateaccepted=? WHERE projectid=?",array($substatushp,$purchaseDateAccepted,$projectid));
						$final_result="success";
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getServiceEventStatus: success') WHERE crmid=$projectid");
}
}

?>

