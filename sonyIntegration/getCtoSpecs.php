<?php

function getCtoSpecs($entity){
require_once("modules/Emails/mail.php");
require_once("modules/hpsmlog/hpsmlog.php");
global $adb;    

$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url, array('keep_alive' => false));

$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;

$orig_pot = $entity->getId(); 
$orig_id = explode("x",$orig_pot);
$ID = $orig_id[1]; 

$queryUnitInfo=$adb->pquery("Select model_number from vtiger_project where projectid=?",array($ID));

$password="";
$userID="";
$modelCode=$adb->query_result($queryUnitInfo,0,0);

$params=array("userId"=>$userID,"password"=>$password,"modelCode"=>$modelCode);


try{
$result = $client->getCtoSpecs(array("param"=>$params));
}
catch(SoapFault $fault){
	// Check for errors
$error = 1;         
}
$successful=$result->return->successful;
if ($error==1) {
				$err =$fault->faultcode." ". $fault->faultstring;
				$date=date('Y-m-d');
				$logFile="logs/sonyintegrationgetCtoSpecs$date.log";


				$datetime=date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony getCtoSpecs with ID ::: $ID $datetime \n";              
				$LogContent.=$err; 

				$to="e.dushku@studiosynthesis.biz";
				$from="Teknema-Sony Integration";
				$from_email="support@evolutivo.it";
				$subject="Sony getCtoSpecs with ID ::: $ID on $datetime ERROR!";
				$contents=$subject." \n For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

				error_log($LogContent,3,$logFile) ;
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getCtoSpecs: $err') WHERE crmid=$ID");
				  return 0;
}
elseif(!$successful){
	switch($result->return->errorCode){

		case "1345": 
		case "1211":
			registerUnitInfo($ID);
			break;
	}
				$focus = new hpsmlog(); 
				$focus->column_fields['assigned_user_id']=1;
				$focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
				$focus->column_fields['incidentid']=0;
				$focus->column_fields['project']=$ID;
				$focus->column_fields['citta']='';
				$focus->column_fields['tiposcript']="getCtoSpecs";
				$focus->saveentity("hpsmlog");

				$err =$result->return->errorCode." ". $result->return->errorMessage;
				$date=date('Y-m-d');
				$logFile="logs/sonyintegrationgetCtoSpecs$date.log";


				$datetime=date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony getCtoSpecs with ID ::: $ID $datetime \n";              
				$LogContent.=$err; 

				$to="e.dushku@studiosynthesis.biz";
				$from="Teknema-Sony Integration";
				$from_email="support@evolutivo.it";
				$subject="Sony getCtoSpecs with ID ::: $ID on $datetime ERROR!";
				$contents=$subject." \n $err. For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

				error_log($LogContent,3,$logFile) ;
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getCtoSpecs: $err') WHERE crmid=$ID");
				  return 1;
}
else{


				$focus = new hpsmlog(); 
				$focus->column_fields['assigned_user_id']=1;
				$focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
				$focus->column_fields['incidentid']=0;
				$focus->column_fields['project']=$ID;
				$focus->column_fields['citta']='';
				$focus->column_fields['tiposcript']="getCtoSpecs";
				$focus->saveentity("hpsmlog");

				$err =$result->return->errorCode." ". $result->return->errorMessage;
				$date=date('Y-m-d');
				$logFile="logs/sonyintegrationgetUnitInfo$date.log";


				$datetime=date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony getCtoSpecs with ID ::: $ID $datetime \n";              
				$LogContent.=$err; 

				$to="e.dushku@studiosynthesis.biz";
				$from="Teknema-Sony Integration";
				$from_email="support@evolutivo.it";
				$subject="Sony getCtoSpecs with ID ::: $ID on $datetime ERROR!";
				$contents=$subject." \n $err. For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

				error_log($LogContent,3,$logFile) ;
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getCtoSpecs: success') WHERE crmid=$ID");
	return 1;
}
}
?>
