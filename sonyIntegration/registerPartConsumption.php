<?php

function registerPartConsumption($entity){   
global $adb,$log;
require_once("modules/Emails/mail.php");
require_once("modules/hpsmlog/hpsmlog.php");

  $socket_context = stream_context_create(
       array('http' => array('protocol_version'  => 1.0))
  );
$wsdl_url = 'http://127.0.0.1/client/teknemanew/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url, array('keep_alive' => false));
	//It needs to be UTF-8
$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;

$orig_pot = $entity->getId(); 
$orig_id = explode("x",$orig_pot);
$servicetypeid = $orig_id[1]; 

$Projectquery=$adb->pquery("SELECT project_id,projectid,projectname,customer_purchase_data,linktobuyer 
                            FROM vtiger_servicetype
                            JOIN vtiger_project on vtiger_servicetype.project=projectid
                            WHERE servicetypeid=? AND project_id<>'' ",array($servicetypeid));
$caseID=$adb->query_result($Projectquery,0,'project_id');
$projectid=$adb->query_result($Projectquery,0,'projectid');
$catid=$adb->query_result($Projectquery,0,'linktobuyer');
$mainAscReferenceId=$projectid;
$catQuery=$adb->pquery("SELECT sitename FROM vtiger_account
			INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_account.accountid 
WHERE ce.deleted=0 AND accountid=?",array($catid));
$catcode=$adb->query_result($catQuery,0,'sitename');
//$subAscId="IT00833CTK0";
$subAscId=$catcode;
$subAscReferenceId="";

$Partsquery=$adb->pquery("SELECT pcdetailsid,quantity,cd_section,cd_defect,cd_repair,productname,vtiger_pcdetails.parteinwarranty as iwyes,fineriparazione,
                          isprimary,isoptional,vtiger_pcdetails.son,ascid  
                          FROM vtiger_servicetype
                          JOIN vtiger_pcdetails on linktopcdetails=pcdetailsid
                          JOIN vtiger_section on section=sectionid
                          JOIN vtiger_defect on defect=defectid
                          JOIN vtiger_repair on repair=repairid
                          JOIN vtiger_products on linktoproduct=productid
                          JOIN vtiger_project on vtiger_servicetype.project=projectid
                          WHERE servicetypeid=?",array($servicetypeid));

$ascMaterialId=$adb->query_result($Partsquery,0,'pcdetailsid');
$sonyPartNumber=$adb->query_result($Partsquery,0,'productname');
$sectionIrisCode=$adb->query_result($Partsquery,0,'cd_section');
$defectIrisCode=$adb->query_result($Partsquery,0,'cd_defect');
$repairIrisCode=$adb->query_result($Partsquery,0,'cd_repair');

$fineriparazione=$adb->query_result($Partsquery,0,'fineriparazione');
if($fineriparazione!='DOA'){
$info=array("caseId"=>$caseID,"mainAscReferenceId"=>$mainAscReferenceId,"subAscId"=>$subAscId,
         "subAscReferenceId"=>$subAscReferenceId);
$AscId="VIT0017185161";
$warrantyStatus=$adb->query_result($Partsquery,0,'iwyes')?"IW":"OOW";
$status="NORMAL";
$isPrimary=$adb->query_result($Partsquery,0,'isprimary');
$isOptional=!($isPrimary);
$amount=$adb->query_result($Partsquery,0,'quantity');
$son=$adb->query_result($Partsquery,0,'son');
$removePart=$adb->query_result($Partsquery,0,'isoptional');
$partReferenceNumber="";
$pcb="";

$part=array("ascMaterialId"=>$ascMaterialId,"defectIrisCode"=>$defectIrisCode,
        "isOptional"=>$isOptional,"isPrimary"=>$isPrimary,"sonyPartNumber"=>$sonyPartNumber,
        "sectionIrisCode"=>$sectionIrisCode,"repairIrisCode"=>$repairIrisCode,
        "warrantyStatus"=>$warrantyStatus,"status"=>$status,"amount"=>$amount,"son"=>$son,
        "removePart"=>$removePart,"partReferenceNumber"=>$partReferenceNumber,"pcb"=>$pcb,'AscId'=>$AscId);
$date=date('Y-m-d');
$logFile="logs/mylog$date.log";
$logparam="";
foreach( $part as $key=>$value){
    $logparam.= $key." =>".$value;
}
error_log($logparam,3,$logFile) ;

$password="";
$userID="";
$params=array("userId"=>$userID,"password"=>$password,"info"=>$info,"part"=>$part);
try{
    $result = $client->registerPartConsumption(array("param"=>$params));
    }
    catch(SoapFault $fault){
        // Check for errors
    $error = 1;  
    }
    $successful=$result->return->successful;
    $final_result="";
        if ($error==1) {
                        $err =$fault->faultcode." ". $fault->faultstring;
                        $date=date('Y-m-d');
                        $logFile="logs/sonyintegrationaddAttachment$date.log";


                        $datetime=date('l jS \of F Y h:i:s A');
                        $LogContent = "\n Sony registerPartConsumption with ID ::: $projectid $datetime \n";              
                        $LogContent.=$err; 

                        $to="e.dushku@studiosynthesis.biz";
                        $from="Teknema-Sony Integration";
                        $from_email="support@evolutivo.it";
                        $subject="Sony registerPartConsumption with ID ::: $projectid on $datetime ERROR!";
                        $contents=$subject." \n For further information please look at the log file $logFile";
                        send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
                        $error_msg=$err." - ".$ascMaterialId;
                        $adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$err' WHERE projectid=?",array($projectid));
                        $adb->pquery("UPDATE vtiger_pcdetails SET error_yes=1, pcd_error=0 WHERE pcdetailsid=?",array($ascMaterialId));
                        error_log($LogContent,3,$logFile) ;
                        //$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' registerPartConsumption: $err') WHERE crmid=$projectid");
                        $final_result=$err;
        }
        elseif(!$successful){
                        $focus = new hpsmlog(); 
                        $focus->column_fields['assigned_user_id']=1;
                        $focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
                        $focus->column_fields['incidentid']=0;
                        $focus->column_fields['project']=$projectid;
                        $focus->column_fields['citta']='';
                        $focus->column_fields['tiposcript']="registerPartConsumption";
                        $focus->saveentity("hpsmlog");
                        
                        $err =$result->return->errorCode." ". $result->return->errorMessage;
                        $date=date('Y-m-d');
                        $logFile="logs/sonyintegrationaddAttachment$date.log";


                        $datetime=date('l jS \of F Y h:i:s A');
                        $LogContent = "\n Sony registerPartConsumption with ID ::: $projectid $datetime \n";              
                        $LogContent.=$err; 

                        $to="e.dushku@studiosynthesis.biz";
                        $from="Teknema-Sony Integration";
                        $from_email="support@evolutivo.it";
                        $subject="Sony registerPartConsumption with ID ::: $projectid on $datetime ERROR!";
                        $contents=$subject." \n For further information please look at the log file $logFile";
                        send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
                        $error_msg=$err." - ".$ascMaterialId;
                        $adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata='Blocco', motivi='$err' WHERE projectid=?",array($projectid));
                        $adb->pquery("UPDATE vtiger_pcdetails SET error_yes=1,pcd_error=0 WHERE pcdetailsid=?",array($ascMaterialId));
                        error_log($LogContent,3,$logFile) ;
                        //$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' registerPartConsumption: $err') WHERE crmid=$projectid");
                        $final_result=$err;
}
else {
                        $focus = new hpsmlog(); 
                        $focus->column_fields['assigned_user_id']=1;
                        $focus->column_fields['chiamata']=json_encode($result->return);
                        $focus->column_fields['incidentid']=$caseID;
                        $focus->column_fields['project']=$projectid;
                        $focus->column_fields['citta']='';
                        $focus->column_fields['tiposcript']="registerPartConsumption";
                        $focus->saveentity("hpsmlog");
                        $final_result=json_encode($result->return);
                        $adb->pquery("UPDATE vtiger_pcdetails SET pcd_error=1,error_yes=0 WHERE pcdetailsid=?",array($ascMaterialId));
                                
                        //$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' registerPartConsumption: success') WHERE crmid=$projectid");
}
return $final_result;
}
}
?>
