<?php

function updateServiceEventStatus($entity, $letter = "") {
	global $adb, $log;
	date_default_timezone_set('Europe/Rome');
	require_once("modules/Emails/mail.php");
	require_once("modules/hpsmlog/hpsmlog.php");

	$socket_context = stream_context_create(
			array('http' => array('protocol_version' => 1.0))
	);

	$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
	$client = new SOAPClient($wsdl_url, array('keep_alive' => false, 'trace' => true));
	//It needs to be UTF-8
	$client->soap_defencoding = 'UTF-8';
	$client->http_encoding = '';
	$client->decode_utf8 = false;

	$orig_pot = $entity->getId();
	$orig_id = explode("x", $orig_pot);
	$projectid = $orig_id[1];
	$queryString = "Select projectid,modifiedtime,packagingok,cosmeticsok,bomok, project_id,linktobuyer, cd_symptom,cd_condition, substatusproj ,tiposerviceevent, logistictype,codiceflusso ,doaclaimdate,subascreferenceid
					from vtiger_project
					join vtiger_account on accountid=linktoaccountscontacts
					join vtiger_condition on conditionid=linktocondition
					join vtiger_symptom on symptomid=linktosymptom
					join vtiger_crmentity on crmid=projectid
					join vtiger_template ON linktoproject=vtiger_project.progetto
					where projectid=? AND project_id<>''";
	$query = $adb->pquery($queryString, array($projectid));
//$results=$adb->fetch_array($query);
	$log->debug('alda1');
	$caseID = $adb->query_result($query, 0, "project_id");
	$mainAscReferenceId = $projectid;

	$AscId = "VIT0017185161";
	$subAscReferenceId = $adb->query_result($query, 0, "projectid");
	$symptomIrisCode = $adb->query_result($query, 0, "cd_symptom");
	$conditionIrisCode = $adb->query_result($query, 0, "cd_condition");
	$substatusproj = $adb->query_result($query, 0, "substatusproj");
	$tiposerviceevent = $adb->query_result($query, 0, "tiposerviceevent");
	$logistictype = $adb->query_result($query, 0, "logistictype");
	$codiceflusso = $adb->query_result($query, 0, "codiceflusso");
	$subascreferenceid = $adb->query_result($query, 0, "subascreferenceid");
//$logisticType=$results["cd_condition"]; $logisticType in (logtype)
	$catid = $adb->query_result($query, 0, "linktobuyer");
	$catQuery = $adb->pquery("SELECT sitename FROM vtiger_account
							INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_account.accountid 
							WHERE ce.deleted=0 AND accountid=?", array($catid));
	$catcode = $adb->query_result($catQuery, 0, 'sitename');
//$subAscId="IT00833CTK0";
	$subAscId = $catcode;
	if ($tiposerviceevent != 'IHREP') {
		if (!empty($subascreferenceid))
			$subAscReferenceId = $subascreferenceid;
		else
			$subAscReferenceId = $adb->query_result($query, 0, "linktobuyer") . $letter;
	}
	$hpsmqueryStringSS = "Select COUNT( * ) AS nr
						from vtiger_substatushpsm
						INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_substatushpsm.substatushpsmid
						where ce.deleted= 0 AND  macrostatusbrand='Sony' and tiposervevent='$tiposerviceevent' and substatushp='$substatusproj' and comrel='$codiceflusso'";
	$hpsmqueryStringSS.=" AND logtype=?  ";


	$final_result = "";
//$final_result.=$hpsmqueryStringSS;
	$hpsmqueryss = $adb->pquery($hpsmqueryStringSS, array($logistictype));
//if it doesn't find any, will not change the substatus
	$tot_nr = $adb->query_result($hpsmqueryss, 0, 0);
	if ($tot_nr > 0 && $adb->num_rows($hpsmqueryss) > 0) {
//Get the respective status in sony, for this specific status in vtiger
		$queryStringSS = "SELECT substatusmacro,logistictype
						FROM vtiger_substatushpsm
						INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_substatushpsm.substatushpsmid
						WHERE ce.deleted=0 AND substatushp=? and macrostatusbrand='Sony' and tiposervevent=? AND tiporelazione=?";
		$hpsmquery = $adb->pquery($queryStringSS, array($substatusproj, $tiposerviceevent, 'Substatus'));
		$serviceEventStatus = $adb->query_result($hpsmquery, 0, "substatusmacro");
//$logisticType=$adb->query_result($hpsmquery,0,"	logistictype");
//Get the time when the status was changed for the last time
		$datetime = new DateTime($adb->query_result($query, 0, "modifiedtime"));
//$datetime->setTimezone(new DateTimeZone('UTC'));
		$datetime->modify('-2 hours');

		$statusStartDate = str_replace("+", ".", $datetime->format(DateTime::ISO8601)) . "Z";

//Get the Comment on the reason the status was changed
		$comment = "";		//$results[""];
		$awbId = "";
		//$results[""];
		$awbDeliveryETA = "";
		//$results[""];
//if($projectid==4423652 && $subAscReferenceId==1928566)
//    $subAscReferenceId="1928566B";
		$info = array("caseId" => $caseID, "mainAscReferenceId" => $mainAscReferenceId, "subAscId" => $subAscId,
			"subAscReferenceId" => $subAscReferenceId, "symptomIrisCode" => $symptomIrisCode, "conditionIrisCode" => $conditionIrisCode,
			"serviceEventStatus" => $serviceEventStatus, "statusStartDate" => $statusStartDate, "comment" => $comment,
			"awbId" => $awbId, "awbDeliveryETA" => $awbDeliveryETA, 'AscId' => $AscId);

		$claimDateVl = new DateTime($adb->query_result($query, 0, "doaclaimdate"));

		$claimDate = str_replace("+", ".", $claimDateVl->format(DateTime::ISO8601)) . "Z";

		$packagingOk = intval($adb->query_result($query, 0, "packagingok")) == 0 ? false : true;
		$cosmeticsOk = intval($adb->query_result($query, 0, "cosmeticsok")) == 0 ? false : true;
		$bomOk = intval($adb->query_result($query, 0, "bomok")) == 0 ? false : true;

		$doaInfo = array("claimDate" => $claimDate, "packagingOk" => $packagingOk,
			"cosmeticsOk" => $cosmeticsOk, "bomOk" => $bomOk);

//$interventionDate=$results[""];
		$uponCustomerRequest = "";
//Get the TC when the product started getting repaired
		$queryStringIntervention = "SELECT min(modifiedtime)
									FROM vtiger_projecttask
									INNER JOIN vtiger_crmentity ce ON ce.crmid = vtiger_projecttask.projecttaskid
									WHERE ce.deleted =0 AND substatus='repairing' AND status_pt<>'Closed' AND projectid=?";
		$interventionDateValue = $adb->query_result($adb->pquery($queryStringIntervention, array($projectid)), 0);
		if (empty($interventionDateValue))
			$interventionDateValue = date("Y-m-d H:i:s");
		$interventionDateObj = new DateTime($interventionDateValue);
//$interventionDateObj->setTimezone(new DateTimeZone('UTC'));
		$interventionDateObj->modify('-2 hours');
		$interventionDate = str_replace("+", ".", $interventionDateObj->format(DateTime::ISO8601)) . "Z";

		$interventionInfo = array("interventionDate" => $interventionDate, "uponCustomerRequest" => $uponCustomerRequest);

		$password = "";
		$userID = "";
		$params = array("userId" => $userID, "password" => $password, "info" => $info, "doaInfo" => $doaInfo, "interventionInfo" => $interventionInfo);
		try {
			$result = $client->updateServiceEventStatus(array("param" => $params));
			//$resp=$client->__getLastResponse();
			//error_log($resp,3,$logFile) ;
			//echo $resp; exit;
		} catch (SoapFault $fault) {
			// Check for errors
			$error = 1;
		}
		$successful = $result->return->successful;
		$final_result.="";
		if ($error == 1) {
			$err = $fault->faultcode . " " . $fault->faultstring;
			if ($fault->faultcode == 1305)
				updateServiceEventStatus($entity, "E");
			$date = date('Y-m-d');
			$logFile = "logs/sonyintegrationupdateServiceEventStatus$date.log";


			$datetime = date('l jS \of F Y h:i:s A');
			$LogContent = "\n Sony updateServiceEventStatus with ID ::: $projectid $datetime \n";
			$LogContent.=$err;

			$to = "e.dushku@studiosynthesis.biz";
			$from = "Teknema-Sony Integration";
			$from_email = "support@evolutivo.it";
			$subject = "Sony updateServiceEventStatus with ID ::: $projectid on $datetime ERROR!";
			$contents = $subject . " \n For further information please look at the log file $logFile $substatusproj $tiposerviceevent";
			send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

			error_log($LogContent, 3, $logFile);
			//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' updateServiceEventStatus: $err') WHERE crmid=$projectid");
			$final_result = $err;
		}
		elseif (!$successful) {
			$focus = new hpsmlog();
			$focus->column_fields['assigned_user_id'] = 1;
			$focus->column_fields['chiamata'] = $result->return->errorCode . " " . $result->return->errorMessage;
			$focus->column_fields['incidentid'] = $caseID;
			$focus->column_fields['project'] = $projectid;
			$focus->column_fields['citta'] = '';
			$focus->column_fields['tiposcript'] = "updateServiceEventStatus";
			$focus->saveentity("hpsmlog");

			$err = $result->return->errorCode . " " . $result->return->errorMessage;
			$date = date('Y-m-d');
			$logFile = "logs/sonyintegrationaupdateServiceEventStatus$date.log";


			$datetime = date('l jS \of F Y h:i:s A');
			$LogContent = "\n Sony updateServiceEventStatus with ID ::: $projectid $datetime \n";
			$LogContent.=$err . json_encode($result->return);

			$to = "e.dushku@studiosynthesis.biz";
			$from = "Teknema-Sony Integration";
			$from_email = "support@evolutivo.it";
			$subject = "Sony updateServiceEventStatus with ID ::: $projectid on $datetime ERROR!";
			$contents = $subject . " \n For further information please look at the log file $logFile  $substatusproj $tiposerviceevent";
			send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

			error_log($LogContent, 3, $logFile);
			//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' updateServiceEventStatus: $err') WHERE crmid=$projectid");
			$final_result = $err;
		} elseif ($successful) {

			$focus = new hpsmlog();
			$focus->column_fields['assigned_user_id'] = 1;
			$focus->column_fields['chiamata'] = json_encode($result->return);
			$focus->column_fields['incidentid'] = $caseID;
			$focus->column_fields['project'] = $projectid;
			$focus->column_fields['citta'] = '';
			$focus->column_fields['tiposcript'] = "updateServiceEventStatus";
			$focus->saveentity("hpsmlog");
			$err = $fault->faultcode . " " . $fault->faultstring;
			$date = date('Y-m-d');
			$logFile = "logs/sonyintegrationupdateServiceEventStatus$date.log";


			$datetime = date('l jS \of F Y h:i:s A');
			$LogContent = "\n Sony updateServiceEventStatus with ID ::: $projectid $datetime \n";
			$LogContent.=$err;

			$to = "e.dushku@studiosynthesis.biz";
			$from = "Teknema-Sony Integration";
			$from_email = "support@evolutivo.it";
			$subject = "Sony updateServiceEventStatus with ID ::: $projectid on $datetime SUCCESS!";
			$contents = $subject . " \n Substatus $substatusproj Service Event $tiposerviceevent";
			send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
			$final_result = "success";
			//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' updateServiceEventStatus: $final_result') WHERE crmid=$projectid");
		}
	} else
		$final_result.=$tot_nr . "No hpsm found";
	return $final_result;
//}
}

?>
