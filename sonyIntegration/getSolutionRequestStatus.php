<?php
require_once("modules/Emails/mail.php");
require_once("modules/hpsmlog/hpsmlog.php");
require_once("modules/HelpDesk/HelpDesk.php");
require_once("data/CRMEntity.php");
require_once("modules/Users/Users.php");
$current_user = new Users();
$current_user->retrieveCurrentUserInfoFromFile(1);
global $adb,$log;

$allrecords=array();
if (isset($argv) && !empty($argv)) {
	//$projectid = $argv[1];
	$input = $argv[1];
}
if($input=='cron'){
$ticketQuery=$adb->pquery("SELECT ticketid 
						   FROM vtiger_troubletickets tt
						   INNER JOIN vtiger_crmentity ce ON ce.crmid=tt.ticketid
						   WHERE ce.deleted=0 AND tt.reportingcategory=?",array("GPTool Solution Request"));
for($i=0;$i<$adb->num_rows($ticketQuery);$i++){
  $ticketid=$adb->query_result($ticketQuery,$i,'ticketid');
  array_push($allrecords,$ticketid);
}
}
else{
array_push($allrecords,$input);
}

//function getSolutionRequestStatus($entity){ 
$socket_context = stream_context_create(
	 array('http' => array('protocol_version'  => 1.0))
);
$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url,array("trace" => true));
//It needs to be UTF-8
$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;
//$orig_pot = $entity->getId(); 
//$orig_id = explode("x",$orig_pot);
//$ticketid = $orig_id[1]; 
//$tickedid=$_REQUEST['ticketid'];

for($i=0;$i<count($allrecords);$i++){
  $ticketid=$allrecords[$i];

$Projectquery=$adb->pquery("SELECT tt.project,linktobuyer,subascreferenceid
							FROM vtiger_troubletickets tt
							INNER JOIN vtiger_crmentity ce ON ce.crmid=tt.ticketid
							INNER JOIN vtiger_project ON tt.project=vtiger_project.projectid
							WHERE ce.deleted=0 AND tt.ticketid=?",array($ticketid));
$projectid=$adb->query_result($Projectquery,0,'project');
$accountid=$adb->query_result($Projectquery,0,'linktobuyer');
$subascreferenceid=$adb->query_result($Projectquery,0,"subascreferenceid");
$mainAscReferenceId=$projectid;
$catQuery=$adb->pquery("SELECT sitename FROM vtiger_account
						INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_account.accountid 
WHERE ce.deleted=0 AND accountid=?",array($accountid));
$catcode=$adb->query_result($catQuery,0,'sitename');
//$subAscId="IT00833CTK0";
$subAscId=$catcode;
if(!empty($subascreferenceid))
	$subAscReferenceId=$subascreferenceid;
else
	$subAscReferenceId=$accountid;

// TO DO: $endUserInvoiceID=
$info=array('mainAscReferenceId'=>$mainAscReferenceId,'subAscId'=>$subAscId,'subAscReferenceId'=>$subAscReferenceId);

$logFile="logs/mylog$date.log";
$logparam="";
//foreach($returnRequest as $key=>$value){
//    $logparam.= $key." =>".$value;
//}
error_log($logparam,3,$logFile) ;
$password="";
$userID="";
$params=array("userId"=>$userID,"password"=>$password,"info"=>$info);
try{
	$result = $client->getSolutionRequestStatus(array("param"=>$params));
	//$resp=$client->__getLastResponse();
  }
	catch(SoapFault $fault){
		// Check for errors
$error = 1;  
   }
	$successful=$result->return->successful;
	$final_result="";
if ($error==1) {
						$err =$fault->faultcode." ". $fault->faultstring;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationgetSolutionRequestStatus$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony getSolutionRequestStatus with ID ::: $projectid $datetime \n";              
						$LogContent.=$err;
						//print_r($fault);

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony getSolutionRequestStatus with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getSolutionRequestStatus: $err') WHERE crmid=$projectid");
						$final_result=$err;
		}
		elseif(!$successful){
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
						$focus->column_fields['incidentid']=0;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="getSolutionRequestStatus";
						$focus->saveentity("hpsmlog");

						$err =$result->return->errorCode." ". $result->return->errorMessage;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationgetSolutionRequestStatus$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony getSolutionRequestStatus with ID ::: $projectid $datetime \n";              
						$LogContent.=$err; 

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony getSolutionRequestStatus with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getSolutionRequestStatus: $err') WHERE crmid=$projectid");
						$final_result=$err;
}
else {
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=json_encode($result->return);
						$focus->column_fields['incidentid']=$caseID;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="getSolutionRequestStatus";
						$focus->saveentity("hpsmlog");
						$final_result=json_encode($result->return);
						$final_result=json_encode($result->return);
						$referenceId=$result->return->solutionInfo->referenceId;
						$statusCode=$result->return->solutionInfo->statusCode;
						$statusDescription=$result->return->solutionInfo->statusDescription;
						$statusStartDate=$result->return->solutionInfo->statusStartDate;
						$activityDescription=$result->return->solutionInfo->activityDescription;
						$subActivityDescription=$result->return->solutionInfo->subActivityDescription;
						$comments=$result->return->solutionInfo->comments;
						$ttmodule="HelpDesk";
						$ttfocus=  CRMEntity::getInstance($ttmodule);
						$ttfocus->retrieve_entity_info($ticketid,$ttmodule);
						$ttfocus->column_fields['referenceid']=$referenceId;
						$ttfocus->column_fields['solution_st_code']=$statusCode;
						$ttfocus->column_fields['statusdescription']=$statusDescription;
						$ttfocus->column_fields['statusstartdate']=$statusStartDate;
						$ttfocus->column_fields['activitydescription']=$activityDescription;
						$ttfocus->column_fields['subactivitydescription']=$subActivityDescription;
						$ttfocus->column_fields['info_gestione']=$comments;
						$ttfocus->column_fields['assigned_user_id']=1;
						$ttfocus->mode='edit';
						$ttfocus->id=$ticketid;
						$ttfocus->saveentity("HelpDesk");
			$final_result="success";
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' getSolutionRequestStatus: $final_result') WHERE crmid=$ticketid");
}
//}
}

echo $final_result;
?>
