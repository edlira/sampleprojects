<?php
require_once("modules/Emails/mail.php");
require_once("modules/hpsmlog/hpsmlog.php");
require_once("data/CRMEntity.php");
require_once("modules/HelpDesk/HelpDesk.php");
require_once("modules/Users/Users.php");
$current_user = new Users();
$current_user->retrieveCurrentUserInfoFromFile(1);
global $adb,$log;

if (isset($argv) && !empty($argv)) {
	//$projectid = $argv[1];
	$ticketid = $argv[1];
}
//function createSolutionRequest($entity){   

  $socket_context = stream_context_create(
	   array('http' => array('protocol_version'  => 1.0))
  );
$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
$client = new SOAPClient($wsdl_url,array("trace" => true));
//It needs to be UTF-8
$client->soap_defencoding = 'UTF-8';
$client->http_encoding='';
$client->decode_utf8 = false;
//$orig_pot = $entity->getId(); 
//$orig_id = explode("x",$orig_pot);
//$ticketid = $orig_id[1];
$Projectquery=$adb->pquery("SELECT project_id,vtiger_project.projectid,projectname, linktobuyer,
							ce.smownerid,tt.srmotive,tt.unitcolor,ce.description,
							tt.pcd_son,subascreferenceid,
							section.cd_section as section,vtiger_notes.notesid
							FROM vtiger_troubletickets tt
							INNER JOIN vtiger_crmentity ce ON ce.crmid=tt.ticketid
							INNER JOIN vtiger_project ON tt.project=vtiger_project.projectid
							INNER JOIN vtiger_senotesrel ON vtiger_senotesrel.crmid= vtiger_project.projectid
							INNER JOIN vtiger_notes ON vtiger_notes.notesid=vtiger_senotesrel.notesid
							INNER JOIN vtiger_attachmentsfolder ON vtiger_attachmentsfolder.folderid=vtiger_notes.folderid
							LEFT JOIN vtiger_pcdetails pc ON (pc.project=vtiger_project.projectid AND pc.son=tt.pcd_son)
							INNER JOIN vtiger_servicetype st on st.project=vtiger_project.projectid
							INNER JOIN vtiger_section section ON section.sectionid=st.section
							WHERE ce.deleted=0 AND foldername='POP' AND tt.ticketid=?",array($ticketid));
$projectid=$adb->query_result($Projectquery,0,'projectid');
$accountid=$adb->query_result($Projectquery,0,'linktobuyer');
$ticket_user=$adb->query_result($Projectquery,0,'smownerid');
$pcd_son=$adb->query_result($Projectquery,0,'pcd_son');
/*$ticket_user = new Users();
$ticket_user->retrieveCurrentUserInfoFromFile($smownerid);*/
$caseID=$adb->query_result($Projectquery,0,'project_id');
$subascreferenceid=$adb->query_result($Projectquery,0,"subascreferenceid");
$mainAscReferenceId=$projectid;
$catQuery=$adb->pquery("SELECT sitename FROM vtiger_account
						INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_account.accountid 
WHERE ce.deleted=0 AND accountid=?",array($accountid));
$catcode=$adb->query_result($catQuery,0,'sitename');
//$subAscId="IT00833CTK0";
$subAscId=$catcode;
if(!empty($subascreferenceid))
	$subAscReferenceId=$subascreferenceid;
else
	$subAscReferenceId=$accountid;
//$subAscReferenceId
$ttmotiveCode=$adb->query_result($Projectquery,0,'srmotive');
$hpsmQuery=$adb->pquery("SELECT motivecode FROM vtiger_substatushpsm
						 INNER JOIN vtiger_crmentity ce ON ce.crmid=vtiger_substatushpsm.substatushpsmid
						 WHERE ce.deleted=0 AND descmotivo=?",array($ttmotiveCode));

$motiveCode=$adb->query_result($hpsmQuery,0,'motivecode');
$unitAttributes=$adb->query_result($Projectquery,0,'unitcolor');
$sectionIRIS=$adb->query_result($Projectquery,0,'section');
$additionalInfo=$adb->query_result($Projectquery,0,'description');
$dealerInvoiceID=$adb->query_result($Projectquery,0,'notesid');
if(stripos($ttmotiveCode,"PART")!==false){
$pcdetailQuery=$adb->pquery("SELECT * FROM vtiger_pcdetails pc
							 INNER JOIN vtiger_crmentity ce ON ce.crmid=pc.pcdetailsid
					 WHERE ce.deleted=0 AND pc.project=? AND pc.son=?",array($projectid,$pcd_son));
$sonyPartNumber=$adb->query_result($pcdetailQuery,0,'linktoproduct');
$son=$adb->query_result($pcdetailQuery,0,'son');
}
//$dealerInvoiceDate="2014-09-18";
// TO DO: $endUserInvoiceID=
$info=array('caseID'=>$caseID,'mainAscReferenceId'=>$mainAscReferenceId,'subAscId'=>$subAscId,'subAscReferenceId'=>$subAscReferenceId,
'motiveCode'=>$motiveCode,'unitAttributes'=>$unitAttributes,'sonyPartNumber'=>$sonyPartNumber,'son'=>$son,
'sectionIRIS'=>$sectionIRIS,'additionalInfo'=>$additionalInfo,'dealerInvoiceID'=>$dealerInvoiceID,'endUserInvoiceID'=>$endUserInvoiceID);

$contactName=getUserName($ticket_user);
$contactEmail=getUserEmail($ticket_user);
$requestorInfo=array('contactName'=>$contactName,'contactEmail'=>$contactEmail);
$accountquery=$adb->pquery("SELECT *,
							billcountry.isocode as billcountryiso,billcity.cityname as billcityname
							FROM vtiger_account
							INNER JOIN vtiger_crmentity
									ON vtiger_account.accountid = vtiger_crmentity.crmid
							LEFT JOIN vtiger_accountbillads
									ON vtiger_accountbillads.accountaddressid = vtiger_account.accountid
							LEFT JOIN vtiger_country billcountry
									ON billcountry.countryid=vtiger_accountbillads.bill_country
							LEFT JOIN vtiger_cities billcity
									ON billcity.citiesid=vtiger_accountbillads.bill_city
							WHERE accountid=?",array($accountid));

$street=$adb->query_result($accountquery,0,'bill_street');
$city=$adb->query_result($accountquery,0,'billcityname');
$countryISO2=$adb->query_result($accountquery,0,'billcountryiso');
$zipcode=$adb->query_result($accountquery,0,'bill_code');
$addressInfo=array('street'=>$street,'city'=>$city,'countryISO2'=>$countryISO2,'zipcode'=>$zipcode);

//$accountNumber
$name=$adb->query_result($accountquery,0,'accountname');
$reference=$adb->query_result($accountquery,0,'codcat');
$receiverInfo=array('accountNumber'=>$accountNumber,'name'=>$name,'reference'=>$reference,'addressInfo'=>$addressInfo);


$logFile="logs/mylog$date.log";
$logparam="";
foreach($returnRequest as $key=>$value){
	$logparam.= $key." =>".$value;
}
error_log($logparam,3,$logFile) ;
$password="";
$userID="";
$params=array("userId"=>$userID,"password"=>$password,"info"=>$info,"receiverInfo"=>$receiverInfo,'requestorInfo'=>$requestorInfo);
try{
	$result = $client->createSolutionRequest(array('param'=>$params));
//var_dump($result);
	//$resp=$client->__getLastResponse();
  }
	catch(SoapFault $fault){
		// Check for errors
$error = 1;  
   }
	$successful=$result->return->successful;
	$final_result="";
		if ($error==1) {
						$err =$fault->faultcode." ". $fault->faultstring;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationcreateSolutionRequest$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony createSolutionRequest with ID ::: $projectid $datetime \n";              
						$LogContent.=$err;
						//print_r($fault);

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony createSolutionRequest with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' createSolutionRequest: $err') WHERE crmid=$projectid");
						$final_result=$err;
		}
		elseif(!$successful){
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=$result->return->errorCode." ".$result->return->errorMessage;
						$focus->column_fields['incidentid']=0;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="createSolutionRequest";
						$focus->saveentity("hpsmlog");

						$err =$result->return->errorCode." ". $result->return->errorMessage;
						$date=date('Y-m-d');
						$logFile="logs/sonyintegrationaddAttachment$date.log";


						$datetime=date('l jS \of F Y h:i:s A');
						$LogContent = "\n Sony createSolutionRequest with ID ::: $projectid $datetime \n";              
						$LogContent.=$err; 

						$to="e.dushku@studiosynthesis.biz";
						$from="Teknema-Sony Integration";
						$from_email="support@evolutivo.it";
						$subject="Sony createSolutionRequest with ID ::: $projectid on $datetime ERROR!";
						$contents=$subject." \n For further information please look at the log file $logFile";
						send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");

						error_log($LogContent,3,$logFile) ;
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' createSolutionRequest: $err') WHERE crmid=$projectid");
						$final_result=$err;
}
else {
						$focus = new hpsmlog(); 
						$focus->column_fields['assigned_user_id']=1;
						$focus->column_fields['chiamata']=json_encode($result->return);
						$focus->column_fields['incidentid']=$caseID;
						$focus->column_fields['project']=$projectid;
						$focus->column_fields['citta']='';
						$focus->column_fields['tiposcript']="createSolutionRequest";
						$focus->saveentity("hpsmlog");
						$final_result=json_encode($result->return);
						$referenceId=$result->return->referenceId;
						$ttmodule="HelpDesk";
						$ttfocus=  CRMEntity::getInstance($ttmodule);
						$ttfocus->retrieve_entity_info($ticketid,$ttmodule);
						$ttfocus->column_fields['referenceid']=$referenceId;
						$ttfocus->column_fields['assigned_user_id']=1;
						$ttfocus->mode='edit';
						$ttfocus->id=$ticketid;
						$ttfocus->saveentity($ttmodule);
						/*$projectmodule="Project";
						$projectfocus=CRMEntity::getInstance($projectmodule);
						$projectfocus->retrieve_entity_info($projectid,$projectmodule);
						$projectfocus->column_fields['ripetitiva_chiamata']="Blocco";
						$projectfocus->column_fields['motivi']="Solution Request Sony ancora aperta";
						$pcfocus->mode='edit';
						$pcfocus->id=$projectid;
						$pcfocus->saveentity($projectmodule);*/
						$adb->pquery("UPDATE vtiger_project SET ripetitiva_chiamata=?, motivi=? WHERE projectid=?",array("Blocco","Solution Request Sony ancora aperta",$projectid));
						shell_exec("cd /var/www/teknemanew; php sonyIntegration/getServiceEventStatus.php $projectid ;");
						$final_result="success";
						//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' createSolutionRequest: $final_result') WHERE crmid=$projectid");
}
//}
echo $final_result;
?>
