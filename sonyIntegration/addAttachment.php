<?php

function addAttachmentPOP($projectid) {
	global $adb, $log;
	require_once("modules/Emails/mail.php");
	require_once("modules/hpsmlog/hpsmlog.php");
	date_default_timezone_set('Europe/Rome');

	$socket_context = stream_context_create(
			array('http' => array('protocol_version' => 1.0))
	);
	$wsdl_url = 'http://127.0.0.1/client/sony/AscServiceSync_production.wsdl';
	$client = new SOAPClient($wsdl_url, array('keep_alive' => false,'trace'=>true));
	//It needs to be UTF-8
	$client->soap_defencoding = 'UTF-8';
	$client->http_encoding = '';
	$client->decode_utf8 = false;


	$Projectquery = $adb->pquery("SELECT project_id,projectname,customer_purchase_data FROM vtiger_project
							WHERE projectid=? AND brand='Sony'", array($projectid));
	if ($adb->num_rows($Projectquery) > 0) {
		$caseId = $adb->query_result($Projectquery, 0, 'project_id');
		$projectname = $adb->query_result($Projectquery, 0, 'projectname');
		$Attachmentquery = $adb->pquery("SELECT attachmentsid,filename,path
							FROM vtiger_notes
							INNER JOIN vtiger_attachments ON filename=name
							INNER JOIN vtiger_senotesrel ON vtiger_senotesrel.notesid= vtiger_notes.notesid
							WHERE vtiger_senotesrel.crmid=? AND folderid=4", array($projectid));
		if ($adb->num_rows($Attachmentquery) > 0) {
			$fileName = $adb->query_result($Attachmentquery, 0, 'filename');
			$message = "";
			$dateOfPurchase = $adb->query_result($Projectquery, 0, 'customer_purchase_data');
			$datetimeDealer = new DateTime($dateOfPurchase);

			$dateOfPurchaseValue = str_replace("+", ".", $datetimeDealer->format(DateTime::ISO8601)) . "Z";
			$pathFile = $adb->query_result($Attachmentquery, 0, 'path');
			$attachmentsid = $adb->query_result($Attachmentquery, 0, 'attachmentsid');
			$attachFile=$pathFile.$attachmentsid."_".$fileName;
			$fp = fopen($attachFile, 'r');
			$contents = fread($fp, filesize($attachFile));
			$attachment = base64_encode($contents);

			$password = "";
			$userID = "";

			$params = array("userId" => $userID, "password" => $password, "caseId" => $caseId, "fileName" => $fileName,
				"message" => $message, "dateOfPurchase" => $dateOfPurchaseValue, "attachment" => $attachment);
			$error = 0;
			try {
				$result = $client->addAttachment(array("param" => $params));
				$logFile="logs/attachmentrequest$date.log";
				$resp=$client->__getLastRequest();
				error_log($resp,3,$logFile) ;  
				$logFile="logs/attachmentresponse$date.log";
				$resp=$client->__getLastResponse();
				error_log($resp,3,$logFile) ;
			} catch (SoapFault $fault) {
				// Check for errors
				$error = 1;
			}
			$successful = $result->return->success;
			if ($error == 1) {
				$err = $fault->faultcode . " " . $fault->faultstring;
				$date = date('Y-m-d');
				$logFile = "logs/sonyintegrationaddAttachment$date.log";


				$datetime = date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony addAttachment with ID ::: $projectid $datetime \n";
				$LogContent.=$err;

				$to = "e.dushku@studiosynthesis.biz";
				$from = "Teknema-Sony Integration";
				$from_email = "support@evolutivo.it";
				$subject = "Sony addAttachment with ID ::: $projectid on $datetime ERROR!";
				$contents = $subject . " \n For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' addAttachment: $err') WHERE crmid=$projectid");
				error_log($LogContent, 3, $logFile);
			} elseif (!$successful) {
				$focus = new hpsmlog();
				$focus->column_fields['assigned_user_id'] = 1;
				$focus->column_fields['chiamata'] = $result->return->errorCode . " " . $result->return->errorMessage;
				$focus->column_fields['incidentid'] = $caseId;
				$focus->column_fields['project'] = $projectid;
				$focus->column_fields['citta'] = '';
				$focus->column_fields['tiposcript'] = "addAttachment";
				$focus->saveentity("hpsmlog");

				$err = $result->return->errorCode . " " . $result->return->errorMessage;
				$date = date('Y-m-d');
				$logFile = "logs/sonyintegrationaddAttachment$date.log";


				$datetime = date('l jS \of F Y h:i:s A');
				$LogContent = "\n Sony addAttachment with ID ::: $projectid $datetime \n";
				$LogContent.=$err;

				$to = "e.dushku@studiosynthesis.biz";
				$from = "Sony Integration";
				$from_email = "support@evolutivo.it";
				$subject = "Sony addAttachment with ID ::: $projectid on $datetime ERROR!";
				$contents = $subject . " \n For further information please look at the log file $logFile";
				send_mail("Accounts", $to, $from, $from_email, $subject, $contents, "", "", "", "");
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' addAttachment: $err') WHERE crmid=$projectid");
				error_log($LogContent, 3, $logFile);
			} else {
				$focus = new hpsmlog();
				$focus->column_fields['assigned_user_id'] = 1;
				$focus->column_fields['chiamata'] = json_encode($result->return);
				$focus->column_fields['incidentid'] = $caseId;
				$focus->column_fields['project'] = $projectid;
				$focus->column_fields['citta'] = '';
				$focus->column_fields['tiposcript'] = "addAttachment";
				$focus->saveentity("hpsmlog");
				//$adb->query("UPDATE vtiger_crmentity SET description=CONCAT(description,' addAttachment: success') WHERE crmid=$projectid");
			}
		}
	}
}

?>
